//
//  BarCodeUtil.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/26/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class QRCodeUtil {
    static func randomQRCodeKey() -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 15 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    static func generateQRCode(string : String) -> UIImage? {
        let data = string.data(using: String.Encoding.utf8)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
            
            filter.setValue(data, forKey: "inputMessage")
            
            filter.setValue("H", forKey: "inputCorrectionLevel")
            colorFilter.setValue(filter.outputImage, forKey: "inputImage")
            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1") // Background white
            colorFilter.setValue(CIColor(red: 0.282353, green: 0.360784, blue: 0.42745), forKey: "inputColor0") // Foreground or the barcode
            
            guard let _ = colorFilter.outputImage else {
                    return nil
            }

            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = colorFilter.outputImage?.applying(transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}
