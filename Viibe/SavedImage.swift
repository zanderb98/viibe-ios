//
//  PartyImage.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/17/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class SavedImage: Object {
    @objc dynamic var data: Data? = nil
    @objc dynamic var url: String = ""
    
   
    func setup(_ url: String, data: Data) {
        self.data = data
        self.url = url
    }
    
    func setup(_ url: String, image: UIImage) {
        self.data = UIImageJPEGRepresentation(image, 1)
        self.url = url
    }
    
    override static func primaryKey() -> String? {
        return "url"
    }
}
