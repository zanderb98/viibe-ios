//
//  vPartyImage.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/17/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class vPartyImage {
    var id = ""
    
    var image: UIImage? = nil
    var caption: String = ""
    var url: String = ""
    
    init(image: UIImage, caption: String = "") {
        self.id = UUID().uuidString
        
        self.image = image
        self.caption = caption
    }
    
    init(_ img: PartyImage) {
        if let data = img.data {
            image = UIImage(data: data, scale: 1.0)
        }
        
        id = img.id
        caption = img.caption
        url = img.url
    }
}
