//
//  Fonts.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/11/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class Fonts {
    static func museo100(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "MuseoSansW01-100", size: size)!
    }
    
    static func museo300(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "MuseoSansW01-300", size: size)!
    }
    
    static func museo500(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "MuseoSansW01-500", size: size)!
    }
    
    static func museo700(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "MuseoSansW01-700", size: size)!
    }
    
    static func fou(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "FouRegularW00-Regular", size: size)!
    }
    
    static func fouMedium(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "FouMediumW00-Regular", size: size)!
    }
    
    static func fouLight(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "FouLightW00-Regular", size: size)!
    }
    
    static func fouBold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "FouBoldW00-Regular", size: size)!
    }
    
    static func visbyBold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "VisbyRoundCF-Bold", size: size)!
    }
}
