//
//  Colors.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/18/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    static let viibeBlue = UIColor(hex: "485C6D")
    static let viibeOrange = UIColor(hex: "F28B7A")
}
