//
//  Util.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/25/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import RealmSwift
import Stripe
import MaterialComponents
import CoreLocation
import Alamofire

class Util {
    static var cities: [String]?
    
    static func getCurrentUser(_ realm: Realm = try! Realm()) -> User? {
        return realm.objects(User.self).first
    }
    
    static func getCurrentUserUnmanaged() -> vUser? {
        guard let user = getCurrentUser() else {
            return nil
        }
        
        return vUser(user)
    }
    
    static func getParty(_ id: String, realm: Realm = try! Realm()) -> Party? {
        return realm.objects(Party.self).first(where: { (party) -> Bool in
            return party.id == id
        })
    }
    
    static func getHost(_ id: String, realm: Realm = try! Realm()) -> Host? {
        return realm.objects(Host.self).first(where: { (host) -> Bool in
            return host.id == id
        })
    }
    
    static func closestCity(lat: Double, lng: Double) -> String {
        guard let cities = parseCities() else {
            return ""
        }
            
        var closestCity = ""
        var closestDistance: Double = -1
        
        let userCoord = CLLocation(latitude: lat, longitude: lng)
        
        for line in cities {
            let arr = line.split(separator: ",", omittingEmptySubsequences: true).map(String.init)
            
            if let lat = Double(arr[2]), let lng = Double(arr[3]) {
                let coord = CLLocation(latitude: lat, longitude: lng)
                let distance = coord.distance(from: userCoord)
                
                if closestDistance == -1 || distance < closestDistance {
                    closestDistance = distance
                    closestCity = String(arr[0])
                }
            }
        }
        
        return closestCity
    }
    
    static func getCities(lat: Double, lng: Double) -> [(String, Double)] {
        var toSort: [(String, Double)] = []
            
        guard let cities = parseCities() else {
            return []
        }
            
        let userCoord = CLLocation(latitude: lat, longitude: lng)
        
        for line in cities {
            let arr = line.split(separator: ",", omittingEmptySubsequences: true).map(String.init)
            
            if let lat = Double(arr[2]), let lng = Double(arr[3]) {
                let coord = CLLocation(latitude: lat, longitude: lng)
                let distance = coord.distance(from: userCoord)
                
                toSort.append((arr[0], distance))
            }
        }
        
        toSort.sort { (a, b) -> Bool in
            return a.1 < b.1
        }
        
        return toSort
    }
    
    static func locationFor(city: String) -> (Double, Double)? {
        guard let cities = parseCities() else {
            return nil
        }
            
        for line in cities {
            let arr = line.split(separator: ",", omittingEmptySubsequences: true).map(String.init)
            
            if arr[0] == city {
                if let lat = Double(arr[2]), let lng = Double(arr[3]) {
                    return (lat, lng)
                } else {
                    return nil
                }
            }
        }
        
        return nil
    }
    
    static func parseCities() -> [String]? {
        if cities != nil {
            return cities
        }
        
        do {
            let path = Bundle.main.path(forResource: "cities", ofType: "csv")
        
            let csv = try String(contentsOfFile: path!)
            self.cities = csv.split(separator: "\n", omittingEmptySubsequences: true).map(String.init)
            
            return cities
        } catch {
            return nil
        }
    }
    
    static func cardBrand(brand: STPCardBrand) -> String {
        if brand == .visa {
            return "visa"
        } else if brand == .masterCard {
            return "master"
        } else if brand == .discover {
            return "discover"
        } else if brand == .amex {
            return "amex"
        } else if brand == .dinersClub {
            return "dinersClub"
        } else if brand == .JCB {
            return "jcb"
        }
        
        return "unknown"
    }
    
    static func defaultAlert(_ vc: UIViewController, title: String, message: String? = nil, action: (() -> Void)? = nil) {
        let alertController = MDCAlertController(title: title, message: message)
        
        let okAction = MDCAlertAction(title:"OK") { (a) in
            if (action != nil) {
                action!()
            }
        }
        
        alertController.addAction(okAction)
        vc.present(alertController, animated:true, completion: nil)
    }
    
    static func messageTimestamp(date: Date) -> String {
        let timeDiff = Int(Date().timeIntervalSince1970 - date.timeIntervalSince1970)
        if timeDiff < 60 {
            return "\(timeDiff)s"
        }
        
        let timeDiffMins = Int(timeDiff / 60)
        if timeDiffMins < 60 {
            return "\(timeDiffMins)m"
        }
        
        let timeDiffHrs = Int(timeDiffMins / 60)
        if timeDiffHrs < 24 {
            return "\(timeDiffHrs)h"
        }
        
        let timeDiffDays = Int(timeDiffHrs / 24)
        if timeDiffDays < 7 {
            return "\(timeDiffDays)d"
        }
        
        return "\(Int(timeDiffDays / 7))w"
    }
    
    static func getDateText(date: Date) -> String {
        return "\(getDayText(date: date)) at \(getTimeText(date: date))"
    }
    
    static func getDayText(date: Date, fullMonth: Bool = false) -> String {
        let formatter = DateFormatter()
        let cal = NSCalendar.current
        
        if cal.isDateInToday(date) {
            return "Today"
        } else if cal.isDateInTomorrow(date) {
            return "Tomorrow"
        } else if cal.isDateInYesterday(date) {
            return "Yesterday"
        }
        
        let dateRangeStart = Date()
        let dateRangeEnd = date
        let components = Calendar.current.dateComponents([.day], from: dateRangeStart, to: dateRangeEnd)
        
        let days = components.day ?? 0
        if days <= 6 && Date() < date {
            formatter.dateFormat = "EEEE"
            return formatter.string(from: date)
        }
        
        formatter.dateFormat = fullMonth ? "MMMM d" : "MMM d"
        return formatter.string(from: date)
    }
    
    static func getTimeText(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        
        return formatter.string(from: date)
    }
    
    static func setSkippedLogin() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "SkippedLogin")
    }
    
    static func setGoogleSignInFromLogin(_ fromLogin: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(fromLogin, forKey: "GoogleSignInFromLogin")
    }
    
    static func isGoogleSignInFromLogin() -> Bool {
        return UserDefaults.standard.bool(forKey:"GoogleSignInFromLogin")
    }
    
    static func clearLoginStatus() {
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "SkippedLogin")
    }
    
    static func getLoginStatus() -> LoginStatus {
        if UserDefaults.standard.bool(forKey: "SkippedLogin") {
            return .skipped
        }
        
        return getCurrentUser() == nil ? .none : .loggedIn
    }
    
    static func favoriteParty(_ id: String) {
        let defaults = UserDefaults.standard
        
        let curr = defaults.string(forKey: "FavoriteParties")
        defaults.set("\(curr ?? "")~\(id)~", forKey: "FavoriteParties")
    }
    
    static func unfavoriteParty(_ id: String) {
        let defaults = UserDefaults.standard
        
        let curr = defaults.string(forKey: "FavoriteParties") ?? ""
        defaults.set(curr.replacingOccurrences(of: "~\(id)~", with: ""), forKey: "FavoriteParties")
    }
    
    static func isFavorited(_ id: String) -> Bool {
        let defaults = UserDefaults.standard
        
        let curr = defaults.string(forKey: "FavoriteParties") ?? ""
        return curr.contains(id)
    }
    
    static func footerView(height: CGFloat) -> UIView {
        let footer = UIView()
        footer.backgroundColor = .clear
        footer.frame.size.height = height
        return footer
    }
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isPartyToday(party: Party) -> Bool {
        let hours = Calendar.current.component(.hour, from: Date())
        return party.date.isToday() && hours >= 6 || party.date.isYesterday() && hours < 6;
    }
    
    static func fadeOut(views: [UIView], duration: TimeInterval, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: {
            for view in views {
                view.alpha = 0
            }
        }) { (_) in
            for view in views {
                view.isHidden = true
            }
            
            if completion != nil {
                completion!()
            }
        }
    }
    
    static func keyExists(_ key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    static func fadeIn(views: [UIView], duration: TimeInterval, completion: (() -> Void)? = nil) {
        for view in views {
            view.alpha = 0
            view.isHidden = false
        }
        
        UIView.animate(withDuration: duration, animations: {
            for view in views {
                view.alpha = 1
            }
        }) { (_) in
            if completion != nil {
                completion!()
            }
        }
    }
    
    static func checkUsername(_ vc: UIViewController, task: @escaping (() -> Void)) {
        guard let user = getCurrentUserUnmanaged() else {
            return
        }
        
        if user.username.isEmpty {
            let card = UsernameCard()
            card.show(vc) {
                task()
            }
        } else {
            task()
        }
    }
}
