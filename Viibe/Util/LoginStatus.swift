//
//  LoginStatus.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/13/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

enum LoginStatus {
    case loggedIn
    case skipped
    case none
}
