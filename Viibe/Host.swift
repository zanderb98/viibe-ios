
//
//  Host.swift
//  viibehost
//
//  Created by Zander Bobronnikov on 5/19/18.
//  Copyright © 2018 Viibe. All rights reserved.
//

import Foundation
import RealmSwift

class Host: Object {
    @objc dynamic var name = ""
    @objc dynamic var desc = ""

    @objc dynamic var id = ""
    
    @objc dynamic var profilePicture: Data?
    @objc dynamic var profilePictureURL = ""
    
    @objc dynamic var followers = 0
    
    let hostedParties = List<StringObj>()
    
    func setup(json: [String: Any]) {
        self.name = json["Name"] as! String
        self.desc = json["Description"] as! String
        self.followers = json["Followers"] as! Int
        
        if let id = json["_id"] as? String {
            self.id = id
        } else {
            self.id = UUID().uuidString
        }
        
        if let url = json["ProfileImage"] as? String {
            self.profilePictureURL = url
        }
        
        for str in (json["HostedParties"] as! [String]) {
            let obj = StringObj()
            obj.str = str
            hostedParties.append(obj)
        }
    }
    
    func setup(_ host: vHost) {
        self.id = host.id
        self.name = host.name
        self.desc = host.desc
        self.profilePictureURL = host.profilePictureURL
        self.followers = host.followers
        
        if let image = host.profilePicture {
            profilePicture = UIImageJPEGRepresentation(image, 1)
        }
        
        for str in host.hostedParties {
            let obj = StringObj()
            obj.str = str
            hostedParties.append(obj)
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func setImage(img: UIImage) {
        profilePicture = UIImageJPEGRepresentation(img, 1)
    }
}

