//
//  vMessage.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/16/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

class vMessage {
    var userID = ""
    var messageID = ""
    var text: String = ""
    var date: Date = Date()
    var atParty: Bool = false
    var isHost: Bool = false
    var displayName: String = ""
    
    init(_ message: Message) {
        self.userID = message.userID
        self.messageID = message.messageID
        self.text = message.text
        self.date = message.date
        self.atParty = message.atParty
        self.isHost = message.isHost
        self.displayName = message.displayName
    }
}
