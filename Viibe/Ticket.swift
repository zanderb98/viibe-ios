//
//  Ticket.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/14/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class Ticket: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var type: String = "fixed"
    @objc dynamic var malePrice: Double = 0
    @objc dynamic var femalePrice: Double = 0
    @objc dynamic var quantity: Int = 0
    @objc dynamic var quantitySold: Int = 0
    @objc dynamic var reserveTickets = 0
    @objc dynamic var reserveTicketsSold = 0
    @objc dynamic var fromDate: Date?
    @objc dynamic var toDate: Date?
    
    func setup(json: [String: Any]) {
        if let id = json["TicketID"] as? String {
            self.id = id
        }
        
        if let name = json["Name"] as? String {
            self.name = name
        }
        
        if let type = json["Type"] as? String {
            self.type = type
        }
        
        if let malePrice = json["MalePrice"] as? Double {
            self.malePrice = malePrice
        }
        
        if let femalePrice = json["FemalePrice"] as? Double {
            self.femalePrice = femalePrice
        }
        
        if let quantity = json["Quantity"] as? Int {
            self.quantity = quantity
        }
        
        if let quantitySold = json["QuantitySold"] as? Int {
            self.quantitySold = quantitySold
        }
        
        if let reserve = json["ReserveTickets"] as? Int {
            self.reserveTickets = reserve
        }
        
        if let reserveSold = json["ReserveTicketsSold"] as? Int {
            self.reserveTicketsSold = reserveSold
        }
        
        if let date = json["FromDate"] as? Double {
            self.fromDate = Date(timeIntervalSince1970: date)
        }
        
        if let date = json["ToDate"] as? Double {
            self.toDate = Date(timeIntervalSince1970: date)
        }
    }
    
    func setup(_ ticket: vTicket) {
        self.id = ticket.id
        self.name = ticket.name
        self.type = ticket.type
        self.malePrice = ticket.malePrice
        self.femalePrice = ticket.femalePrice
        self.quantity = ticket.quantity
        self.quantitySold = ticket.quantitySold
        self.reserveTickets = ticket.reserveTickets
        self.reserveTicketsSold = ticket.reserveTicketsSold
        self.fromDate = ticket.fromDate
        self.toDate = ticket.toDate
    }
}
