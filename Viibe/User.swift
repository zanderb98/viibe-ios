//
//  User.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var name = ""
    @objc dynamic var username = ""
    @objc dynamic var id = ""
    
    @objc dynamic var gender = true // true = male, false = female
    @objc dynamic var malesInGroup = 0
    @objc dynamic var femalesInGroup = 0
    @objc dynamic var lastTimeGroupSet: Date?
    
    @objc dynamic var lastKnownLat = 0.0
    @objc dynamic var lastKnownLng = 0.0
    
    @objc dynamic var profilePicture: Data?
    
    @objc dynamic var creditCardImage: Data?
    @objc dynamic var creditCardBrand = ""
    @objc dynamic var creditCardLast4 = ""
    
    @objc dynamic var currentParty: Party?
    
    let pastParties = List<Party>()
    let reservedTickets = List<Party>()
    let following = List<StringObj>()
    let likes = List<StringObj>()
    let dislikes = List<StringObj>()
    
    func setup(json: [String: Any]) {
        self.id = json["_id"] as! String
        self.name = json["Name"] as! String
        self.gender = (json["Gender"] as! String) == "male"
        self.creditCardLast4 = json["CardLast4"] as! String
        self.creditCardBrand = json["CardBrand"] as! String
        
        if let group = json["Group"] as? [String: Any] {
            malesInGroup = group["Males"] as! Int
            femalesInGroup = group["Females"] as! Int
            
            if let interval = group["LastTimeSet"] as? Double {
                self.lastTimeGroupSet = Date(timeIntervalSince1970: interval)
            }
        }
        
        if let username = json["Username"] as? String {
            self.username = username
        }

        
        if !creditCardBrand.isEmpty && creditCardBrand != "unknown" {
            if let img = UIImage(named: "\(creditCardBrand)CardImage") {
                if let imgData = UIImagePNGRepresentation(img) {
                    creditCardImage = imgData
                }
            }
        }
        
        for partyJSON in (json["PastParties"] as! [[String: Any]]) {
            let party = Party()
            party.isPastParty = true
            party.setup(json: partyJSON)
            
            pastParties.append(party)
        }
        
        if let reservedTix = json["ReservedTickets"] as? [[String: Any]] {
            for partyJSON in reservedTix {
                let party = Party()
                party.isReserved = true
                party.setup(json: partyJSON)
                
                reservedTickets.append(party)
            }
        }
        
        if let fo = json["Following"] as? [String] {
            for str in fo {
                let obj = StringObj()
                obj.str = str
                following.append(obj)
            }
        }
        
        if let lik = json["Likes"] as? [String] {
            for str in lik {
                let obj = StringObj()
                obj.str = str
                likes.append(obj)
            }
        }
        
        if let dis = json["Dislikes"] as? [String] {
            for str in dis {
                let obj = StringObj()
                obj.str = str
                dislikes.append(obj)
            }
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
