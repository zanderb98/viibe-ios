//
//  vTicket.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/14/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

class vTicket {
    var id = ""
    
    var name: String = ""
    var type: String = ""
    var malePrice: Double = -1
    var femalePrice: Double = -1
    var quantity: Int = -1
    var quantitySold: Int = 0
    var reserveTickets = 0
    var reserveTicketsSold = 0
    var fromDate: Date? = nil
    var toDate: Date? = nil
    
    var isAvailable: Bool {
        if !isBetweenDates {
            return false
        }
        
        return (quantity == -1) || (quantity - quantitySold > 0)
    }
    
    var isReserveAvailable: Bool {
        if !isBetweenDates {
            return false
        }
        
        return (reserveTickets - reserveTicketsSold > 0)
    }
    
    var isBetweenDates: Bool {
        if let from = fromDate {
            if Date().compare(from) == .orderedAscending {
                return false
            }
        }
        
        if let to = toDate {
            if Date().compare(to) == .orderedDescending {
                return false
            }
        }
        
        return true
    }
    
    init() {
        id = UUID().uuidString
    }
    
    init(_ ticket: Ticket) {
        self.id = ticket.id
        self.name = ticket.name
        self.type = ticket.type
        self.malePrice = ticket.malePrice
        self.femalePrice = ticket.femalePrice
        self.quantity = ticket.quantity
        self.quantitySold = ticket.quantitySold
        self.reserveTickets = ticket.reserveTickets
        self.reserveTicketsSold = ticket.reserveTicketsSold
        self.fromDate = ticket.fromDate
        self.toDate = ticket.toDate
    }
}
