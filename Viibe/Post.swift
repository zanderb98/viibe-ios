//
//  Post.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class Post: Object {
    dynamic var type: String = ""
    dynamic var userID: String = ""
    dynamic var username: String = ""
    dynamic var id: String = ""
    dynamic var text: String = ""
    dynamic var date: Date = Date()
    dynamic var likes: Int = 0
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var lng: Double = 0.0
    dynamic var address: String = ""
    
    dynamic var eventDate: Date = Date()
    dynamic var title: String = ""
    dynamic var caption: String = ""
    
    let comments = List<Comment>()
    
    func setup(json: [String: Any]) {
         self.id = json["_id"] as! String
        
        if let userID = json["UserID"] as? String {
            self.userID = userID
        }
        
        if let username = json["Username"] as? String {
            self.username = username
        }
        
        if let type = json["Type"] as? String {
            self.type = type
        }
        
        if type == "EVENT" {
            if let title = json["Title"] as? String {
                self.title = title
            }
            
            if let caption = json["Caption"] as? String {
                self.caption = caption
            }
            
            if let eventDate = json["EventDate"] as? Double {
                self.eventDate = Date(timeIntervalSince1970: eventDate)
            }
            
            if let address = json["Address"] as? String {
                self.address = address
            }
        } else {
            if let text = json["Text"] as? String {
                self.text = text
            }
        }
        
        if let likes = json["Likes"] as? Int {
            self.likes = likes
        }
        
        self.lat = ((json["Location"] as! [String: Any])["coordinates"] as! [Double])[1]
        self.lng = ((json["Location"] as! [String: Any])["coordinates"] as! [Double])[0]
        
        if let date = json["Date"] as? Double {
            self.date = Date(timeIntervalSince1970: date)
        }
        
        if let commentsArr = json["Comments"] as? [[String: Any]] {
            for cJson in commentsArr {
                let comment = Comment()
                comment.setup(cJson)
                comments.append(comment)
            }
        }
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
