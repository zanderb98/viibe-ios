//
//  UIView+Extensions.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/27/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func getConstraints(attr: NSLayoutAttribute) -> [NSLayoutConstraint] {
        let searchConstraints = self.superview == nil ? constraints : self.superview!.constraints
        var ret: [NSLayoutConstraint] = []
        for constraint in searchConstraints {
            if (constraint.firstAttribute == attr || constraint.secondAttribute == attr) {
                ret.append(constraint)
            }
        }
        
        return ret
    }
    
    func getConstraintByID(id: String) -> NSLayoutConstraint? {
        let searchConstraints = self.constraints
        
        for constraint in searchConstraints {
            if (constraint.identifier == id) {
               return constraint
            }
        }
        
        return nil
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addDropShadow(opacity: Float = 0.3, offset: CGSize = CGSize(width: 1, height: 1)) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = 5
        
    }
    
    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
   
}
