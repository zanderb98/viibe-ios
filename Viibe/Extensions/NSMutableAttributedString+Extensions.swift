//
//  NSMutableAttributedString+Extensions.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/29/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String, fontSize: CGFloat = 17) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold)]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func underline(_ text:String, fontSize: CGFloat = 17) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: fontSize),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue as AnyObject]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String, font: UIFont? = nil, fontSize: CGFloat = 17, color: UIColor? = nil)->NSMutableAttributedString {
        var attrs:[String:AnyObject] = [NSFontAttributeName: font == nil ? UIFont.systemFont(ofSize: fontSize) : font!]
        
        if color != nil {
            attrs[NSForegroundColorAttributeName] = color!
        }
        
        let normal =  NSAttributedString(string: text, attributes:attrs)
        self.append(normal)
        return self
    }
    
    public func link(_ textToFind:String, linkURL:String) -> NSMutableAttributedString {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSLinkAttributeName, value: linkURL, range: foundRange)
        }
        
        return self
    }
}
