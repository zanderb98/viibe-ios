//
//  Date+Extensions.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/25/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation

extension Date {
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func isTomorrow() -> Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    
    func isYesterday() -> Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    
    func sameDay(_ date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs: date)
    }
    
    func addMinutes(mins: Int) -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .minute, value: mins, to: self)!
    }
}
