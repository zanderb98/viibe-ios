//
//  UILabel+Extensions.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/13/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    var isTruncated: Bool {
        
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
    
    var untruncatedHeight: CGFloat {
        
        guard let labelText = text else {
            return 0
        }
        
        return (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: font],
            context: nil).size.height
    }
}
