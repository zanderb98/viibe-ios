//
//  vPartyAnalytics.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/16/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation

class vPartyAnalytics: Equatable {
    
    var userID: String?
    var name: String?
    var type: String?
    var price: Double?
    var date: Date?
    var gender: Bool?
    
    init(_ analytics: PartyAnalytics) {
        self.name = analytics.name
        self.type = analytics.type
        self.price = analytics.price
        self.date = analytics.date
        self.gender = analytics.gender
    }
    
    init(type: String, price: Double, gender: Bool, date: Date) {
        self.name = gender ? "John Doe" : "Jane Doe"
        self.type = type
        self.price = price
        self.gender = gender
        self.date = date
    }
    
    static func ==(lhs: vPartyAnalytics, rhs: vPartyAnalytics) -> Bool {
        return lhs.userID == rhs.userID && lhs.type == rhs.type && lhs.type == rhs.type && lhs.date == rhs.date
    }
}
