//
//  PartyAnalytics.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/16/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class PartyAnalytics: Object {
    dynamic var userID: String = ""
    dynamic var name: String = ""
    dynamic var type: String = ""
    dynamic var price: Double = 0.0
    dynamic var date: Date?
    dynamic var gender = false
    
    func setup(json: [String: Any]) {
        if let userID = json["UserID"] as? String {
            self.userID = userID
        }
        
        if let name = json["Name"] as? String {
            self.name = name
        }
        
        if let type = json["Type"] as? String {
            self.type = type
        }
        
        if let dateInt = json["Date"] as? Double {
            self.date = Date(timeIntervalSince1970: dateInt)
        }
        
        if let price = json["Price"] as? Double {
            self.price = price
        }
        
        if let gender = json["Gender"] as? String {
            self.gender = (gender == "male")
        }


    }
    
    func setup(_ analytics: vPartyAnalytics) {
        self.name = analytics.name!
        self.type = analytics.type!
        self.price = analytics.price!
        self.date = analytics.date
        self.gender = analytics.gender!
    }
}
