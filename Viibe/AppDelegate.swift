//
//  AppDelegate.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/23/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import GooglePlaces
import FacebookCore
import Stripe
import BugfenderSDK
import Alamofire
import UserNotifications
import RealmSwift
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate {

    var timer: Timer?
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        STPPaymentConfiguration.shared().publishableKey = Config.shared.stripePublicKey
        // do any other necessary launch configuration
        GMSPlacesClient.provideAPIKey(Config.shared.googlePlacesKey)
        
        Bugfender.activateLogger(Config.shared.bugfenderKey)
        Bugfender.enableUIEventLogging()  // optional, log user interactions automatically
        Bugfender.enableCrashReporting()  // optional, log crashes automatically
        
        GIDSignIn.sharedInstance().clientID = Config.shared.googleClientID
        GIDSignIn.sharedInstance().delegate = self
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
       
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: []) { (_, _) in }
        
        let config = Realm.Configuration(
            schemaVersion: 3,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    migration.enumerateObjects(ofType: User.className()) { oldObject, newObject in
                        newObject!["reservedTickets"] = List<Party>()
                    }
                    
                    migration.enumerateObjects(ofType: Party.className()) { oldObject, newObject in
                        newObject!["reservedTickets"] = 0
                        newObject!["reservedTicketsAllowed"] = 0
                    }
                }
                
                if (oldSchemaVersion < 2) {
                    migration.enumerateObjects(ofType: Party.className()) { oldObject, newObject in
                        newObject!["city"] = "Boston"
                    }
                }
                
                if (oldSchemaVersion < 3) {
                    migration.enumerateObjects(ofType: User.className()) { oldObject, newObject in
                        newObject!["name"] = oldObject!["username"]
                        newObject!["username"] = ""
                        newObject!["likes"] = []
                        newObject!["dislikes"] = []
                    }
                }
        },  deleteRealmIfMigrationNeeded: true)
        
        Realm.Configuration.defaultConfiguration = config
        UIApplication.shared.registerForRemoteNotifications()
        
        if let lo = launchOptions, let url = lo[.url] as? URL {
            if(url.scheme == "viibe") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let navVC = storyboard.instantiateViewController(withIdentifier: "NavController") as! UINavigationController
                self.window?.rootViewController = navVC
                
                let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! ViewController
                navVC.pushViewController(mainVC, animated: true)
                mainVC.openParty(id: url.host!)
                
                self.window?.makeKeyAndVisible()
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let partyID = userInfo["PartyID"] as? String {
            if application.applicationState == .active {
                NotificationCenter.default.post(Notification(name: Notifications.ticketChecked(partyID: partyID)))
            }
        }
        
        completionHandler(.noData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if let partyID = notification.request.content.userInfo["PartyID"] as? String {
            if UIApplication.shared.applicationState == .active {
                NotificationCenter.default.post(Notification(name: Notifications.ticketChecked(partyID: partyID)))
            }
        }
        
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenStr = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})

        if let user = Util.getCurrentUserUnmanaged() {
            let params = [
                "UserID": user.id,
                "Platform": "iOS",
                "DeviceToken": tokenStr,
                "ServerVersion": "2"
            ]
            
             Alamofire.request(Config.shared.registerDeviceURL, method: .post, parameters: params)
        }
        
        UserDefaults.standard.set(tokenStr, forKey: "DeviceToken")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for PUSH with error: \(error)")
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if(url.scheme == "viibe") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navVC = storyboard.instantiateViewController(withIdentifier: "NavController") as! UINavigationController
            self.window?.rootViewController = navVC
            
            let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! ViewController
            navVC.pushViewController(mainVC, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                mainVC.openParty(id: url.host!)
            }
            
            self.window?.makeKeyAndVisible()
            return true
        } else if url.scheme == Config.shared.googleURLScheme {
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else {
            return SDKApplicationDelegate.shared.application(app,
                                                         open: url,
                                                         sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                         annotation: options[UIApplicationOpenURLOptionsKey.annotation] as Any)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("Error signing in with Google\(error.localizedDescription)")
            
            Util.isGoogleSignInFromLogin() ?
                LoginViewController.googleSignInCallback?("", "", nil) :
                SignupViewController.googleSignInCallback?("", "", nil)
        } else {
            let idToken = user.authentication.idToken
            let accessToken = user.authentication.accessToken
            
            let imageWidth: CGFloat = 30
            let pic = user.profile.imageURL(withDimension: UInt(round(imageWidth * UIScreen.main.scale)))
            
            Util.isGoogleSignInFromLogin() ?
                LoginViewController.googleSignInCallback?(idToken ?? "", accessToken ?? "", pic) :
                SignupViewController.googleSignInCallback?(idToken ?? "", accessToken ?? "", pic)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        pauseUpdates()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        pauseUpdates()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.post(Notification(name: Notifications.EnteredForeground))
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        startUpdates()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        pauseUpdates()
    }
    
    //Update parties every 10 seconds
    func startUpdates() {
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (_) in
            guard let user = Util.getCurrentUserUnmanaged() else {
                if Util.getLoginStatus() == .skipped {
                    HostLoader.loadHosts {
                        PartyLoader.updateParties()
                    }
                    
                    FeedLoader.loadPosts()
                }
                
                return
            }
            
            HostLoader.loadHosts {
                PartyLoader.updateParties()
            }
            
            UserLoader.updateUser(user)
            FeedLoader.loadPosts()
        })
    }
    
    func pauseUpdates() {
        if timer == nil { return }
        
        timer!.invalidate()
        timer = nil
    }
}

