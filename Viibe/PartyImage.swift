//
//  PartyImage.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/17/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class PartyImage: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var data: Data? = nil
    @objc dynamic var caption: String = ""
    @objc dynamic var url: String = ""
    
    func setup(_ url: String) {
        self.id = UUID().uuidString
        self.url = url
    }
    
    func setup(_ img: vPartyImage) {
        if let image = img.image {
            setImage(img: image)
        }
        
        id = img.id
        caption = img.caption
        url = img.url
    }
    
    func setImage(img: UIImage) {
        data = UIImageJPEGRepresentation(img, 1)
    }
}
