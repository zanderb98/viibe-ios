//
//  Comment.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class Comment: Object {
    dynamic var userID: String = ""
    dynamic var username: String = ""
    dynamic var id: String = ""
    dynamic var text: String = ""
    dynamic var date: Date = Date()
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var lng: Double = 0.0
    
    dynamic var hasParent = false
    let comments = List<Comment>()
    
    func setup(_ json: [String: Any]) {
        if let commentID = json["CommentID"] as? String {
            self.id = commentID
        }
        
        if let userID = json["UserID"] as? String {
            self.userID = userID
        }
        
        if let username = json["Username"] as? String {
            self.username = username
        }
        
        if let text = json["Text"] as? String {
            self.text = text
        }
        
        if let date = json["Date"] as? Double {
            self.date = Date(timeIntervalSince1970: date)
        }
        
        if let parents = json["Parents"] as? [String] {
            hasParent = !parents.isEmpty
        }
        
        if let commentsArr = json["Comments"] as? [[String: Any]] {
            for cmt in commentsArr {
                let c = Comment()
                c.setup(cmt)
                comments.append(c)
            }
        }
    }
    
    func setup(_ comment: vComment) {
        self.id = comment.id
        self.username = comment.username
        self.userID = comment.userID
        self.text = comment.text
        self.date = comment.date
        self.hasParent = comment.hasParent
        
        self.comments.removeAll()
        for cmt in comment.comments {
            let c = Comment()
            c.setup(cmt)
            
            self.comments.append(c)
        }
    }
}

