//
//  RoundCornerView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class RoundBottomCornersView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        DispatchQueue.main.async {
            self.roundCorners(corners: [.bottomRight,.bottomLeft], radius: 5)
            self.layer.masksToBounds = true
        }
        
        self.layoutIfNeeded()
    }
}
