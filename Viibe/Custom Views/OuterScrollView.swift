//
//  OuterScrollView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/13/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class OuterScrollView: UIScrollView, UIGestureRecognizerDelegate {
    var inner: UIScrollView?
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if (inner == nil) {
            return true
        }
        
        if (self.inner!.isDragging == false) {
            self.inner!.isScrollEnabled = false; // The presence of this line does the job
        }
        
        return true;
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self
    }
    
    func didEndDragging() {
        inner!.isScrollEnabled = true
    }
}
