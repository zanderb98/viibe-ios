//
//  ViibeSlider.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 2/19/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class ViibeSlider: UISlider {

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var result = super.trackRect(forBounds: bounds)
        result.origin.x = 0
        result.size.width = bounds.size.width
        result.size.height = 10 //added height for desired effect
        return result
    }
    
    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        return super.thumbRect(forBounds:
            bounds, trackRect: rect, value: value)
            .offsetBy(dx: 0/*Set_0_value_to_center_thumb*/, dy: 0)
    }

}
