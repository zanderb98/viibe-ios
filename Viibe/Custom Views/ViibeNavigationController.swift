//
//  ViibeNavigationController.swift
//  Venu
//
//  Created by Zander Bobronnikov on 10/2/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit

class ViibeNavigationController: UINavigationController {
    
    func show(vc: UIViewController) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
