//
//  RoundCornerView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/13/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class RoundCornerView: UIView {
    var radius: CGFloat = 5
    var corners: UIRectCorner = [.topRight,.topLeft]
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        DispatchQueue.main.async {
            self.roundCorners(corners: self.corners, radius: self.radius)
            self.layer.masksToBounds = true
        }
        
        self.layoutIfNeeded()
    }
}
