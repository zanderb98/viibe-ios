//
//  DashedLine.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/11/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class DashedLine: UIView {
    
    override func draw(_ rect: CGRect) {
        let  path = UIBezierPath()
        let  p0 = CGPoint(x: 0,
                          y: bounds.height / 2)
        path.move(to: p0)
        
        let  p1 = CGPoint(x: bounds.width,
                          y: bounds.height / 2)
        path.addLine(to: p1)
        
        let  dashes: [ CGFloat ] = [ 8.0, 6.0 ]
        path.setLineDash(dashes, count: dashes.count, phase: 0)
        
        path.lineWidth = 8.0
        path.lineCapStyle = .butt
        UIColor(hex: "bbbbbb").set()
        path.stroke()
    }
}
