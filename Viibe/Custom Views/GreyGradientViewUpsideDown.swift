//
//  GreyGradientViewUpsideDown.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/14/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class GreyGradientViewUpsideDown: UIView {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            UIColor.init(colorLiteralRed: 242, green: 242, blue: 242, alpha: 0).cgColor,
            UIColor.init(colorLiteralRed: 242, green: 242, blue: 242, alpha: 0.95).cgColor
        ]
    }
}

