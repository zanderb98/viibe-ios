//
//  RoundCornerView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class RoundTopCornersView: UIView {
    var radius: CGFloat = 5
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        DispatchQueue.main.async {
            self.roundCorners(corners: [.topRight,.topLeft], radius: self.radius)
            self.layer.masksToBounds = true
        }
        
        self.layoutIfNeeded()
    }
}
