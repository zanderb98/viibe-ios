//
//  OutsideView.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class OutsideView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let view = super.hitTest(point, with: event) {
            return view
        }
        
        if clipsToBounds || isHidden || alpha == 0 {
            return nil
        }
        
        for subview in subviews.reversed() {
            let subPoint = subview.convert(point, from: self)
            if let result = subview.hitTest(subPoint, with: event) {
                return result
            }
        }
        
        return nil
    }
}
