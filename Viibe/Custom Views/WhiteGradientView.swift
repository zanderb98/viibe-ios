//
//  WhiteGradientView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/8/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class WhiteGradientView: UIView {
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            UIColor.init(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.75).cgColor,
            UIColor.init(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
    }
}
