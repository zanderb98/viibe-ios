//
//  GradientView.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    var gradientLayer: CAGradientLayer!
    
    @IBInspectable
    public var startColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.75) {
        didSet {
            if (gradientLayer.colors != nil) {
                gradientLayer.colors![0] = startColor.cgColor
            }
        }
    }
    
    @IBInspectable
    public var endColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            if (gradientLayer.colors != nil) {
                gradientLayer.colors![1] = endColor.cgColor
            }
        }
    }
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            startColor.cgColor,
            endColor.cgColor
        ]
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            startColor.cgColor,
           endColor.cgColor
        ]
    }
}
