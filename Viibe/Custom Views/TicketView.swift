//
//  TicketView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 3/11/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class TicketView: OutsideView {
    
    func mask(framesToCutOut: [CGRect]) {
        let layer = CAShapeLayer()
        let path = CGMutablePath()
        
        layer.frame = self.bounds
        path.addRect(CGRect(x: -1, y: framesToCutOut[0].minY, width: bounds.width + 2, height: framesToCutOut[0].height))
        
        for value in framesToCutOut {
            path.addEllipse(in: value)
        }
        
        layer.path = path
        layer.fillRule = kCAFillRuleEvenOdd
        
        self.layer.mask = layer
    }
}
