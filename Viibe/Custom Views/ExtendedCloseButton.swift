//
//  SilvermistAlertBackgroundView.swift
//  Daffodil
//
//  Created by Chengshi Zhang on 10/7/16.
//  Copyright © 2016 EF Education First. All rights reserved.
//

import UIKit

class ExtendedCloseButton: UIButton {
    
    // Extends the touch area of the alert's close button by 10 pixels around it
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - 10,
            y: self.bounds.origin.y - 10,
            width: self.bounds.width + 20,
            height: self.bounds.width + 20
        )
        return newBound.contains(point)
    }
}
