//
//  Message.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/16/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class Message: Object {
    dynamic var userID: String = ""
    dynamic var messageID: String = ""
    dynamic var text: String = ""
    dynamic var date: Date = Date()
    dynamic var isHost: Bool = false
    dynamic var atParty: Bool = false
    dynamic var displayName: String = ""
    
    func setup(_ json: [String: Any]) {
        if let userID = json["UserID"] as? String {
            self.userID = userID
        }
        
        if let messageID = json["MessageID"] as? String {
            self.messageID = messageID
        }
        
        if let text = json["Text"] as? String {
            self.text = text
        }
        
        if let atParty = json["AtParty"] as? Bool {
            self.atParty = atParty
        }
        
        if let isHost = json["IsHost"] as? Bool {
            self.isHost = isHost
        }
        
        if let displayName = json["DisplayName"] as? String {
            self.displayName = displayName
        }
        
        if let date = json["Date"] as? Double {
            self.date = Date(timeIntervalSince1970: date)
        }
    }
    
    func setup(_ message: vMessage) {
        self.userID = message.userID
        self.messageID = message.messageID
        self.text = message.text
        self.date = message.date
        self.atParty = message.atParty
        self.isHost = message.isHost
        self.displayName = message.displayName
    }
}
