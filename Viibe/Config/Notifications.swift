//
//  Notifications.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 11/15/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import UIKit

class Notifications {
    static let PartiesUpdated = Notification.Name("PartiesUpdated")
    static let PostsUpdated = Notification.Name("PostsUpdated")
    static let UserUpdated = Notification.Name("UserUpdated")
    static let EnteredForeground = Notification.Name("EnteredForeground")
    static func ticketChecked(partyID: String) -> Notification.Name {
        return Notification.Name(partyID + "TicketChecked")
    }
}
