//
//  Env.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 2/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit

class Env {
    
    static var iPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}
