//
//  Config.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 2/19/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

class Config {
    #if DEBUG
    let debug = true
    #else
    let debug = false
    #endif
    
    static let shared = Config()
    
    //~=~=~=~=~=~=~=~=~
    //Keys
    //~=~=~=~=~=~=~=~=~
    
    let googleMapsKey = "AIzaSyCfZnzrMvhNyDUTNX9_x42fFM5LsgStKd4"
    
    let googlePlacesKey = "AIzaSyBT_LqV9w5k8hEvNRS51CqgpJoxb9iUBYA"
    
    let bugfenderKey = "42ycNRo5xCNycTMY3Qim0PZ4AVLo2GPd"
    
    let plaidPublicKey = "41164524988a9bfbb0b287aca10015"
    
    let mapboxAccessToken = "pk.eyJ1IjoiemFuZGVyYjk4IiwiYSI6ImNqN3hmbTZoOTVxa2UycXQ2bXZ6Z2NkeDIifQ.A8hts8k2nc9b0N9kvgL6Cg"
    
    let googleClientID = "760188884306-rcd3kl8pq8vusjtpgq1tif6a81kv12tp.apps.googleusercontent.com"
    
    let googleURLScheme = "com.googleusercontent.apps.760188884306-rcd3kl8pq8vusjtpgq1tif6a81kv12tp"
    
    var stripePublicKey: String {
        return debug ? "pk_test_sXQMd52GQPQqQrUplDeSyJff" : "pk_live_xWUR1kbb3mhQzbRw49QRPqLc"
    }
    
    //~=~=~=~=~=~=~=~=~
    //URLs
    //~=~=~=~=~=~=~=~=~
    
    var baseHerokuURL: String {
        return debug ? "https://viibe-dev.herokuapp.com" : "https://feeltheviibe.herokuapp.com"
    }
    
    let viibeTermsURL = "http://www.feeltheviibe.com/tos"
    
    let viibeEulaURL = "http://www.feeltheviibe.com/eula"
    
    func geocodeApiURL(lat: Double, lng: Double) -> String {
        return "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(lng)&key=\(googleMapsKey)"
    }
    
    //~=~=~=~=~=~=~=~=~
    //User URLs
    //~=~=~=~=~=~=~=~=~
    
    var loginURL: String {
        return baseHerokuURL + "/users/login"
    }
    
    var signupURL: String {
        return baseHerokuURL + "/users/signup"
    }
    
    var updateUserURL: String {
        return baseHerokuURL + "/users/update"
    }
    
    var setRatioURL: String {
        return baseHerokuURL + "/users/setratio"
    }
    
    var withdrawURL: String {
        return baseHerokuURL + "/users/withdraw"
    }
    
    var followURL: String {
        return baseHerokuURL + "/users/follow"
    }
    
    var setUsernameURL: String {
        return baseHerokuURL + "/users/setusername"
    }
    
    var requestPasswordURL: String {
        return baseHerokuURL + "/password/requestreset"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Payments URLs
    //~=~=~=~=~=~=~=~=~
    
    var setBankTokenURL: String {
        return baseHerokuURL + "/payments/setbanktoken"
    }
    
    var setCardURL: String {
        return baseHerokuURL + "/payments/setcard"
    }
    
    var removeCardURL: String {
        return baseHerokuURL + "/payments/removecard"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Ticket URLs
    //~=~=~=~=~=~=~=~=~
    
    var checkTicketURL: String {
        return baseHerokuURL + "/tickets/check"
    }
    
    var createTicketURL: String {
        return baseHerokuURL + "/tickets/create"
    }
    
    var reserveTicketURL: String {
        return baseHerokuURL + "/tickets/reserve"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Party URLs
    //~=~=~=~=~=~=~=~=~
    
    var getPartiesURL: String {
        return baseHerokuURL + "/parties/get"
    }
    
    var getHostsURL: String {
        return baseHerokuURL + "/hosts/get"
    }
    
    var partyEstimateURL: String {
        return baseHerokuURL + "/parties/estimate"
    }
    
    var hostPartyURL: String {
        return baseHerokuURL + "/parties/host"
    }
    
    var editPartyURL: String {
        return baseHerokuURL + "/parties/edit"
    }
    
    var closePartyURL: String {
        return baseHerokuURL + "/parties/close"
    }
    
    var openPartyURL: String {
        return baseHerokuURL + "/parties/open"
    }
    
    var leavePartyURL: String {
        return baseHerokuURL + "/parties/leave"
    }
    
    var addBouncerURL: String {
        return baseHerokuURL + "/parties/addbouncer"
    }
    
    var sendMessageURL: String {
        return baseHerokuURL + "/chat/send"
    }
    
    var reportMessageURL: String {
        return baseHerokuURL + "/chat/report"
    }
    
    var litFeedbackURL: String {
        return baseHerokuURL + "/parties/litfeedback"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Feed URLs
    //~=~=~=~=~=~=~=~=~
    var feedUserURL: String {
        return baseHerokuURL + "/feed/user"
    }
    
    var getFeedURL: String {
        return baseHerokuURL + "/feed/get"
    }
    
    var newPostURL: String {
        return baseHerokuURL + "/feed/create"
    }
    
    var newEventURL: String {
        return baseHerokuURL + "/feed/event"
    }
    
    var sendCommentURL: String {
        return baseHerokuURL + "/feed/comment"
    }
    
    var reportCommentURL: String {
        return baseHerokuURL + "/feed/reportComment"
    }
    
    var reportPostURL: String {
        return baseHerokuURL + "/feed/report"
    }
    
    var blockUserURL: String {
        return baseHerokuURL + "/feed/blockuser"
    }
    
    var feedLikeURL: String {
        return baseHerokuURL + "/feed/like"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Devices URLs
    //~=~=~=~=~=~=~=~=~
    
    var registerDeviceURL: String {
        return baseHerokuURL + "/devices/register"
    }
    
}
