//
//  Constants.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/30/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation

class Constants {
    static let NoConnectionTitle = "No Connection"
    static let NoConnectionMessage = "Check your internet connection and try again."
    
    static let FavoritesFilter = "ViibeFilter.FAVORITES"
    static let ViibeTicketsFilter = "ViibeFilter.VIIBETICKETS"
    static let DistanceFilter = "ViibeFilter.MAXDISTANCE"
    static let DateFilter = "ViibeFilter.Date"
    
    static let FirstListLoad = "Viibe.FirstListLoad"

}
