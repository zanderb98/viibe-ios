//
//  Party.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class Party: Object {
    @objc dynamic var name = ""
    @objc dynamic var desc = ""
    @objc dynamic var city = ""
    
    @objc dynamic var lit = 0 // 0 - 100
    @objc dynamic var crowed = 0 // 0 - 100
    @objc dynamic var id = ""
    @objc dynamic var date = Date()
    @objc dynamic var endDate = Date()
    @objc dynamic var favorite = false
    
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var lng: Double = 0.0
    @objc dynamic var address = ""
    
    @objc dynamic var malePrice: Double = 0.0
    @objc dynamic var femalePrice: Double = 0.0
    
    @objc dynamic var maleGuests = 0
    @objc dynamic var femaleGuests = 0
    
    @objc dynamic var capacity = 0
    @objc dynamic var chatEnabled = true
    
    @objc dynamic var host: Host?
    
    @objc dynamic var isPastParty = false // If in users past parties
    @objc dynamic var isMyParty = false //If in hosted parties
    @objc dynamic var isBouncedParty = false //If in bounced parties
    @objc dynamic var isReserved = false //If in reserved tickets
    
    let tickets = List<Ticket>()
    let comments = List<Comment>()
    let images = List<PartyImage>()
    
    @objc dynamic var open = false
    @objc dynamic var hasBeenClosed = false
    @objc dynamic var lastTimeClosed: Date?
    
    //Only for past parties
    @objc dynamic var dateEntered: Date?
    @objc dynamic var price: Double = 0.0
    
    //Only for reserved parties
    @objc dynamic var key: String = ""
    @objc dynamic var reservedTicketID: String = ""
    
    //Only for hosted parties
    @objc dynamic var prophit: Double = 0
    let analytics = List<PartyAnalytics>()
    
    //For hosted or bounced parties
    @objc dynamic var password: String = ""
    
    @objc dynamic var bouncerCode: String = ""
    
    func setup(json: [String: Any]) {
        if isPastParty {
            self.id = "p\(json["_id"] as! String)"
        } else if isReserved {
            self.id = "r\(json["_id"] as! String)"
        } else {
            self.id = json["_id"] as! String
        }
        
        self.name = json["Name"] as! String
        self.desc = json["Description"] as! String
        
        self.lit = Int(json["Lit"] as! Double)
        self.crowed = json["Crowded"] as! Int
        
        self.lat = ((json["Location"] as! [String: Any])["coordinates"] as! [Double])[1]
        self.lng = ((json["Location"] as! [String: Any])["coordinates"] as! [Double])[0]
        
        self.city = Util.closestCity(lat: lat, lng: lng)
        
        if let address = json["Address"] as? String {
            self.address = address
        }
        
        self.maleGuests = json["MaleGuests"] as! Int
        self.femaleGuests = json["FemaleGuests"] as! Int
        
        if let lastClosed = json["LastTimeClosed"] as? Double {
            self.hasBeenClosed = lastClosed != 0.0
        }
        
        if let op = json["Open"] as? Bool {
            self.open = op
        }
        
        if let cap = json["Capacity"] as? Int {
            self.capacity = cap
        }
        
        if let dateInterval = json["Date"] as? Double {
            self.date = Date(timeIntervalSince1970: dateInterval)
        }
        
        if let endDateInterval = json["EndDate"] as? Double {
            self.endDate = Date(timeIntervalSince1970: endDateInterval)
        }
        
        if let lastClosed = json["LastTimeClosed"] as? Double {
            self.lastTimeClosed = Date(timeIntervalSince1970: lastClosed)
        }
        
        if let dateEnteredInt = json["DateEntered"] as? Double {
            self.dateEntered = Date(timeIntervalSince1970: dateEnteredInt)
        }
        
        if let price = json["Price"] as? Double {
            self.price = price
        }
        
        if let prophitVal = json["Prophit"] as? Double {
            self.prophit = prophitVal
        }
        
        if let pass = json["Password"] as? String {
            self.password = pass
        }
        
        if let code = json["BouncerCode"] as? String {
            self.bouncerCode = code
        }
        
        if let k = json["Key"] as? String {
            self.key = k
        }
        
        if let tID = json["ReservedTicketID"] as? String {
            self.reservedTicketID = tID
        }
        
        if let enabled = json["ChatEnabled"] as? Bool {
            self.chatEnabled = enabled
        }
        
        if let hostID = json["Host"] as? String {
            self.host = Util.getHost(hostID)
        }
        
        if let analyticsArr = json["Analytics"] as? [[String: Any]] {
            for analytic in analyticsArr {
                let a = PartyAnalytics()
                a.setup(json: analytic)
                analytics.append(a)
            }
        }
        
        if let ticketsArr = json["Tickets"] as? [[String: Any]] {
            for ticket in ticketsArr {
                let t = Ticket()
                t.setup(json: ticket)
                tickets.append(t)
            }
        }
        
        if let commentsArr = json["Comments"] as? [[String: Any]] {
            for cmt in commentsArr {
                let c = Comment()
                c.setup(cmt)
                comments.append(c)
            }
        }
        
        if let imagesArr = json["Images"] as? [String] {
            for url in imagesArr {
                let img = PartyImage()
                img.setup(url)
                images.append(img)
            }
        }
    }
    
    func setup(_ party: vParty) {
        self.id = party.id
        self.name = party.name
        self.city = party.city
        self.desc = party.desc
        self.lit = party.lit
        self.crowed = party.crowded
        self.date = party.date
        self.endDate = party.endDate
        self.lat = party.lat
        self.lng = party.lng
        self.address = party.address
        self.malePrice = party.malePrice
        self.femalePrice = party.femalePrice
        self.maleGuests = party.maleGuests
        self.femaleGuests = party.femaleGuests
        
        self.capacity = party.capacity
        self.chatEnabled = party.chatEnabled
        
        self.open = party.open
        self.hasBeenClosed = party.hasBeenClosed
        self.lastTimeClosed = party.lastTimeClosed
        
        self.dateEntered = party.dateEntered
        self.price = party.price
        
        self.prophit = party.prophit
        
        self.isPastParty = party.isPastParty
        self.isMyParty = party.isMyParty
        self.isBouncedParty = party.isBouncedParty
        self.isReserved = party.isReserved
        
        self.key = party.key
        self.reservedTicketID = party.reservedTicketID
        self.password = party.password

        let realm = try! Realm()
        
        if let oldHost = party.host, let rHost = realm.objects(Host.self).filter({ (h) -> Bool in
            return h.id == oldHost.id
        }).first {
            self.host = rHost
        }
        
        self.analytics.removeAll()
        for analytic in party.analytics {
            let a = PartyAnalytics()
            a.setup(analytic)
            
            self.analytics.append(a)
        }
        
        self.tickets.removeAll()
        for ticket in party.tickets {
            let t = Ticket()
            t.setup(ticket)
            
            self.tickets.append(t)
        }
        
        self.comments.removeAll()
        for cmt in party.comments {
            let c = Comment()
            c.setup(cmt)
            
            self.comments.append(c)
        }
        
        self.images.removeAll()
        for image in party.images {
            let img = PartyImage()
            img.setup(image)
            
            self.images.append(img)
        }
    }
    
    func copyImages(from: vParty) {
        if let oldHost = from.host,
            let proPic = oldHost.profilePicture {
            if oldHost.profilePictureURL == host!.profilePictureURL {
                host!.setImage(img: proPic)
            }
        }
        
        for image in self.images {
            for old in from.images {
                if image.url == old.url {
                    if let oldImg = old.image {
                        image.setImage(img: oldImg)
                        break
                    }
                }
            }
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
