import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import MaterialComponents
import Alamofire

class RatioCardController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var groupTitle: UILabel!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var confirmGroupButton: UIView!
    @IBOutlet weak var confirmGroupText: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var maleCard: UIView!
    @IBOutlet weak var femaleCard: UIView!
    @IBOutlet weak var malePicker: UIPickerView!
    @IBOutlet weak var femalePicker: UIPickerView!
    var alert: RatioCard?
    var user: vUser?
    var close: (() -> Void)?
    
    var malesInGroup = 0, femalesInGroup = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        user = Util.getCurrentUserUnmanaged()
        
        malesInGroup = user!.gender ? 1 : 0
        femalesInGroup = user!.gender ? 0 : 1
    
        background.setCornerRadius(radius: 5)
        background.addDropShadow(offset: CGSize(width: 0, height: -1))
        background.layer.shadowOpacity = 0.6
        
        maleCard.setCornerRadius(radius: 5)
        maleCard.addDropShadow()
        
        femaleCard.setCornerRadius(radius: 5)
        femaleCard.addDropShadow()
        
        confirmGroupButton.setCornerRadius(radius: 5)
        confirmGroupButton.addDropShadow()
        
        malePicker.dataSource = self
        malePicker.delegate = self
        
        femalePicker.dataSource = self
        femalePicker.delegate = self
        
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { _ in
            self.malePicker.selectRow(self.user!.gender ? self.user!.malesInGroup - 1 : self.user!.malesInGroup, inComponent: 0, animated: true)
            self.femalePicker.selectRow(!self.user!.gender ? self.user!.femalesInGroup - 1 : self.user!.femalesInGroup, inComponent: 0, animated: true)
        }
        
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(RatioCardController.confirmedButtonPressed))
        confirmGroupButton.addGestureRecognizer(rec)
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(RatioCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RatioCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func confirmedButtonPressed() {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }

        let params = ["UserID": user.id, "Males": malesInGroup, "Females": femalesInGroup,
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.setRatioURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let success = response.result.value as? Bool {
                if (success) {
                    let realm = try! Realm()
                    let managedUser = Util.getCurrentUser(realm)
                
                    try! realm.write {
                        managedUser?.malesInGroup = self.malesInGroup
                        managedUser?.femalesInGroup = self.femalesInGroup
                    
                        managedUser?.lastTimeGroupSet = Date()
                    }
                
                    if self.close != nil { self.close!() }
                } else {
                    Util.defaultAlert(self, title: "Uh Oh...", message: "Failed to set ratio.")
                }
            } else {
                Util.defaultAlert(self, title: "Uh Oh...", message: "Failed to set ratio.")
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == malePicker) {
            return "\(user!.gender ? row + 1 : row)"
        } else {
            return "\(user!.gender ? row : row + 1)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView == malePicker) {
            return user!.gender ? 8 - femalesInGroup : 9 - femalesInGroup
        } else {
            return user!.gender ? 9 - malesInGroup : 8 - malesInGroup
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == malePicker) {
            malesInGroup = (user!.gender ? row + 1 : row)
            
            let females = femalePicker.selectedRow(inComponent: 0)
            let currVal = (user!.gender ? females : females + 1)
            if (currVal > (8 - malesInGroup)) {
                femalePicker.selectRow(user!.gender ? 8 - malesInGroup : 7 - malesInGroup, inComponent: 0, animated: true)
                femalesInGroup = 8 - malesInGroup
            }
        } else {
            femalesInGroup = (user!.gender ? row : row + 1)
            
            let males = malePicker.selectedRow(inComponent: 0)
            let currVal = (user!.gender ? males + 1 : males)
            if (currVal > (8 - femalesInGroup)) {
                malePicker.selectRow(!user!.gender ? 8 - femalesInGroup : 7 - femalesInGroup, inComponent: 0, animated: true)
                malesInGroup = 8 - femalesInGroup
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show(vc: UIViewController, close: @escaping () -> Void) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        self.close = close
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let views: [UIView] = [background, maleCard, femaleCard, malePicker, femalePicker, confirmGroupButton]
        
        var found = false
        for touch in touches {
            if (views.contains(touch.view!)) {
                found = true
                break
            }
        }
        
        if (!found) {
            let alertController = MDCAlertController(title: "Are you sure?", message: "You didn't confirm your group. To set your group for the night, press \"Confirm Group\"")
            let action = MDCAlertAction(title:"Cancel") { (action) in }
            
            let action2 = MDCAlertAction(title:"I'm Sure") { (action) in
                self.alert?.hide()
            }
            
            alertController.addAction(action2)
            alertController.addAction(action)
            present(alertController, animated:true, completion: nil)
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }

}
