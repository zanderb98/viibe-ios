//
//  SilvermistEditAlert.swift
//  EditAlertTest
//
//  Created by Zander Bobronnikov on 6/20/17.
//  Copyright © 2017 GoAheadTours. All rights reserved.
//

import Foundation
import UIKit

class PastPartiesCard {
    var controller: PastPartiesCardController
    var blurEffectView: UIVisualEffectView?
    var inVC: UIViewController?
    
    init() {
        controller = PastPartiesCardController(nibName: "PastPartiesCardController", bundle: nil)
        controller.alert = self
    }
    
    func show(_ inVC: UIViewController) {
        addBlurEffect(vc: controller)
        controller.show(vc: inVC)
        
        self.inVC = inVC
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, animations: {
            self.blurEffectView?.alpha = 0
        }, completion: {
            (_) in
            self.blurEffectView!.removeFromSuperview()
        })
        
        controller.hide()
    }
    
    fileprivate func addBlurEffect(vc: UIViewController) {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        if vc.navigationController == nil {
            self.blurEffectView!.frame = vc.view.bounds
        } else {
            self.blurEffectView!.frame = vc.navigationController!.view.bounds
        }
        
        self.blurEffectView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView!.alpha = 0
        
        if vc.navigationController == nil {
            vc.view.insertSubview(self.blurEffectView!, at: 0)
        } else {
            vc.navigationController?.view.addSubview(self.blurEffectView!)
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blurEffectView?.alpha = 0.85
        }, completion: {
            (_) in
        })
    }
}
