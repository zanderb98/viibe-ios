//
//  MessageCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/16/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Alamofire

class MessageCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var dropdown: UIImageView!
    
    var vc: UIViewController?
    var msg: vMessage?
    var partyID: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dropdown.image = #imageLiteral(resourceName: "dropdown-arrow").withRenderingMode(.alwaysTemplate)
        dropdown.tintColor = UIColor(hex: "#777777")
    }
    
    func setup(_ msg: vMessage, partyID: String, vc: UIViewController) {
        self.vc = vc
        self.msg = msg
        self.partyID = partyID
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let nameStr = user.id.hash() == msg.userID ? "You" : msg.displayName
        date.text = Util.messageTimestamp(date: msg.date)
        
        if msg.isHost {
            name.attributedText = NSMutableAttributedString()
                .normal("◎  ", fontSize: 16, color: .orange)
                .normal("Host", font: UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold), color: .black)
        } else if msg.atParty {
            name.attributedText = NSMutableAttributedString()
                .normal("◎  ", fontSize: 16, color: UIColor(hex: "11AF50"))
                .normal(nameStr, font: UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold), color: .black)
        } else {
            name.attributedText = NSMutableAttributedString()
                .normal("◎  ", fontSize: 16, color: UIColor(hex: "CCCCCC"))
                .normal(nameStr, font: UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold), color: .black)
        }
        
        let nameW = name.text!.width(font: UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold))
        
        var paddingStr = ""
        while paddingStr.width(font: UIFont.systemFont(ofSize: 16)) < nameW + 6 {
            paddingStr += " "
        }
        
        let attributedString = NSMutableAttributedString(string:  "\(paddingStr)\(msg.text)")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.1
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        message.attributedText = attributedString;
        
    }
    
    @IBAction func reportPressed(_ sender: Any) {
        if vc == nil || msg == nil { return }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        
        let reportAction: UIAlertAction = UIAlertAction(title: "Report", style: .default) { action -> Void in
            
            let params = ["PartyID": self.partyID!, "MessageID": self.msg!.messageID, "UserID": user.id]
            
            Alamofire.request(Config.shared.reportMessageURL, method: .post, parameters: params)
            Util.defaultAlert(self.vc!, title: "Message Reported Successfully")
        }
        
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(reportAction)
        vc!.present(actionSheet, animated: true, completion: nil)
    }
}
