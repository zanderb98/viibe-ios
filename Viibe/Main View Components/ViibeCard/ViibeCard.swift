//
//  SilvermistEditAlert.swift
//  EditAlertTest
//
//  Created by Zander Bobronnikov on 6/20/17.
//  Copyright © 2017 GoAheadTours. All rights reserved.
//

import Foundation
import UIKit

class ViibeCard {
    var controller: ViibeCardController
    var blurEffectView: UIVisualEffectView?
    var inVC: UIViewController?
    
    init() {
        controller = ViibeCardController(nibName: "ViibeCardController", bundle: nil)
        controller.alert = self
    }
    
    func setUnfavorited(_ unfavorited: @escaping () -> Void) {
        controller.unfavorited = unfavorited
    }
    
    func show(_ inVC: UIViewController, newControllerVC: UIViewController? = nil, party: vParty, type: String = "", selected: vTicket? = nil, reloadDetails: (() -> Void)? = nil) {
        addBlurEffect(vc: controller)
        
        if let newVC = newControllerVC {
            controller.reloadDetails = reloadDetails
            controller.show(vc: inVC, newControllerVC: newVC, party: party, type: type, selected: selected)
        } else {
            controller.reloadDetails = reloadDetails
            controller.show(vc: inVC, newControllerVC: inVC, party: party, type: type, selected: selected)

        }
        
        self.inVC = inVC
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, animations: {
            self.blurEffectView?.alpha = 0
        }, completion: {
            (_) in
            self.blurEffectView!.removeFromSuperview()
        })
        
        controller.hide()
    }
    
    fileprivate func addBlurEffect(vc: UIViewController) {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        if vc.navigationController == nil {
            self.blurEffectView!.frame = vc.view.bounds
        } else {
            self.blurEffectView!.frame = vc.navigationController!.view.bounds
        }
        
        self.blurEffectView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView!.alpha = 0
        
        if vc.navigationController == nil {
            vc.view.addSubview(self.blurEffectView!)
        } else {
            vc.navigationController?.view.addSubview(self.blurEffectView!)
        }
        
        blurEffectView!.superview?.sendSubview(toBack: blurEffectView!)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.blurEffectView?.alpha = 0.85
        }, completion: {
            (_) in
        })
    }
}
