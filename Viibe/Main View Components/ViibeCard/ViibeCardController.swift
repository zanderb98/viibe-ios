import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import Alamofire
import Lottie
import CoreLocation
import CHIPageControl
import MaterialComponents
import MWPhotoBrowser
import SDWebImage

class ViibeCardController: UIViewController, UIScrollViewDelegate, UITableViewDataSource, MWPhotoBrowserDelegate {
    
    @IBOutlet weak var topSegment: RoundTopCornersView!
    @IBOutlet weak var middleSegment: TicketView!
    @IBOutlet weak var middleSegmentNoNotch: UIView!
    @IBOutlet weak var bottomSegment: UIView!
    
    @IBOutlet weak var ticketDesc: UILabel!
    @IBOutlet weak var ticketHeader: RoundTopCornersView!
    @IBOutlet weak var ticketPriceLabel: UILabel!
    @IBOutlet weak var qrCode: UIImageView!
    @IBOutlet weak var qrLoading: UIActivityIndicatorView!
    @IBOutlet weak var estimatedPrice: UILabel!
    @IBOutlet weak var litLabel: UILabel!
    @IBOutlet weak var crowdedLabel: UILabel!
    @IBOutlet weak var litNoTix: UILabel!
    @IBOutlet weak var crowdedNoTix: UILabel!
    @IBOutlet weak var pager: UIScrollView!
    @IBOutlet weak var pageControl: CHIPageControlAleppo!
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var partyTime: UILabel!
    @IBOutlet weak var getTicketsButton: UIView!
    @IBOutlet weak var getTicketsLabel: UILabel!
    @IBOutlet weak var navigateButton: UIView!
    @IBOutlet weak var uberButton: UIView!
    @IBOutlet weak var uberImage: UIImageView!
    @IBOutlet weak var crowdedMeter: MBCircularProgressBarView!
    @IBOutlet weak var litMeter: MBCircularProgressBarView!
    @IBOutlet weak var metersContainer: UIView!
    @IBOutlet weak var background: TicketView!
    @IBOutlet weak var ticketContainer: UIView!
    @IBOutlet weak var ticketTypeCard: UIView!
    @IBOutlet weak var ticketType: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var reservedTicketDesc: UILabel!
    @IBOutlet weak var reservedPrice: UILabel!
    @IBOutlet weak var reservedPriceCard: UIView!
    @IBOutlet weak var reserveTicketsContainer: UIView!
    @IBOutlet weak var reserveDate: UILabel!
    @IBOutlet weak var reserveTime: UILabel!
    @IBOutlet weak var reserveAge: UILabel!
    @IBOutlet weak var reserveSurge: UILabel!
    @IBOutlet weak var reserveHostedBy: UILabel!
    @IBOutlet weak var reserveTicketsLeft: UILabel!
    @IBOutlet weak var chatTitle: UILabel!
    @IBOutlet weak var chatSubtitle: UILabel!
    var reserveHeight: NSLayoutConstraint?
    var litSlider: ViibeSlider?
    
    @IBOutlet weak var favoriteButton: ExtendedCloseButton!
    
    @IBOutlet weak var genderCard: UIView!
    @IBOutlet weak var genderImage: UIImageView!

    var alert: ViibeCard?
    var favPulse: Pulsator?
    
    var descLabel: UILabel?
    var descContainer: UIView?
    var chatContainer: UIView?
    var chatTable: UITableView?
    
    var messageField: UITextField?
    var sendBtn: UIImageView?
    var party: vParty?
    var onTicketScreen = false
    var cardTop: NSLayoutConstraint?
    var getTicketsRight: NSLayoutConstraint?
    var getTicketsReserveRight: NSLayoutConstraint?
    var getTicketsInitialRight: NSLayoutConstraint?
    
    var getTicketBottom: NSLayoutConstraint?
    var getTicketInitialBottom: NSLayoutConstraint?
    var getTicketLeft: NSLayoutConstraint?
    var getTicketInitialLeft: NSLayoutConstraint?
    
    @IBOutlet weak var typeDropdown: UIImageView!
    
    var animView: LOTAnimationView?
    
    var unfavorited: (() -> Void)?
    
    var ticketButtonEnabled = false
    var ticketScanned = false
    var descExpanded = false
    
    var initialPagerHeight: CGFloat = 0
    
    var estPrice: Double = 0;
    var estPriceShown = false
    
    var ticket: vTicket? = nil
    
    var inVC: UIViewController?
    var newControllerVC: UIViewController?
    
    let chatHeight: CGFloat = 240
    
    var pictures: [MWPhoto] = []
    var browser = MWPhotoBrowser()
    
    var centeredPager = false
    var firstPagerSetup = true
    
    var secondsLeft = 0
    var timer: Timer? = nil
    
    var reloadDetails: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        background.sendSubview(toBack: topSegment)
        background.sendSubview(toBack: middleSegment)
        background.sendSubview(toBack: bottomSegment)
        
        topSegment.radius = 10
        ticketHeader.radius = 10
        
        background.layer.cornerRadius = 10
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.shadowOpacity = 0.6
        background.layer.shadowOffset = CGSize(width: 0, height: -1)
        background.layer.shadowRadius = 3
        
        let secondOffset: CGFloat = UIDevice.screenType == .iPhones_5_5s_5c_SE ? 86 : 32
        middleSegment.mask(framesToCutOut: [CGRect(x: -16, y: 0, width: 24, height: 24),
                                     CGRect(x: background.bounds.width - secondOffset, y: 0, width: 24, height: 24)])
        
        getTicketsButton.setCornerRadius(radius: 5)
        navigateButton.setCornerRadius(radius: 5)
        uberButton.setCornerRadius(radius: 5)
        
        favoriteButton.layer.shadowColor = UIColor.black.cgColor
        favoriteButton.layer.shadowOpacity = 0.2
        favoriteButton.layer.shadowOffset = CGSize.zero
        favoriteButton.layer.shadowRadius = 3
        favoriteButton.layer.cornerRadius = 22
        favoriteButton.adjustsImageWhenHighlighted = false
        favoriteButton.imageEdgeInsets = UIEdgeInsetsMake(6,6,6,6)
        
        favPulse = Pulsator()
        favPulse!.animationDuration = 0.55
        favPulse!.radius = 50
        favPulse!.numPulse = 1
        favPulse!.repeatCount = 0
        
        favPulse!.backgroundColor = UIColor.red.cgColor
        favoriteButton.layer.addSublayer(favPulse!)
        favoriteButton.layer.layoutIfNeeded()
        favPulse!.position = CGPoint(x: favPulse!.position.x + 22, y: favPulse!.position.y + 22)
                
        reservedPriceCard.addDropShadow(opacity: 0.22)
        reservedPriceCard.setCornerRadius(radius: 10)
        
        ticketTypeCard.addDropShadow(opacity: 0.22)
        ticketTypeCard.setCornerRadius(radius: 10)
        
        genderCard.addDropShadow(opacity: 0.22)
        genderCard.setCornerRadius(radius: 10)
        
        typeDropdown.image = UIImage(named: "dropdown-arrow")!.withRenderingMode(.alwaysTemplate)
        
        ticketPriceLabel.font = Fonts.fouLight(size: 50)

        self.qrCode.contentMode = .scaleAspectFit
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.getTicketsPressed))
        getTicketsButton.addGestureRecognizer(rec)
        
        let rec2 = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.uberPressed))
        uberButton.addGestureRecognizer(rec2)
        
        let rec3 = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.navigatePressed))
        navigateButton.addGestureRecognizer(rec3)
        
        let typeRec = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.showTicketPicker))
        ticketTypeCard.addGestureRecognizer(typeRec)
        
        cardTop = NSLayoutConstraint(item: background, attribute: .top, relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0)
        getTicketsRight = NSLayoutConstraint(item: getTicketsButton, attribute: .trailing, relatedBy: .equal, toItem: background, attribute: .trailing, multiplier: 1, constant: 8)
        getTicketsReserveRight = NSLayoutConstraint(item: getTicketsButton, attribute: .trailing, relatedBy: .equal, toItem: uberButton, attribute: .leading, multiplier: 1, constant: -1 * 8)
        getTicketBottom = NSLayoutConstraint(item: getTicketsButton, attribute: .bottom, relatedBy: .equal, toItem: background, attribute: .bottom, multiplier: 1, constant: 0)
         getTicketLeft = NSLayoutConstraint(item: getTicketsButton, attribute: .leading, relatedBy: .equal, toItem: background, attribute: .leading, multiplier: 1, constant: -8)
        
        if Config.shared.debug {
            let longTapRec = UILongPressGestureRecognizer(target: self, action: #selector(ViibeCardController.ticketChecked))
            qrCode.addGestureRecognizer(longTapRec)
        }
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(ViibeCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViibeCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatedParties), name: Notifications.PartiesUpdated, object: nil)
    }
    
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(ticketChecked), name: Notifications.ticketChecked(partyID: party!.id), object: nil)
        
        partyName.text = party!.name
        
        reservedPrice.attributedText = NSMutableAttributedString().normal("PAID ", font: Fonts.fou(size: 25)).normal("$\(String(format: "%.2f", party!.price))", font: Fonts.fouMedium(size: 25))
        
        partyTime.text = Util.getDateText(date: party!.date)
        
        favoriteButton.setImage(Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart-full") : #imageLiteral(resourceName: "heart-outline"), for: .normal)
        
        litMeter.value = CGFloat(party!.lit)
        crowdedMeter.value = CGFloat(party!.crowded)
        
        if party!.availableTix.isEmpty || party!.crowded < 15 {
            litMeter.value = 0
            crowdedMeter.value = 0
            
            litMeter.showValueString = false
            litMeter.showUnitString = false
            litNoTix.isHidden = false
            
            crowdedMeter.showValueString = false
            crowdedMeter.showUnitString = false
            crowdedNoTix.isHidden = false
        } else {
            litMeter.showValueString = true
            litMeter.showUnitString = true
            litNoTix.isHidden = true
            
            crowdedMeter.showValueString = true
            crowdedMeter.showUnitString = true
            crowdedNoTix.isHidden = true
        }
        
        let user = Util.getCurrentUserUnmanaged()
        
        if Util.getLoginStatus() == .loggedIn {
            loadEstimatedPrice(user!)
            genderImage.image = (user!.gender ? #imageLiteral(resourceName: "human-male-black") : #imageLiteral(resourceName: "human-female-black")).withRenderingMode(.alwaysTemplate)
        }
        
        setupGetTicketButton(user)
    }
    
    func setupGetTicketButton(_ user: vUser?) {
        if (onTicketScreen) {
            return
        }
        
        if reserveHeight == nil {
            reserveHeight = reserveTicketsLeft.getConstraintByID(id: "reserveHeight")
        }
        
        if party!.availableTix.isEmpty {
            ticketButtonEnabled = false
            reserveHeight?.isActive = true
            
            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "PAY AT VENUE"
        } else if Util.getLoginStatus() == .skipped {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true
            
            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "LOGIN"
        } else if party!.isReserved {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true

            self.getTicketsLabel.text = "VIEW TICKET"
            self.reserveTicketsLeft.text = "\(party!.reserveTixLeft) LEFT"
            self.reserveTicketsLeft.isHidden = false
        } else if party!.isReservable {
            ticketButtonEnabled = true
            reserveHeight?.isActive = false

            self.getTicketsLabel.text = "RESERVE"
            self.reserveTicketsLeft.text = "\(party!.reserveTixLeft) LEFT"
            self.reserveTicketsLeft.isHidden = false
        } else if !user!.creditCardLast4.isEmpty || estPrice == 0.0 {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true

            self.getTicketsLabel.text = "GET TICKET"
            self.getTicketsLabel.textColor = .white
            self.getTicketsButton.backgroundColor = Colors.viibeBlue
        } else {
            ticketButtonEnabled = false
            reserveHeight?.isActive = true

            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "ADD PAYMENT"
        }
    }
    
    var lastPageNum = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var pageNumber = (scrollView.contentOffset.x / scrollView.frame.size.width)
        
        if party!.chatEnabled {
            pageNumber -= 1
        }
        
        if pageNumber == CGFloat(Int(pageNumber)) {
            self.lastPageNum = Int(pageNumber)
        }
        
        if !centeredPager {
            updatePagerHeight(pageNumber: 0)
            pageControl.progress = 1
            return
        } else {
            updatePagerHeight()
        }
        
        pageControl.progress = Double(party!.chatEnabled ? pageNumber + 1 : pageNumber)
        
        if pageNumber <= 0 {
            chatSubtitle.text = party!.name

            chatTitle.alpha = -1 * pageNumber
            partyName.alpha = 1 - chatTitle.alpha
            
            chatSubtitle.alpha = chatTitle.alpha
            partyTime.alpha = partyName.alpha
        } else {
            chatSubtitle.text = "by \(party!.host!.name)"
            
            partyName.alpha = 1
            chatTitle.alpha = 0
            
            chatSubtitle.alpha = pageNumber
            partyTime.alpha = 1 - pageNumber
        }
    }
    
    var lastPagerHeight: CGFloat = 0
    func updatePagerHeight(pageNumber: CGFloat = -1) {
        var pageNumber = pageNumber == -1 ? (pager.contentOffset.x / pager.frame.size.width) - 1 : pageNumber
        
        if !party!.chatEnabled {
            pageNumber += 1
        }
        
        let w = UIScreen.main.bounds.width - 24
        let pagerHeight = pager.getConstraintByID(id: "pagerHeight")!
        
        if pageNumber > 0 {
            let diff = descLabel!.untruncatedHeight + 40 + viewImagesHeight - initialPagerHeight
            
            pagerHeight.constant = max(initialPagerHeight + pageNumber * diff, initialPagerHeight)
            
            self.pager.layoutIfNeeded()
            
            self.descContainer!.frame.size.height = self.initialPagerHeight
            self.descLabel!.frame.origin.x = 0.1 * w
            self.descLabel!.frame.size.width = 0.8 * w
            self.descLabel!.preferredMaxLayoutWidth = 0.8 * w
            self.descLabel!.sizeToFit()
            self.descLabel!.frame.size.width = 0.8 * w
        } else {
            let diff = chatHeight - initialPagerHeight
            pagerHeight.constant = initialPagerHeight - pageNumber * diff
            self.pager.layoutIfNeeded()
        }
        
        self.lastPagerHeight = pagerHeight.constant
    }
    
    var viewImagesHeight: CGFloat = 0
    func setupPager() {
        let w = UIScreen.main.bounds.width - 24
        
        //Doesnt run on parties updated
        if firstPagerSetup {
            pager.delegate = self
            pager.getConstraintByID(id: "metersLeading")?.constant = party!.chatEnabled ? w : 0
            
            metersContainer.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(w * 0.1 - 3)-[lit(\(w * 0.4))]-6-[crowded(\(w * 0.4))]-\(w * 0.1 - 4)-|", options: [], metrics: nil, views: ["crowded": crowdedMeter, "lit": litMeter]))
            
            let metersContainerW = metersContainer.getConstraintByID(id: "metersContainerWidth")!
            metersContainerW.constant = UIScreen.main.bounds.width - 24
            metersContainer.layoutIfNeeded()
            
            initialPagerHeight = metersContainer.frame.height
            let pagerHeight = pager.getConstraintByID(id: "pagerHeight")!
            pagerHeight.constant = initialPagerHeight
            pager.layoutIfNeeded()
            
            //Live Chat
            
            if party!.chatEnabled {
                chatContainer = UIView(frame: CGRect(x: 0, y: 0, width: w, height: chatHeight))
                
                chatTable = UITableView(frame: CGRect(x: 0, y: 0, width: chatContainer!.frame.width, height: chatHeight - 45))
                
                chatTable!.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
                chatTable!.dataSource = self
                chatTable!.allowsSelection = false
                chatTable!.separatorStyle = .none
                chatTable!.rowHeight = UITableViewAutomaticDimension
                chatTable!.estimatedRowHeight = 20
                chatTable!.tableFooterView = Util.footerView(height: 10)
                chatTable!.tableHeaderView = Util.footerView(height: 10)

                chatTable!.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                
                if !party!.comments.isEmpty {
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.chatTable!.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    }
                }
                
                let gradientView = GradientView(frame: CGRect(x: 0, y: 0, width: chatContainer!.frame.width, height: 50))
                gradientView.startColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1 )
                gradientView.endColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0)
                
                messageField?.removeFromSuperview()
                messageField = UITextField(frame: CGRect(x: 12, y: chatHeight - 40, width: chatContainer!.frame.width * 0.85 - 12, height: 40))
                messageField!.placeholder = "Enter a message"
                messageField!.addTarget(self, action: #selector(ViibeCardController.messageTextChanged), for: .editingChanged)
                
                sendBtn = UIImageView(frame: CGRect(x: chatContainer!.frame.width * 0.85, y: chatHeight - 35, width: 25, height: 25))
                sendBtn!.image = #imageLiteral(resourceName: "send").withRenderingMode(.alwaysTemplate)
                sendBtn!.tintColor = UIColor(hex: "EFEFEF")
                sendBtn!.isUserInteractionEnabled = true
                
                let rec = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.chatSendPressed))
                sendBtn!.addGestureRecognizer(rec)
                
                let separator = UIView(frame: CGRect(x: 4, y: chatHeight - 45, width: chatContainer!.frame.width - 24, height: 1.2))
                separator.backgroundColor = UIColor(hex: "DDDDDD")
                
                chatContainer!.addSubview(chatTable!)
                chatContainer!.addSubview(separator)
                chatContainer!.addSubview(messageField!)
                chatContainer!.addSubview(sendBtn!)
                chatContainer!.addSubview(gradientView)
                pager.addSubview(chatContainer!)
            }
        }
        
        if descLabel != nil && party!.desc == descLabel!.text {
            //Description same on party update
            return
        }
        
        descContainer?.removeFromSuperview()
        descContainer = UIView(frame: CGRect(x: party!.chatEnabled ? w * 2 : w, y: 0, width: w, height: pager.frame.height))
        
        let imagesCard = UIView(frame: CGRect(x: w * 0.1 + 8, y: 8, width:  w * 0.4 - 12, height: 25))
        imagesCard.setCornerRadius(radius: 10)
        imagesCard.layer.borderWidth = 1.5
        imagesCard.layer.borderColor = party!.images.isEmpty ? UIColor(hex: "EFEFEF").cgColor : UIColor.black.cgColor
        
        let imagesRec = UITapGestureRecognizer(target: self, action: #selector(viewImagesPressed))
        imagesCard.addGestureRecognizer(imagesRec)
        
        let imagesLabel = UILabel()
        imagesLabel.numberOfLines = 2
        imagesLabel.textAlignment = .center
        imagesLabel.text = "VIEW IMAGES"
        imagesLabel.font = Fonts.fouMedium(size: 15)
        imagesLabel.textColor = party!.images.isEmpty ? UIColor(hex: "EFEFEF") : UIColor.black
        imagesLabel.sizeToFit()
        
        imagesCard.frame.size.height = 1.6 * imagesLabel.frame.height
        viewImagesHeight = imagesCard.frame.height
        imagesCard.addSubview(imagesLabel)
        
        imagesLabel.frame.origin.x = imagesCard.frame.width / 2 - imagesLabel.frame.width / 2
        imagesLabel.frame.origin.y = imagesCard.frame.height / 2 - imagesLabel.frame.height / 2
        
        let hostCard = UIView(frame: CGRect(x: w * 0.5 + 8, y: 8, width:  w * 0.4 - 12, height: 25))
        hostCard.setCornerRadius(radius: 10)
        hostCard.layer.borderWidth = 1.5
        hostCard.layer.borderColor = UIColor.black.cgColor
        
        let hostRec = UITapGestureRecognizer(target: self, action: #selector(hostProfilePressed))
        hostCard.addGestureRecognizer(hostRec)
        
        let hostLabel = UILabel()
        hostLabel.numberOfLines = 2
        hostLabel.textAlignment = .center
        hostLabel.text = "HOST PROFILE"
        hostLabel.font = Fonts.fouMedium(size: 15)
        hostLabel.textColor = .black
        hostLabel.sizeToFit()
        
        hostCard.frame.size.height = 1.6 * hostLabel.frame.height
        hostCard.addSubview(hostLabel)
        
        hostLabel.frame.origin.x = hostCard.frame.width / 2 - hostLabel.frame.width / 2
        hostLabel.frame.origin.y = hostCard.frame.height / 2 - hostLabel.frame.height / 2
        
        descContainer!.addSubview(imagesCard)
        descContainer!.addSubview(hostCard)

        descLabel = UILabel()
        descLabel!.font = Fonts.fou(size: 19)
        descLabel!.numberOfLines = 0
        descLabel!.textColor = UIColor(hex: "333333")
        descLabel!.text = party!.desc.isEmpty ? "No Description" : party!.desc
    
        descLabel!.textAlignment = .center
        descContainer!.addSubview(descLabel!)
    
        descLabel!.frame.origin.y = 35 + imagesLabel.frame.height
        descLabel!.frame.origin.x = 0.1 * w
        descLabel!.frame.size.width = 0.8 * w
        descLabel!.preferredMaxLayoutWidth = 0.8 * w
        descLabel!.sizeToFit()
        descLabel!.frame.size.width = 0.8 * w
        if (descLabel!.frame.height > pager.frame.height - 16) {
            descLabel!.frame.size.height = pager.frame.height - 16
        }
    
        pager.addSubview(descContainer!)
        
        if pager.contentOffset.x != w {
            updatePagerHeight()
        }
        
        if firstPagerSetup {
            pager.contentSize.width = party!.chatEnabled ? w * 3 : w * 2
            
            pageControl.numberOfPages =  party!.chatEnabled ? 3 : 2
            pageControl.isHidden = false
            pager.isScrollEnabled = true
            pager.isPagingEnabled = true
            
            background.getConstraintByID(id: "pagerBottom")?.constant = 15
            background.layoutIfNeeded()
            
            pager.clipsToBounds = true
            self.pager.contentOffset.x =  party!.chatEnabled ? w : 0
            centeredPager = true
            firstPagerSetup = false
        }
    }
    
    func setupImageBrowser() {
        for img in party!.images {
            SDWebImageManager.shared().downloadImage(with: URL(string: img.url), options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                let picture = MWPhoto(image: image)
                self.pictures.append(picture!)
            })
        }
        
        browser.delegate = self
    }
    
    func viewImagesPressed() {
        if party!.images.isEmpty {
            return
        }
        
        if inVC is HostProfileController {
            alert?.hide()
            (inVC as! HostProfileController).openParty = party
        }
        
        self.newControllerVC?.navigationController?.pushViewController(browser, animated: true)
        
        browser = MWPhotoBrowser()
        browser.delegate = self
    }
    
    func hostProfilePressed() {
//        let transition = CATransition()
//        transition.duration = 0.4
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromTop
//        self.inVC?.navigationController!.view.layer.add(transition, forKey: nil)
        
        if inVC! is HostProfileController {
            alert!.hide()
            return
        }
        
        if  let p = party,
            let host = p.host {
            let hostVC = HostProfileController(nibName: "HostProfileController", bundle: nil)
            hostVC.host = host
            self.newControllerVC?.navigationController?.pushViewController(hostVC, animated: true)
        }
    }
    
    func chatSendPressed() {
        guard let u = Util.getCurrentUserUnmanaged(), let p = party else {
            return
        }
        
        if messageField == nil || messageField!.text!.isEmpty {
            return
        }
        
        let params = [
            "PartyID": p.id,
            "UserID": u.id,
            "Text": messageField!.text!,
            "Parents": "[]",
            "ServerVersion": "2"
        ]
        
        Alamofire.request(Config.shared.sendCommentURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let commentJSON = json["Comment"] as? [String: Any] {
                
                let comment = Comment()
                comment.setup(commentJSON)
                
                let realm = try! Realm()
                if let realmParty = realm.objects(Party.self).first(where: { (realmParty) -> Bool in
                    return realmParty.id == self.party!.id
                }) {
                    try! realm.write {
                        realmParty.comments.append(comment)
                    }
                }
                
                self.messageField!.text = ""
                self.view.endEditing(true)
                self.party!.comments.append(vComment(comment))
                self.chatTable!.beginUpdates()
                self.chatTable!.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.chatTable!.endUpdates()
                
                self.sendBtn!.tintColor = UIColor(hex: "DDDDDD")
                
                self.viewWillLayoutSubviews()
                
            } else {
                Util.defaultAlert(self, title: "Failed to Send Comment")
            }
        }
    }
    
    func ticketChecked() {
        if ticketScanned { return }
        ticketScanned = true
        
        let realm = try! Realm()
        if let user = Util.getCurrentUser(realm),
            let partyUnmanaged = party,
            let currParty = realm.objects(Party.self).filter("id == '\(partyUnmanaged.id)'").first {
            try! realm.write {
                user.currentParty = currParty
            }
            
            //Register region 50m circle around party
            LocationManager.shared.atParty(vParty(currParty))
        }
        
        Util.fadeOut(views: [qrCode, qrLoading, ticketContainer, uberButton, navigateButton, partyName, partyTime, ticketHeader, ticketDesc], duration: 0.1)
        Util.fadeIn(views: [middleSegmentNoNotch], duration: 0.1)
    
        animView = LOTAnimationView(name: "success")
        
        animView!.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        animView!.contentMode = .scaleAspectFit
        animView!.frame = view.bounds
        
        view.addSubview(animView!)
        animView?.loopAnimation = false
        
        let welcomeLabel = UILabel()
        welcomeLabel.numberOfLines = 2
        welcomeLabel.textAlignment = .center
        welcomeLabel.text = "WELCOME TO\n\(party!.name.uppercased())!"
        welcomeLabel.font = Fonts.fouBold(size: 25)
        welcomeLabel.sizeToFit()
        welcomeLabel.alpha = 0
        welcomeLabel.center = animView!.center
        view.insertSubview(welcomeLabel, belowSubview: animView!)
        
        let litLabel = UILabel()
        litLabel.textAlignment = .center
        litLabel.textColor = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        litLabel.attributedText = NSMutableAttributedString().normal("How ", font: Fonts.fou(size: 18))
            .normal("Lit",  font: Fonts.fouBold(size: 18)).normal(" Does This Party Look?",  font: Fonts.fou(size: 18))
        litLabel.sizeToFit()
        litLabel.alpha = 0
        litLabel.center = animView!.center
        litLabel.frame.origin.y += (welcomeLabel.frame.height / 2 + 75)
        view.addSubview(litLabel)
        
        litSlider = ViibeSlider()
        litSlider!.alpha = 0
        litSlider!.center = litLabel.center
        let screenWidth = UIScreen.main.bounds.width
        litSlider!.frame.origin.x = screenWidth * 0.2
        litSlider!.frame.size.width = screenWidth * 0.6
        litSlider!.frame.origin.y += (litLabel.frame.height / 2 + 25)
        litSlider!.thumbTintColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        litSlider!.minimumTrackTintColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.75)
        litSlider!.maximumTrackTintColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 0.65)
        litSlider!.minimumValue = 0.0
        litSlider!.maximumValue = 100.0
        litSlider!.value = 50.0
        view.addSubview(litSlider!)
        
        let sliderNotLit = UILabel()
        sliderNotLit.textAlignment = .center
        sliderNotLit.textColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1)
        sliderNotLit.font = Fonts.fou(size: 16)
        sliderNotLit.text = "Not Lit"
        sliderNotLit.sizeToFit()
        sliderNotLit.alpha = 0
        sliderNotLit.frame.origin.x = litSlider!.frame.origin.x + 4
        sliderNotLit.frame.origin.y = litSlider!.frame.origin.y + litSlider!.frame.height + 8
        view.addSubview(sliderNotLit)
        
        let sliderOK = UILabel()
        sliderOK.textAlignment = .center
        sliderOK.textColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1)
        sliderOK.font = Fonts.fou(size: 16)
        sliderOK.text = "OK"
        sliderOK.sizeToFit()
        sliderOK.alpha = 0
        sliderOK.frame.origin.x = litSlider!.frame.origin.x + litSlider!.frame.width / 2 - sliderOK.frame.width / 2
        sliderOK.frame.origin.y = litSlider!.frame.origin.y + litSlider!.frame.height + 8
        view.addSubview(sliderOK)
        
        let sliderLit = UILabel()
        sliderLit.textAlignment = .center
        sliderLit.textColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1)
        sliderLit.font = Fonts.fou(size: 16)
        sliderLit.text = "Lit"
        sliderLit.sizeToFit()
        sliderLit.alpha = 0
        sliderLit.frame.origin.x = litSlider!.frame.origin.x + litSlider!.frame.width - sliderOK.frame.width - 4
        sliderLit.frame.origin.y = litSlider!.frame.origin.y + litSlider!.frame.height + 8
        view.addSubview(sliderLit)
        
        getTicketsButton.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        getTicketsLabel.text = "CONTINUE"
        
        animView?.play(completion: { (_) in
            UIView.animate(withDuration: 0.25, animations: {
                self.animView?.frame.origin.y -= (welcomeLabel.frame.height / 2 + UIScreen.main.bounds.height * 0.1025 + 10)
                welcomeLabel.alpha = 1
                litLabel.alpha = 1
                self.litSlider!.alpha = 1
                sliderNotLit.alpha = 1
                sliderOK.alpha = 1
                sliderLit.alpha = 1
            })
        })
        
    }
    
    func updatedParties() {
        if party == nil { return }
        
        let oldChat = party!.comments
        
        let realm = try! Realm()
        if let partyRealm = realm.objects(Party.self).filter("id == '\(party!.id)'").first {
            party = vParty(partyRealm)
            
            if let reserved = realm.objects(Party.self).filter("id == 'r\(party!.id)'").first {
                party?.isReserved = true
                party?.key = reserved.key
            }
        
            setup()
            setupPager()
        } else {
            alert!.hide()
        }
        
        var rowsAdded: [IndexPath] = []
        let newChat = party!.comments
        
        for i in 0..<newChat.count {
            let cmt = newChat[i]
            
            var found = false
            for old in oldChat {
                if cmt.id == old.id {
                    found = true
                    break
                }
            }
            
            if !found {
                rowsAdded.append(IndexPath(row: newChat.count - i - 1, section: 0))
            }
        }
        
        chatTable?.insertRows(at: rowsAdded, with: .automatic)

    }
    
    func setPartyFavorite() {        
        if Util.isFavorited(party!.id) {
            Util.unfavoriteParty(party!.id)
            
            if unfavorited != nil {
                unfavorited!()
            }
        } else {
            Util.favoriteParty(party!.id)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uberPressed() {
        if party == nil { return }
        
        if uberImage.image == #imageLiteral(resourceName: "close") {
            if let details = self.reloadDetails {
                self.hide()
                details()
                
                return
            }
            
            animateToParty()
            return
        }
        
        let urlStr = "https://m.uber.com/ul/?client_id=U2aHW22C3HAX0ZoNPHZofjCQxGw7s8oj&action=setPickup&pickup=my_location&dropoff[latitude]=\(party!.lat)&dropoff[longitude]=\(party!.lng)&dropoff[nickname]=\(party!.name)".replacingOccurrences(of: " ", with: "%20")

        guard let url = URL(string: urlStr) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func navigatePressed() {
        if party == nil { return }
        
        let urlStr = "http://maps.apple.com/?daddr=\(party!.lat),\(party!.lng)"
        
        guard let url = URL(string: urlStr) else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }

    }
    
    func getTicketsPressed() {
        if !ticketButtonEnabled { return }
        
        if Util.getLoginStatus() == .skipped {
            Util.clearLoginStatus()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            inVC?.navigationController?.pushViewController(vc, animated: true)
            inVC?.navigationController?.viewControllers = [vc]
            
            return
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            Util.defaultAlert(self, title: "Theres a problem...", message: "Log out and back in and try again.")
            return
        }
        
        if (ticketScanned) {
            let params: [String: Any] = ["PartyID": party!.id, "UserID": user.id , "Lit": litSlider!.value, "ServerVersion": "2"]
            Alamofire.request(Config.shared.litFeedbackURL, method: .post, parameters: params)
            
            if let details = self.reloadDetails {
                details()
            }
            
            hide()
            return
        }
        
        if user.creditCardLast4.isEmpty && estPrice > 0 {
            Util.defaultAlert(self, title: "No Payment Method Set", message: "Go to the account page, add a credit/debit card, and try again.")
            return
        }
        
        if !onTicketScreen {
            if !Reachability.isConnectedToNetwork() {
                Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
                return
            }
            
            if (party!.isReservable) {
                if party!.reservableTicketCount > 1 {
                    showTicketPicker()
                } else {
                    for ticket in party!.availableTix {
                        if ticket.isReserveAvailable {
                            self.ticket = ticket
                        }
                    }
                    
                    setReserveDetails()
                    animateToReserve()
                }
            } else {
                if !party!.isReserved && party!.ticketCount > 1 {
                    showTicketPicker()
                } else {
                    self.ticket = party!.availableTix.first!
                        
                    animateToTicket()
                    setTicketDetails()
                    
                    if !party!.isReserved {
                        generateTicket()
                    }
                }
            }
        } else {
            if party!.isReservable {
                let alertController = MDCAlertController(title: "Are you sure?", message: "By pressing reserve below, your card will be charged and a ticket will be reserved for you. ")
                
                let reserveAction = MDCAlertAction(title:"Reserve") { (_) in
                    self.reserveTicket()
                }
                
                alertController.addAction(reserveAction)
                alertController.addAction(MDCAlertAction(title:"Cancel"))
                
                present(alertController, animated:true, completion: nil)
            } else {
                if let details = self.reloadDetails {
                    self.hide()
                    details()
                    
                    return
                }
                
                animateToParty()
            }
        }
    }
    
    @objc func showTicketPicker() {
        if party!.availableTix.count <= 1 {
            return
        }
        
        let actionSheet: UIAlertController = UIAlertController(title: "Pick Ticket Type", message: "This can be changed on the next page", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        actionSheet.addAction(cancelAction)
       
        for ticket in party!.availableTix {
            if party!.isReservable {
                if ticket.isReserveAvailable {
                    let action = UIAlertAction(title: ticket.name, style: .default) { action -> Void in
                        self.ticket = ticket
                        
                        self.setReserveDetails()
                        self.animateToReserve()
                    }
                    
                    actionSheet.addAction(action)
                }
            } else {
                if ticket.isAvailable {
                    let action = UIAlertAction(title: ticket.name, style: .default) { action -> Void in
                        self.ticket = ticket
                        
                        if !self.onTicketScreen {
                            self.animateToTicket()
                        }

                        self.setTicketDetails()
                        self.generateTicket()
                    }
                    
                    actionSheet.addAction(action)
                }
            }
        }
        
        //Present the AlertController
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func setTicketDetails() {
        if party == nil { return }
        let reserved = party!.isReserved
        
        ticketType.text = ticket!.name.uppercased()
        ticketPriceLabel.text = reserved ? ticket!.name.uppercased() : "--"
        ticketPriceLabel.font = Fonts.fouLight(size: reserved ? 35 : 50)
        qrCode.image = reserved ? QRCodeUtil.generateQRCode(string: party!.key) : nil
        qrLoading.isHidden = !onTicketScreen || reserved
        timerLabel.isHidden = reserved
        typeDropdown.isHidden = party!.availableTix.count <= 1
        
         self.timerLabel.attributedText = NSMutableAttributedString().normal("Refreshes in ").bold("120s")
    }
    
    func generateTicket() {
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let params = ["PartyID": party!.id, "UserID": user.id, "TicketID": ticket!.id, "ServerVersion": "2"]
        Alamofire.request(Config.shared.createTicketURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any] {
                self.qrCode.image = QRCodeUtil.generateQRCode(string: json["Key"] as! String)
                self.ticketPriceLabel.text = "$\(String(format: "%.2f", json["Price"] as! Double))"
                self.qrLoading.isHidden = true
                
                self.secondsLeft = 120
                self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (_) in
                    self.secondsLeft -= 1
                    
                    DispatchQueue.main.async {
                        self.timerLabel.attributedText = NSMutableAttributedString().normal("Expires in ").bold("\(self.secondsLeft)s")
                        
                        if self.secondsLeft == 0 {
                            if let details = self.reloadDetails {
                                self.hide()
                                details()
                                
                                return
                            }
                            
                            self.animateToParty()
                            self.timer = nil
                        }
                    }
                })
            } else {
                Util.defaultAlert(self, title: "Uh Oh...", message: "There was a problem communicating with our server. Please try again.", action: {
                    self.animateToParty()
                })
            }
        }
    }
    
    func loadEstimatedPrice(_ user: vUser) {
        if party == nil {
            return
        }
        
        var ticketID = ""
        var minPrice = 99999999.0
        
        for ticket in party!.availableTix {
            if user.gender && ticket.malePrice < minPrice {
                ticketID = ticket.id
                minPrice = ticket.malePrice
            } else if !user.gender && ticket.femalePrice < minPrice {
                ticketID = ticket.id
                minPrice = ticket.femalePrice
            }
        }
        
        let params = ["PartyID": party!.id, "Gender": (user.gender ? "male" : "female"), "TicketID": ticketID, "ServerVersion": "2"]
        Alamofire.request(Config.shared.partyEstimateURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any] {
                self.estPrice = json["Price"] as! Double
                self.estimatedPrice.text =  "Est. $\(String(format: "%.2f", self.estPrice))"
                self.setupGetTicketButton(user)
                
                if !self.estPriceShown  {
                    Util.fadeIn(views: [self.estimatedPrice], duration: 0.4, completion: nil)
                    self.estPriceShown = true
                }
            } else {}
        }
    }
    
    func show(vc: UIViewController, newControllerVC: UIViewController, party: vParty, type: String, selected: vTicket?) {
        self.inVC = vc
        self.newControllerVC = newControllerVC
        
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        let realm = try! Realm()
        if let partyRealm = realm.objects(Party.self).filter("id == '\(party.id)'").first {
            self.party = vParty(partyRealm)
            
            if let reserved = realm.objects(Party.self).filter("id == 'r\(party.id)'").first {
                self.party?.isReserved = true
                self.party?.key = reserved.key
            }
            
            setup()
            setupPager()
            setupImageBrowser()
        } else {
            alert!.hide()
        }
        
        if !type.isEmpty, let s = selected {
            self.ticket = s
            
            if type == "TICKET" {
                animateToTicket()
                setTicketDetails()
                
                if !party.isReserved {
                    generateTicket()
                }
            } else if type == "RESERVE" {
                animateToReserve()
                setReserveDetails()
            }
        }
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favoriteButtonPressed(_ sender: Any) {
        setPartyFavorite()
        favoriteButton.setImage(Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart-full") : #imageLiteral(resourceName: "heart-outline"), for: .normal)
        if !favPulse!.isPulsating { favPulse?.start() }
    }
    
    func animateToTicket() {
        onTicketScreen = true
        
        self.view.addConstraint(cardTop!)
        cardTop!.constant = 36
        
        self.background.addConstraint(getTicketsRight!)
        getTicketsInitialRight = background.getConstraintByID(id: "getTicketsInitialRight")
        
        if let constraint = getTicketsInitialRight {
            background.removeConstraint(constraint)
        }
        
        self.background.addConstraint(getTicketBottom!)
        getTicketInitialBottom = background.getConstraintByID(id: "getTicketInitialBottom")
        
        if let constraint = getTicketInitialBottom {
            background.removeConstraint(constraint)
        }
        
        self.background.addConstraint(getTicketLeft!)
        getTicketInitialLeft = background.getConstraintByID(id: "getTicketInitialLeft")
        
        if let constraint = getTicketInitialLeft {
            background.removeConstraint(constraint)
        }
    
        let favWidth = favoriteButton.getConstraintByID(id: "favWidth")
        let favHeight = favoriteButton.getConstraintByID(id: "favHeight")
        
        favWidth?.constant = 0
        favHeight?.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            self.favoriteButton.isHidden = true
        })
        
        Util.fadeOut(views: [litMeter, crowdedMeter, litLabel, crowdedLabel, estimatedPrice,
                             navigateButton, uberButton, partyName, partyTime, middleSegmentNoNotch, pageControl, pager, chatTitle, chatSubtitle], duration: 0.2)
        
        if descContainer != nil {
            Util.fadeOut(views: [descContainer!], duration: 0.2)
        }
        
        let fadeIn: [UIView] = party!.isReserved ?
            [ticketContainer, ticketHeader, reservedTicketDesc, reservedPriceCard] :
            [ticketContainer, ticketHeader, ticketDesc, ticketTypeCard]
        Util.fadeIn(views: fadeIn, duration: 0.2)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.getTicketsButton.setCornerRadius(radius: 0)
            self.getTicketsLabel.text = "CANCEL"
        })
    }
    
    func animateToReserve() {
        onTicketScreen = true
        
        let pagerHeight = pager.getConstraintByID(id: "pagerHeight")!
        pagerHeight.constant = initialPagerHeight

        self.view.addConstraint(cardTop!)
        cardTop!.constant = 295
        
        getTicketInitialLeft = background.getConstraintByID(id: "getTicketInitialLeft")
        getTicketInitialBottom = background.getConstraintByID(id: "getTicketInitialBottom")
        
        self.background.addConstraint(getTicketsReserveRight!)
        
        getTicketsInitialRight = background.getConstraintByID(id: "getTicketsInitialRight")
        background.removeConstraint(getTicketsInitialRight!)
        
        
        let favWidth = favoriteButton.getConstraintByID(id: "favWidth")
        let favHeight = favoriteButton.getConstraintByID(id: "favHeight")
        
        reserveHeight?.isActive = true
        
        favWidth?.constant = 0
        favHeight?.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            self.favoriteButton.isHidden = true
        })
        
        Util.fadeOut(views: [litMeter, crowdedMeter, litLabel, crowdedLabel, estimatedPrice,
                             navigateButton, partyName, partyTime, middleSegmentNoNotch, pageControl, pager, chatTitle, chatSubtitle], duration: 0.2)
        
        if descContainer != nil {
            Util.fadeOut(views: [descContainer!], duration: 0.2)
        }
        
        Util.fadeIn(views: [ticketHeader, reserveTicketsContainer], duration: 0.2)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.uberButton.backgroundColor = Colors.viibeBlue
            self.uberImage.image = #imageLiteral(resourceName: "close")
            self.getTicketsLabel.text = "BUY TICKET"
        })
    }
    
    func setReserveDetails() {
        ticketPriceLabel.text = "--"
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "MMM dd"
        reserveDate.text = formatter.string(from: party!.date).uppercased()
        
        formatter.dateFormat = "h:mm a"
        reserveTime.text = formatter.string(from: party!.date)
        
        reserveSurge.text = "--"
        
        if let host = party!.host {
            reserveHostedBy.text = host.name
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
         
        let params = ["PartyID": party!.id,  "Gender": (user.gender ? "male" : "female"), "TicketID": ticket!.id, "ServerVersion": "2"]
        Alamofire.request(Config.shared.partyEstimateURL, method: .post, parameters: params).responseJSON { (response) in
            if let price = response.result.value as? [String: Any] {
                self.ticketPriceLabel.text = "$\(String(format: "%.2f", price["Price"] as! Double))"
                self.reserveSurge.text = self.ticket!.type == "fixed" ?
                    "FIXED" : "\(String(format: "%.2f", price["Multiplier"] as! Double))"

            } else {
                Util.defaultAlert(self, title: "Uh Oh...", message: "There was a problem communicating with our server. Please try again.")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.35) {
                    self.animateToParty()
                }
            }
        }
    }
    
    func animateToParty() {
        onTicketScreen = false
        
        if (party!.isReservable) {
            reserveHeight?.isActive = false
        }
        
        let pagerHeight = pager.getConstraintByID(id: "pagerHeight")!
        pagerHeight.constant = lastPagerHeight
        
        self.view.removeConstraint(cardTop!)
        let cardBottom = view.getConstraintByID(id: "cardBottom")!
        cardBottom.constant = -4
        
        self.background.removeConstraint(getTicketsRight!)
        self.background.removeConstraint(getTicketsReserveRight!)
        background.addConstraint(getTicketsInitialRight!)
        
        self.background.removeConstraint(getTicketLeft!)
        background.addConstraint(getTicketInitialLeft!)
        
        self.background.removeConstraint(getTicketBottom!)
        background.addConstraint(getTicketInitialBottom!)
        
        let favWidth = favoriteButton.getConstraintByID(id: "favWidth")
        let favHeight = favoriteButton.getConstraintByID(id: "favHeight")
        
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
            self.uberButton.backgroundColor = UIColor(hex: "152F3C")
            self.uberImage.image = #imageLiteral(resourceName: "uber")
        }, completion: { (_) in
            favWidth?.constant = 44
            favHeight?.constant = 44
            self.favoriteButton.isHidden = false
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        })
      
        var views: [UIView] = []
        if lastPageNum == 0 {
            views = [litMeter, crowdedMeter, litLabel, crowdedLabel, estimatedPrice,
                      navigateButton, partyName, partyTime, middleSegmentNoNotch, pager]
            
            chatTitle.isHidden = false
            chatSubtitle.isHidden = false
        } else if lastPageNum == 2 {
            views = [litMeter, crowdedMeter, litLabel, crowdedLabel, estimatedPrice,
                     navigateButton, chatTitle, partyTime, middleSegmentNoNotch, pager]
            
            partyName.isHidden = false
            chatSubtitle.isHidden = false
        } else {
            views = [litMeter, crowdedMeter, litLabel, crowdedLabel, estimatedPrice,
                     navigateButton, chatTitle, chatSubtitle, middleSegmentNoNotch, pager]
            
            partyName.isHidden = false
            partyTime.isHidden = false
        }
        
        Util.fadeIn(views: views, duration: 0.2)
        if !party!.desc.isEmpty {
            Util.fadeIn(views: [pageControl], duration: 0.2)
        }
        
        if uberButton.isHidden {
            Util.fadeIn(views: [uberButton], duration: 0.2)
        }
        
        if descContainer != nil {
            Util.fadeIn(views: [descContainer!], duration: 0.2)
        }
        
        Util.fadeOut(views: [ticketContainer, ticketHeader, reserveTicketsContainer, reservedTicketDesc, reservedPriceCard], duration: 0.2)

        UIView.animate(withDuration: 0.2, animations: {
            self.getTicketsButton.setCornerRadius(radius: 5)
            
            if self.party!.isReserved {
                self.getTicketsLabel.text = "VIEW TICKET"
            } else if self.party!.isReservable {
                self.getTicketsLabel.text = "RESERVE"
            } else {
                self.getTicketsLabel.text = "GET TICKET"
            }
        })
    }
    
    func reserveTicket() {
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        self.animateToParty()
        
        let params = [
            "PartyID": party!.id,
            "UserID": user.id,
            "TicketID": ticket!.id,
            "ServerVersion": "2"
        ]
        
        Alamofire.request(Config.shared.reserveTicketURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let ticketObj = json["Ticket"] as? [String: Any],
                let key = ticketObj["Key"] as? String {
                self.party!.id = "r\(self.party!.id)"
                self.party!.isReserved = true
                self.party!.key = key
                
                let managed = Party()
                managed.setup(self.party!)
                
                let realm = try! Realm()
                
                Util.defaultAlert(self, title: "Success!", message: "To view your ticket or scan into the party, tap 'View Ticket'");
                
                guard let rUser = Util.getCurrentUser(realm) else {
                    return
                }
                
                try! realm.write {
                    rUser.reservedTickets.append(managed)
                }
                
                self.setupGetTicketButton(vUser(rUser))
            } else {
                Util.defaultAlert(self, title: "Failed to Reserve Ticket")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return party!.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        let cmt = party!.comments[party!.comments.count - indexPath.row - 1]
        
        cell.setup(cmt, partyID: party!.id, vc: self, heightUpdated: {
            tableView.beginUpdates()
            tableView.endUpdates()
        }, last: indexPath.row == party!.comments.count - 1)
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        return cell
    }
    
    func messageTextChanged() {
        sendBtn?.tintColor = messageField!.text!.isEmpty ? UIColor(hex: "EFEFEF"): Colors.viibeOrange

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if messageField != nil && messageField!.isFirstResponder {
            self.view.endEditing(true)
            return
        }
        
        let views: [UIView] = [topSegment, bottomSegment, middleSegment, middleSegmentNoNotch, background, favoriteButton, getTicketsButton, crowdedMeter, litMeter, estimatedPrice, uberButton, navigateButton, pager, pageControl]
        
        var found = false
        for touch in touches {
            if (views.contains(touch.view!)) {
                found = true
                break
            }
        }
        
        if (!onTicketScreen && !found) { self.alert?.hide() }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = -1 * (keyboardSize.height - 127)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
        return UInt(pictures.count)
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
        return pictures[Int(index)]
    }
}
