//
//  FavoritesCell.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import MBCircularProgressBar
import Pulsator
import RealmSwift
import CoreLocation

class FavoritesCell: UITableViewCell {

    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var imgGradient: GradientView!
    @IBOutlet weak var imgContainer: UIView!
    @IBOutlet weak var partyImage: UIImageView!
    @IBOutlet weak var partyPrice: UILabel!
    @IBOutlet weak var partyDistance: UILabel!
    @IBOutlet weak var partyDate: UILabel!
    @IBOutlet weak var partyTime: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var favoriteButton: ExtendedCloseButton!
    
    var favPulse: Pulsator?
    var party: vParty?
    
    var delete: ((vParty?) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        partyImage.setCornerRadius(radius: 5)
        partyImage.clipsToBounds = true
        
        imgContainer.addDropShadow()
        imgContainer.backgroundColor = .clear
        
        background.setCornerRadius(radius: 5)
        background.addDropShadow()
        
        imgGradient.setCornerRadius(radius: 5)
        
        background.layoutIfNeeded()
        
        favoriteButton.setCornerRadius(radius: 22)
        favoriteButton.addDropShadow(offset: CGSize.zero)

        favoriteButton.adjustsImageWhenHighlighted = false
        favoriteButton.imageEdgeInsets = UIEdgeInsetsMake(6,6,6,6)
        
        favPulse = Pulsator()
        favPulse!.animationDuration = 0.55
        favPulse!.radius = 50
        favPulse!.numPulse = 1
        favPulse!.repeatCount = 0
        
        favPulse!.backgroundColor = UIColor.red.cgColor
        favoriteButton.layer.addSublayer(favPulse!)
        favoriteButton.layer.layoutIfNeeded()
        favPulse!.position = CGPoint(x: favPulse!.position.x + 22, y: favPulse!.position.y + 22)
        
        background.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundPressed)))
    }

    func setup(_ party: vParty, gender: Bool) {
        self.party = party
        favoriteButton.setImage(Util.isFavorited(party.id) ? #imageLiteral(resourceName: "heart-full") : #imageLiteral(resourceName: "heart-outline"), for: .normal)
        
        partyName.text = party.name
        
        let dateString = NSMutableAttributedString().normal("\(Util.getDayText(date: party.date).uppercased())", font: Fonts.fouBold(size: 20))
        let timeString = NSMutableAttributedString().normal("\(Util.getTimeText(date: party.date))", font: Fonts.fou(size: 15))
        
        if width(dateString) < width(timeString) {
            var spacing = 0.0
            while width(dateString) < width(timeString) {
                dateString.setAttributes([NSKernAttributeName : spacing], range: NSMakeRange(0, dateString.length))
                spacing += 0.05
            }
        } else {
            var spacing = 0.0
            while width(timeString) < width(dateString) {
                timeString.setAttributes([NSKernAttributeName : spacing], range: NSMakeRange(0, timeString.length))
                spacing += 0.05
            }
        }
        
        partyDate.attributedText = dateString
        partyTime.attributedText = timeString
        
        partyName.getConstraintByID(id: "partyNameWidth")?.constant = partyPrice.frame.origin.x - 18
        partyName.layoutIfNeeded()
        
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        let userCoord = CLLocation(latitude: lat, longitude: lng)
        
        let distanceMeters = userCoord.distance(from: CLLocation(latitude: party.lat, longitude: party.lng))
        let distanceMi = distanceMeters * 0.000621371192
        
        partyDistance.text = "\(String(format: "%.1f", distanceMi)) mi"
        
        if let url = party.images.first?.url {
            partyImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        } else {
            partyImage.image = #imageLiteral(resourceName: "default-party-image")
        }
        
        if party.tickets.isEmpty {
            partyPrice.attributedText = NSMutableAttributedString().normal("PAY AT VENUE", font: Fonts.fouMedium(size: 20))
        } else {
            var minPrice = 9999.0
            
            for ticket in party.tickets {
                if ticket.type == "dynamic" {
                    minPrice = min(0.7 * (gender ? ticket.malePrice : ticket.femalePrice), minPrice)
                } else {
                    minPrice = min(gender ? ticket.malePrice : ticket.femalePrice, minPrice)
                }
            }
            
            partyPrice.attributedText = NSMutableAttributedString().normal("from ", font: Fonts.fouMedium(size: 15)).normal("$\(String(format: "%.2f", minPrice))", font: Fonts.fouMedium(size: 25))
        }
    }
    
    //            dateString.addAttribute(NSKernAttributeName, value: 1, range: NSRange(location: 0, length: dateString.length))
    
    
    func width(_ str: NSAttributedString) -> CGFloat {
        let label = UILabel()
        label.attributedText = str
        label.sizeToFit()
        
        return label.frame.width
        
    }
    
    @IBAction func favoritesButtonPressed(_ sender: Any) {
        setPartyFavorite()
        favoriteButton.setImage(Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart-full") : #imageLiteral(resourceName: "heart-outline"), for: .normal)
        if !favPulse!.isPulsating { favPulse?.start() }
        
        if delete != nil { delete!(party) }
    }
    
    func backgroundPressed() {
        if ViewController.instance == nil || party == nil { return }
        
        
        let card = ViibeCard()
        card.setUnfavorited {
            card.hide()
            if self.delete != nil { self.delete!(self.party) }
        }
        
        card.show(ViewController.instance!, party: party!)
    }
    
    func setPartyFavorite() {
        if Util.isFavorited(party!.id) {
            Util.unfavoriteParty(party!.id)
        } else {
            Util.favoriteParty(party!.id)
        }
    }
}
