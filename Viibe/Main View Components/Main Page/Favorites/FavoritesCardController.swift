import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift

class FavoritesCardController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var favorites: [vParty] = []
    @IBOutlet weak var table: UITableView!
    var alert: FavoritesCard?
    var favPulse: Pulsator?
    var close: (() -> Void)?
    
    var gender = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        table.backgroundColor = .clear
        table.tableFooterView = UIView()
        table.separatorStyle = .none
        table.allowsSelection = false
        table.estimatedRowHeight = 190

        view.sendSubview(toBack: table)
        
        table.register(UINib(nibName: "FavoritesCell", bundle: nil), forCellReuseIdentifier: "FavCell")
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        gender = user.gender
        
        loadFavorites()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = UITableViewCell()

            cell.backgroundColor = .clear
            return cell
        }
        
        let cell = table.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavoritesCell
        cell.setup(favorites[indexPath.row - 1], gender: gender)
        cell.backgroundColor = .clear
        cell.delete = { (party) in
            if party == nil { return }
            
            guard let index = self.favorites.index(where: { (p) -> Bool in
                return p.id == party!.id
            }) else {
                return
            }
            
            self.favorites.remove(at: index)
            self.table.beginUpdates()
            self.table.deleteRows(at: [IndexPath(row: index + 1, section: 0)], with: .right)
            self.table.endUpdates()
            
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { _ in
                if self.favorites.isEmpty { self.close!()}
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favorites.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 70 : UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show(vc: UIViewController, close: @escaping () -> Void) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        self.close = close;
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func backPressed(_ sender: Any) {
        if (close != nil) {
            close!()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadFavorites()
    }
    
    func loadFavorites() {
        let realm = try! Realm()
        let parties = realm.objects(Party.self).filter { (party) -> Bool in
            return Util.isFavorited(party.id)
        }
        
        favorites.removeAll()
        for party in parties {
            favorites.append(vParty(party))
        }
        
        table.reloadData()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
