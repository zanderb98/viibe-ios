//
//  FilterCardController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/28/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import Alamofire
import Lottie
import CoreLocation
import CHIPageControl
import MaterialComponents
import MWPhotoBrowser
import SDWebImage
import ActionSheetPicker_3_0

class FilterCardController: UIViewController {

    var alert: FilterCard?
    var inVC: UIViewController?
    @IBOutlet weak var background: UIView!
    
    @IBOutlet weak var viibeTicketsOff: UIImageView!
    @IBOutlet weak var viibeTicketsOn: UIImageView!
    
    @IBOutlet weak var favoritesOff: UIImageView!
    @IBOutlet weak var favoritesOn: UIImageView!
    
    @IBOutlet weak var maxDistanceOff: UIImageView!
    @IBOutlet weak var maxDistanceOn: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceSlider: UISlider!
    
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var dateOff: UIImageView!
    @IBOutlet weak var dateOn: UIImageView!
    
    var date: Date? = nil
    var filtered: (() -> Void)?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let distance = Util.keyExists(Constants.DistanceFilter) ?
            UserDefaults.standard.double(forKey: Constants.DistanceFilter) : -1.0
        let filterDate = Util.keyExists(Constants.DateFilter) ?
            UserDefaults.standard.double(forKey: Constants.DateFilter) : -1.0
        let favorited = Util.keyExists(Constants.FavoritesFilter) ?
            UserDefaults.standard.bool(forKey: Constants.FavoritesFilter) : false
        let viibeTix = Util.keyExists(Constants.ViibeTicketsFilter) ?
            UserDefaults.standard.bool(forKey: Constants.ViibeTicketsFilter) : false
        
        distanceSlider.minimumValue = 0
        distanceSlider.maximumValue = 200
        distanceSlider.value = Float((distance == -1) ? 100 : (distance * 10))
        distanceLabel.text = "\(String(format: "%.1f", Float((distance == -1) ? 10 : (distance)))) mi"

        distanceSlider.isEnabled = distance != -1
        if (distance != -1) {
            showSwitch(switchOn: maxDistanceOn, switchOff: maxDistanceOff, duration: 0)
        }
        
        if (filterDate != -1) {
            showSwitch(switchOn: dateOn, switchOff: dateOff, duration: 0)
            date = Date(timeIntervalSince1970: filterDate)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM d, YYYY"
            
            dateText.text = formatter.string(from: date!)
        }
        
        if (favorited) {
            showSwitch(switchOn: favoritesOn, switchOff: favoritesOff, duration: 0)
        }
        
        if (viibeTix) {
            showSwitch(switchOn: viibeTicketsOn, switchOff: viibeTicketsOff, duration: 0)
        }
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(FilterCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FilterCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func show(vc: UIViewController) {
        self.inVC = vc
        
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        let currentValue = Float(sender.value) / 10
        UserDefaults.standard.set(currentValue, forKey: Constants.DistanceFilter)
        distanceLabel.text = "\(String(format: "%.1f", currentValue)) mi"
        
        filtered?()
    }
    
    func showDatePicker() {
        self.view.endEditing(true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM d, YYYY"
        
        let datePicker = ActionSheetDatePicker(title: "Pick Date", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            if let dt = value as? Date {
                self.date = dt
                self.dateText.text = formatter.string(from: self.date!)
                
                UserDefaults.standard.set(self.date!.timeIntervalSince1970, forKey: Constants.DateFilter)
                self.filtered?()

                if self.dateOn.isHidden {
                    self.showSwitch(switchOn: self.dateOn, switchOff: self.dateOff)
                }
            }
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    func showSwitch(switchOn: UIImageView, switchOff: UIImageView, duration: Double = 0.2) {
        Util.fadeIn(views: [switchOn], duration: 0.2)
        Util.fadeOut(views: [switchOff], duration: 0.2)
    }
    
    @IBAction func viibeTicketsPressed(_ sender: Any) {
        if viibeTicketsOn.isHidden {
            showSwitch(switchOn: viibeTicketsOn, switchOff: viibeTicketsOff)
            UserDefaults.standard.set(true, forKey: Constants.ViibeTicketsFilter)
        } else {
            showSwitch(switchOn: viibeTicketsOff, switchOff: viibeTicketsOn)
            UserDefaults.standard.set(false, forKey: Constants.ViibeTicketsFilter)
        }
        
        filtered?()
    }
    
    @IBAction func favoritesSwitchPressed(_ sender: Any) {
        if favoritesOn.isHidden {
            showSwitch(switchOn: favoritesOn, switchOff: favoritesOff)
            UserDefaults.standard.set(true, forKey: Constants.FavoritesFilter)
        } else {
            showSwitch(switchOn: favoritesOff, switchOff: favoritesOn)
            UserDefaults.standard.set(false, forKey: Constants.FavoritesFilter)
        }
        
        filtered?()
    }
    
    @IBAction func maxDistanceSwitchPressed(_ sender: Any) {
        if maxDistanceOn.isHidden {
            showSwitch(switchOn: maxDistanceOn, switchOff: maxDistanceOff)
        
            distanceSlider.isEnabled = true
        } else {
            showSwitch(switchOn: maxDistanceOff, switchOff: maxDistanceOn)
            UserDefaults.standard.set(-1, forKey: Constants.DistanceFilter)
            distanceSlider.isEnabled = false
            
            filtered?()
        }
    }
    
    @IBAction func dateSwitchPressed(_ sender: Any) {
        if dateOff.isHidden {
            UserDefaults.standard.set(-1, forKey: Constants.DateFilter)
            showSwitch(switchOn: dateOff, switchOff: dateOn)
            
            filtered?()
        } else if date == nil {
            showDatePicker()
        } else {
            showSwitch(switchOn: dateOn, switchOff: dateOff)
        }
    }
    
    @IBAction func dateTextPressed(_ sender: Any) {
        showDatePicker()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var views: [UIView] = [background]
        
        for child in background.subviews {
            views.append(child)
        }
        
        var found = false
        for touch in touches {
            if (views.contains(touch.view!)) {
                found = true
                break
            }
        }
        
        if (!found) { self.alert?.hide() }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y = -1 * (keyboardSize.height - 127)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
}
