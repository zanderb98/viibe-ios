//
//  ListPartyCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/14/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation

class ListPartyCell: UITableViewCell {
    
    @IBOutlet weak var partyImg: UIImageView!
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var partyDate: UILabel!
    @IBOutlet weak var partyPrice: UILabel!
    @IBOutlet weak var partyDistance: UILabel!
    @IBOutlet weak var imgGradient: GradientView!
    @IBOutlet weak var partyLoc: UIImageView!
    @IBOutlet weak var favoriteHeart: UIImageView!
    
    var newVC: UIViewController?
    var filter: (() -> Void)?

    var party: vParty?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        partyImg.clipsToBounds = true
        
        partyLoc.image = #imageLiteral(resourceName: "partyloc").withRenderingMode(.alwaysTemplate)
        partyLoc.tintColor = .white
    }
    
    func setup(_ party: vParty, newVC: UIViewController?, gender: Bool) {
        self.newVC = newVC
        self.party = party
        
        partyName.text = party.name
        partyDate.text = Util.getDateText(date: party.date)
        
        favoriteHeart.image = Util.isFavorited(party.id) ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_empty").withRenderingMode(.alwaysTemplate)
        favoriteHeart.tintColor = .white
        
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        let userCoord = CLLocation(latitude: lat, longitude: lng)
        
        let distanceMeters = userCoord.distance(from: CLLocation(latitude: party.lat, longitude: party.lng))
        let distanceMi = distanceMeters * 0.000621371192
        
        partyDistance.text = "\(String(format: "%.1f", distanceMi)) mi"
        
        if let url = party.images.first?.url {
            partyImg.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        } else {
            partyImg.image = #imageLiteral(resourceName: "default-party-image")
        }
        
        if party.tickets.isEmpty {
            partyPrice.text = "PAY AT VENUE"
        } else {
            var minPrice = 9999.0
            
            for ticket in party.tickets {
                if ticket.type == "dynamic" {
                    minPrice = min(0.7 * (gender ? ticket.malePrice : ticket.femalePrice), minPrice)
                } else {
                    minPrice = min(gender ? ticket.malePrice : ticket.femalePrice, minPrice)
                }
            }
            
            partyPrice.attributedText = NSMutableAttributedString().normal("from ", font: Fonts.museo300(size: 13)).normal("$\(String(format: "%.2f", minPrice))", font: Fonts.museo300(size: 15))
        }
    }
    
    //            dateString.addAttribute(NSKernAttributeName, value: 1, range: NSRange(location: 0, length: dateString.length))

    @IBAction func favoritePressed(_ sender: Any) {
        if Util.isFavorited(party!.id) {
            Util.unfavoriteParty(party!.id)
        } else {
            Util.favoriteParty(party!.id)
        }
        
        favoriteHeart.image = Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_empty").withRenderingMode(.alwaysTemplate)
    }
    
    func width(_ str: NSAttributedString) -> CGFloat {
        let label = UILabel()
        label.attributedText = str
        label.sizeToFit()
        
        return label.frame.width

    }
    
    @IBAction func cardPressed(_ sender: Any) {
        if let p = party {
            let detailsVC = EventDetailsController(nibName: "EventDetailsController", bundle: nil)
            detailsVC.party = p
            detailsVC.favoriteChanged = {
                self.favoriteHeart.image = Util.isFavorited(p.id) ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_empty").withRenderingMode(.alwaysTemplate)
                self.filter?()
            }
            
            ViewController.instance?.navigationController?.pushViewController(detailsVC, animated: true)
        }
    }
}
