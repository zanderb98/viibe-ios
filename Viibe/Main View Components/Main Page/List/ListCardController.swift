import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import Stripe
import Alamofire
import MaterialComponents
import ActionSheetPicker_3_0
import CoreLocation

class ListCardController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var parties: [vParty] = []
    var filtered: [vParty] = []

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noPartiesLabel: UILabel!
    @IBOutlet weak var filterButton: MDCFloatingButton!
    
    var close: (() -> Void)?
    var inVC: UIViewController?
    
    var user: vUser?
    var gender = true
    
    var city = ""
    
    var searchText = ""
    var filterDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Util.getCurrentUserUnmanaged() {
            self.user = user
            self.gender = user.gender
        }
        
        city = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        table.register(UINib(nibName: "ListPartyCell", bundle: nil), forCellReuseIdentifier: "ListPartyCell")
        
        table.delegate = self
        table.dataSource = self
        
        table.tableHeaderView = Util.footerView(height: 20)
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 160
    
        filterButton.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
        filterButton.backgroundColor = UIColor(hex: "F2F2F2")
        
        loadParties()
            
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(ListCardController.loadParties), name: Notifications.PartiesUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func filterAndUpdate(searchText: String, scrollToTop: Bool = true) {
        self.searchText = searchText
        let prefilter = filtered
        
        filtered = parties.filter({ (a) -> Bool in
            var included = true
            
            let distance = Util.keyExists(Constants.DistanceFilter) ?
                UserDefaults.standard.double(forKey: Constants.DistanceFilter) : -1.0
            let date = Util.keyExists(Constants.DateFilter) ?
                UserDefaults.standard.double(forKey: Constants.DateFilter) : -1.0
            let favorited = Util.keyExists(Constants.FavoritesFilter) ?
                UserDefaults.standard.bool(forKey: Constants.FavoritesFilter) : false
            let viibeTix = Util.keyExists(Constants.ViibeTicketsFilter) ?
                UserDefaults.standard.bool(forKey: Constants.ViibeTicketsFilter) : false
            
            included = searchText.isEmpty || a.name.uppercased().contains(searchText.uppercased())
            included = included && (distance == -1 ||  a.distanceMi() <= distance)
            included = included && (date == -1 ||  a.date.sameDay(Date(timeIntervalSince1970: date)))
            included = included && (!favorited || Util.isFavorited(a.id))
            included = included && (!viibeTix || !a.tickets.isEmpty)
            
            return included
        })
        
        if (filterDate != nil) || !searchText.isEmpty {
            let userCoord = CLLocation(latitude: UserDefaults.standard.double(forKey: "LastKnownLat"),
                                   longitude: UserDefaults.standard.double(forKey: "LastKnownLng"))
            
            filtered.sort { (a, b) -> Bool in
                if a.name.lowercased().starts(with: searchText.lowercased()) && !b.name.lowercased().starts(with: searchText.lowercased()) {
                    return true
                } else if b.name.lowercased().starts(with: searchText.lowercased()) && !a.name.lowercased().starts(with: searchText.lowercased()) {
                    return false
                }
                
                if a.date == b.date {
                    return CLLocation(latitude: a.lat, longitude: a.lng).distance(from: userCoord) < CLLocation(latitude: b.lat, longitude: b.lng).distance(from: userCoord)
                }
                
                return a.date.compare(b.date) == .orderedAscending
            }
        }
        
        noPartiesLabel.isHidden = !filtered.isEmpty
        
        if (UserDefaults.standard.bool(forKey: Constants.FirstListLoad)) {
            noPartiesLabel.text = "LOADING..."
            UserDefaults.standard.set(false, forKey: Constants.FirstListLoad)
        } else {
            noPartiesLabel.text = parties.isEmpty ? "NO PARTIES AROUND" : "NO RESULTS"
        }
        
        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]
        
        for i in 0..<prefilter.count {
            let a = prefilter[i]
            
            if !arrContains(arr: filtered, object: a) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }
        
        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]
            
            if !arrContains(arr: prefilter, object: a) {
                inserted.append(IndexPath(row: index, section: 0))
            }
            
            index += 1
        }
        
        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }
            
            let a = prefilter[i]
            let filteredIndex = filtered.index(of: a)
    
            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }
        
        self.table.beginUpdates()
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)
        
        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }
        
        self.table.endUpdates()
        
        if scrollToTop && !filtered.isEmpty {
            self.table.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    
    func loadParties() {
        let realm = try! Realm()
        let realmParties = realm.objects(Party.self).filter { (party) -> Bool in
            return (Util.isPartyToday(party: party) || Date().compare(party.date) == .orderedAscending) && !party.isPastParty && !party.isReserved && party.open
        }
        
        parties = []
        for party in realmParties {
            parties.append(vParty(party))
        }
        
        let userCoord = CLLocation(latitude: UserDefaults.standard.double(forKey: "LastKnownLat"),
                                   longitude: UserDefaults.standard.double(forKey: "LastKnownLng"))
        
        parties.sort { (a, b) -> Bool in
            if a.city == city && b.city != city {
                return true
            } else if a.city != city && b.city == city {
                return false
            }
            
            if a.date == b.date {
                return CLLocation(latitude: a.lat, longitude: a.lng).distance(from: userCoord) < CLLocation(latitude: b.lat, longitude: b.lng).distance(from: userCoord)
            }
            
            return a.date.compare(b.date) == .orderedAscending
        }
        
        filterAndUpdate(searchText: searchText, scrollToTop: false)
    }
    
    func show(vc: UIViewController, close: @escaping () -> Void) {
        self.inVC = vc
        
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        self.close = close;
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let party = filtered[indexPath.row]
        
        let cell = table.dequeueReusableCell(withIdentifier: "ListPartyCell", for: indexPath) as! ListPartyCell
        cell.filter = {
            self.filterAndUpdate(searchText: self.searchText, scrollToTop: false)
        }
        cell.setup(party, newVC: inVC ?? self, gender: gender)
        return cell
        
    }

    @IBAction func filterPressed(_ sender: Any) {
        let filterCard = FilterCard()
        
        filterCard.show(ViewController.instance!, filtered: {
            self.filterAndUpdate(searchText: self.searchText)
        })
    }
    
    @IBAction func backPressed(_ sender: Any) {
        if (close != nil) {
            close!()
        }
    }
    
    func arrContains(arr: [vParty], object: vParty) -> Bool {
        for a in arr {
            if a.id == object.id {
                return true
            }
        }
        
        return false
    }
    
    var keyboardOpen = false
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardOpen {
                return
            }
            
            keyboardOpen = true
            self.table.frame.size.height -= keyboardSize.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardOpen {
                return
            }
            
            keyboardOpen = false
             self.table.frame.size.height += keyboardSize.height
        }
    }
}
