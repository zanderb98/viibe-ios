
//
//  ListHostCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 7/15/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class ListHostCell: UITableViewCell {

    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var eventsHosted: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    
    
    var host: vHost?
    var vc: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        arrowImg.image = UIImage(named: "chevron-right")?.withRenderingMode(.alwaysTemplate)
        arrowImg.tintColor = Colors.viibeBlue
        
        profilePicView.clipsToBounds = true
        profilePicView.setCornerRadius(radius: profilePicView.frame.width / 2)
        
        self.clipsToBounds = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cardPressed)))
    }

    func setup(_ host: vHost) {
        let realm = try! Realm()
        if let rHost = realm.objects(Host.self).first(where: { (h) -> Bool in
            return h.id == host.id
        }) {
            self.host = vHost(rHost)
        } else {
            self.host = host
        }

        profileName.text = host.name
        
        if !host.profilePictureURL.isEmpty {
            profilePic.sd_setImage(with: URL(string: host.profilePictureURL), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        profilePicView.addDropShadow()
        
        eventsHosted.attributedText = NSMutableAttributedString().normal("\(host.hostedParties.count)", font: Fonts.museo500(size: 15)).normal(" events hosted", font: Fonts.museo300(size: 15))
    }
    
    @objc func cardPressed() {
        let hostVC = HostProfileController(nibName: "HostProfileController", bundle: nil)
        hostVC.host = host
        hostVC.updateListCell = {
            self.setup(self.host!)
        }
        
        self.vc?.navigationController?.pushViewController(hostVC, animated: true)
    }
}
