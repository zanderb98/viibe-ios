//
//  PastPartiesController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 2/14/19.
//  Copyright © 2019 Brodly. All rights reserved.
//

import UIKit
import Pulsator

class PastPartiesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    var pastParties: [vParty] = []
    @IBOutlet weak var table: UITableView!
    var favPulse: Pulsator?
    var close: (() -> Void)?
    var alert: PastPartiesCard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topBar.addDropShadow(opacity: 0.2)
        
        table.backgroundColor = .clear
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = Util.footerView(height: 24)
        table.separatorStyle = .none
        table.allowsSelection = false
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 93
        
        view.sendSubview(toBack: table)
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        pastParties = user.pastParties
        
        pastParties.sort { (partyA, partyB) -> Bool in
            return partyA.dateEntered! > partyB.dateEntered!
        }
        
        table.register(UINib(nibName: "PastPartiesCell", bundle: nil), forCellReuseIdentifier: "PastPartiesCell")
        table.reloadData()
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "PastPartiesCell", for: indexPath) as! PastPartiesCell
        cell.vc = self
        cell.setup(party: pastParties[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pastParties.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show(vc: UIViewController) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
