//
//  PastPartiesCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/8/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import Pulsator

class PastPartiesCell: UITableViewCell {

    @IBOutlet weak var background: OutsideView!
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var partyTime: UILabel!
    @IBOutlet weak var partyPrice: UILabel!
    
    var party: vParty?
    var vc: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor(hex: "FAFAFA")
        self.contentView.backgroundColor = UIColor(hex: "FAFAFA")
        
        background.addDropShadow()
    }
    
    func setup(party: vParty) {
        self.party = party
        
        partyName.text = party.name
        if let dateEntered = party.dateEntered {
            partyTime.text = Util.getDateText(date: dateEntered)
            partyPrice.attributedText = NSMutableAttributedString().normal("$", font: Fonts.fouMedium(size: 18)).normal("\(String(format: "%.2f", party.price))", font: Fonts.fouMedium(size: 27))
        }
        
        background.layoutIfNeeded()
        
        partyName.getConstraintByID(id: "partyNameWidth")?.constant = partyPrice.frame.origin.x - 24
        partyName.layoutIfNeeded()
        
    }
}
