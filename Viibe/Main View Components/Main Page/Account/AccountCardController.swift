import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import Stripe
import Alamofire
import MaterialComponents

class AccountCardController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var alert: AccountCard?
    var favPulse: Pulsator?
    var close: (() -> Void)?
    var inVC: UIViewController?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Profile
    @IBOutlet weak var topCard: OutsideView!
    @IBOutlet weak var profilePicBig: UIImageView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var numberOfParties: UILabel!

    //Payment
    @IBOutlet weak var paymentCard: UIView!
    @IBOutlet weak var paymentContent: UIView!
    @IBOutlet weak var paymentExpand: UIImageView!
    @IBOutlet weak var paymentContentHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentBottom: NSLayoutConstraint!
    @IBOutlet weak var creditCardNumber: UILabel!
    @IBOutlet weak var creditCardImage: UIImageView!
    @IBOutlet weak var paymentEdit: UIView!
    @IBOutlet weak var paymentButtonText: UILabel!
    @IBOutlet weak var paymentRemove: UIView!
    @IBOutlet weak var paymentRemoveTrailing: NSLayoutConstraint!
    var paymentRemoveWidth: NSLayoutConstraint?
    
    //My Tickets
    @IBOutlet weak var myTicketsCard: OutsideView!
    @IBOutlet weak var myTicketsContent: UIView!
    @IBOutlet weak var myTicketsExpand: UIImageView!
    @IBOutlet weak var myTicketsContentHeight: NSLayoutConstraint!
    @IBOutlet weak var myTicketsNoTix: UILabel!
    @IBOutlet weak var myTicketsTable: UITableView!
    @IBOutlet weak var myTicketsTableHeight: NSLayoutConstraint!
    
    //Past Parties
    @IBOutlet weak var pastPartiesCard: UIView!
    @IBOutlet weak var pastPartiesContent: UIView!
    @IBOutlet weak var pastPartiesExpand: UIImageView!
    @IBOutlet weak var pastPartiesContentHeight: NSLayoutConstraint!
    @IBOutlet weak var pastPartiesBottom: NSLayoutConstraint!
    @IBOutlet weak var pastPartiesViewAll: UIView!
    @IBOutlet weak var pastPartyNameA: UILabel!
    @IBOutlet weak var pastPartyPriceA: UILabel!
    @IBOutlet weak var pastPartyNameB: UILabel!
    @IBOutlet weak var pastPartyPriceB: UILabel!
    var pastParties: [vParty]?
    
    //Support
    @IBOutlet weak var supportCard: UIView!
    
    @IBOutlet weak var signOutButton: UIView!
    
    var user: vUser?
    var reservedTix: [vParty] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        topCard.addDropShadow()
        
//        let topCardBG = UIImageView()
//        topCardBG.image = #imageLiteral(resourceName: "gradient")
//        topCardBG.frame = topCard.bounds
//        topCardBG.frame.size.height += 2
//        topCard.contentMode = .scaleAspectFill
//        topCard.insertSubview(topCardBG, at: 0)
        topCard.clipsToBounds = true
        
        profilePic.setCornerRadius(radius: profilePic.frame.width / 2)
        
        profilePicView.addDropShadow()
        
        paymentCard.addDropShadow()
        profilePicView.setCornerRadius(radius: profilePicView.frame.width / 2)
        
        paymentContent.clipsToBounds = true
        myTicketsContent.clipsToBounds = true
        pastPartiesContent.clipsToBounds = true
        
        paymentEdit.addDropShadow()
        paymentEdit.setCornerRadius(radius: paymentEdit.frame.height / 2)
        
        paymentRemove.addDropShadow()
        paymentRemove.setCornerRadius(radius: paymentRemove.frame.height / 2)
        
        paymentExpand.image = #imageLiteral(resourceName: "expand").withRenderingMode(.alwaysTemplate)
        paymentExpand.tintColor = .black
        
        myTicketsExpand.image = #imageLiteral(resourceName: "expand").withRenderingMode(.alwaysTemplate)
        myTicketsExpand.tintColor = .black
        
        myTicketsCard.addDropShadow()
        
        pastPartiesExpand.image = #imageLiteral(resourceName: "expand").withRenderingMode(.alwaysTemplate)
        pastPartiesExpand.tintColor = .black
        
        pastPartiesCard.addDropShadow()
        
        pastPartiesViewAll.addDropShadow()
        pastPartiesViewAll.setCornerRadius(radius: pastPartiesViewAll.frame.height / 2)
        
        supportCard.addDropShadow()
        
        signOutButton.addDropShadow()
        signOutButton.setCornerRadius(radius: pastPartiesViewAll.frame.height / 2)
        
        pastPartyPriceA.font = Fonts.fou(size: 19)
        pastPartyPriceB.font = Fonts.fou(size: 19)
        
        myTicketsTable.register(UINib(nibName: "MyTicketCell", bundle: nil), forCellReuseIdentifier: "MyTicketCell")
        myTicketsTable.dataSource = self
        myTicketsTable.delegate = self
        
        myTicketsTable.rowHeight = UITableViewAutomaticDimension
        myTicketsTable.estimatedRowHeight = 60
        myTicketsTable.tableFooterView = UIView()
        
        setUpWithUser()
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(AccountCardController.signOutPressed))
        signOutButton.addGestureRecognizer(rec)
        
        creditCardImage.contentMode = .scaleAspectFill
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(AccountCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AccountCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setUpWithUser() {
        guard let user = Util.getCurrentUserUnmanaged() else { return }
        self.user = user
        
        //Top Card
        if let proPicData = user.profilePicture {
            profilePic.image = UIImage(data: proPicData)
            profilePicBig.image = UIImage(data: proPicData)
        }
        
        profileName.text = user.name
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .normal("Viibed at ", font: Fonts.museo300(size: 16))
            .normal("\(user.pastParties.count)", font: Fonts.museo500(size: 16))
            .normal(" \(user.pastParties.count == 1 ? "event" : "events")", font: Fonts.museo300(size: 16))
        numberOfParties.attributedText = formattedString
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M/dd/YYYY"
        
        //Payment Card
        if let paymentData = user.creditCardImage {
            creditCardImage.image = UIImage(data: paymentData)
            creditCardNumber.text = "∙∙∙∙ ∙∙∙∙ ∙∙∙∙ \(user.creditCardLast4)"
            
            showPaymentRemove(true, duration: 0)
        } else {
            creditCardImage.isHidden = true
            creditCardNumber.text = "No Payment Method"
            paymentButtonText.text = "ADD"
            
            showPaymentRemove(false, duration: 0)
        
        }
        
        //My Tickets Card
        reservedTix = user.reservedTickets.filter({ (party) -> Bool in
            return party.date.compare(Date()) != .orderedAscending ||
                party.endDate.compare(Date()) != .orderedAscending || party.open
        })
        
        reservedTix.sort { (a, b) -> Bool in
            return a.date.compare(b.date) == .orderedAscending
        }

        myTicketsNoTix.isHidden = !reservedTix.isEmpty
        myTicketsTable.isHidden = reservedTix.isEmpty
        
        //Past Parties Card
        pastParties = user.pastParties
        pastPartiesViewAll.isHidden = pastParties!.isEmpty

        pastPartiesCard.getConstraintByID(id: "pastPartiesBottom")?.constant = 18
        pastPartiesCard.layoutIfNeeded()
        
        if (pastParties!.isEmpty) {
            pastPartyNameA.text = "No Past Parties"
            pastPartyPriceA.isHidden = true
            pastPartyNameB.isHidden = true
            pastPartyPriceB.isHidden = true
            
//            pastPartyNameA.getConstraintByID(id: "pastPartiesNameWidth")?.constant = 300
        } else if (pastParties!.count == 1) {
            pastPartyNameA.text = pastParties!.first!.name
            pastPartyPriceA.text = "$\(String(format: "%.2f", pastParties!.first!.price))"
            
            pastPartyNameB.isHidden = true
            pastPartyPriceB.isHidden = true
            
        
            pastPartiesCard.layoutIfNeeded()
        } else {
            pastParties = pastParties!.sorted(by: { (partyA, partyB) -> Bool in
                return partyA.dateEntered! > partyB.dateEntered!
            })
            
            pastPartyNameA.text = pastParties!.first!.name
            pastPartyPriceA.text = "$\(String(format: "%.2f", pastParties!.first!.price))"
            
            pastPartyNameB.text = pastParties![1].name
            pastPartyPriceB.text = "$\(String(format: "%.2f", pastParties![1].price))"
            
        }
        
        pastPartyNameA.getConstraintByID(id: "pastPartyNameAWidth")?.constant = pastPartyPriceA.frame.origin.x - 32
        pastPartyNameB.getConstraintByID(id: "pastPartyNameBWidth")?.constant = pastPartyPriceB.frame.origin.x - 32
        view.layoutIfNeeded()
    }
    
    func showPaymentRemove(_ show: Bool, duration: TimeInterval) {
        if show {
            if let w = paymentRemoveWidth {
                paymentRemove.removeConstraint(w)
                paymentRemoveTrailing.constant = 16
            }
        } else {
            paymentRemoveWidth = NSLayoutConstraint(item: paymentRemove, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
            paymentRemove.addConstraint(paymentRemoveWidth!)
            paymentRemoveTrailing.constant = 8
        }
        
        UIView.animate(withDuration: duration) {
            self.paymentContent.layoutIfNeeded()
        }
    }
    
    @IBAction func paymentExpandPressed(_ sender: Any) {
        if paymentContentHeight.constant == 0 {
            paymentContentHeight.constant = 78
            paymentBottom.constant = 26
            paymentContent.clipsToBounds = false
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.paymentExpand.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2));
            }
        } else {
            paymentContentHeight.constant = 0
            paymentBottom.constant = 18
            paymentContent.clipsToBounds = true
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.paymentExpand.transform = CGAffineTransform(rotationAngle: 0)
            })
        }
    }
    
    @IBAction func editPaymentsPressed(_ sender: Any) {
        let vc = STPAddCardViewController()
        vc.delegate = self
        
        ViewController.instance?.navigationController?.isNavigationBarHidden = false
        ViewController.instance?.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func removePaymentPressed(_ sender: Any) {
        let alertController = MDCAlertController(title: "Remove Card?", message: "Are you sure you want to remove your card from Viibe?")
        
        let okAction = MDCAlertAction(title: "Remove") { (a) in
            self.removeCard()
        }
        
        alertController.addAction(okAction)
        alertController.addAction(MDCAlertAction(title: "Cancel"))
        present(alertController, animated:true, completion: nil)
    }
    
    func removeCard() {
        guard let userUnmanaged = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let params = ["UserID": userUnmanaged.id,
                      "ServerVersion": "2"]
        
        Alamofire.request(Config.shared.removeCardURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            let user = Util.getCurrentUser(realm)
            
            if (response.response?.statusCode != 200) {
                Util.defaultAlert(self, title: "Uh Oh...", message: "Something went wrong while removing your card from our server. Try again later.", action: {
                    
                })
                
                return
            }
            
            if let json = response.result.value as? [String: Any],
                let success = json["Success"] as? Bool {
                if success {
                    try! realm.write {
                        user?.creditCardBrand = ""
                        user?.creditCardLast4 = ""
                        user?.creditCardImage = nil
                    }
                    
                    self.creditCardImage.isHidden = true
                    self.creditCardNumber.text = "No Payment Method"
                    self.paymentButtonText.text = "ADD"
                    
                    self.showPaymentRemove(false, duration: 0.3)
                    
                    let alertController = MDCAlertController(title: "Card Removed!", message: "You're card was removed successfully.")
                    
                    let action = MDCAlertAction(title:"OK")
                    
                    alertController.addAction(action)
                    self.present(alertController, animated:true, completion: nil)
                } else {
                    Util.defaultAlert(self, title: "Uh Oh...", message: "Something went wrong while removing your card from our server. Try again later.", action: {})
                }
            }
        }
    }
    
    var tableCSHeight: CGFloat = 0
    
    @IBAction func myTicketsExpandPressed(_ sender: Any) {
        if myTicketsContentHeight.constant == 0 {
            myTicketsContentHeight.constant = reservedTix.isEmpty ? 54 : tableCSHeight + 36
            myTicketsContent.clipsToBounds = false
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.myTicketsExpand.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2));
            }
        } else {
            myTicketsContentHeight.constant = 0
            myTicketsContent.clipsToBounds = true
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.myTicketsExpand.transform = CGAffineTransform(rotationAngle: 0)
            })
        }
    }
    
    @IBAction func pastPartiesExpandPressed(_ sender: Any) {
        if pastPartiesContentHeight.constant == 0 {
            if pastParties!.count >= 2 {
                pastPartiesContentHeight.constant = 100
            } else if pastParties!.count == 1 {
                pastPartiesContentHeight.constant = 65
            } else {
                pastPartiesContentHeight.constant = 55
            }
            
            pastPartiesBottom.constant = pastParties!.isEmpty ? 18 : 26
            pastPartiesContent.clipsToBounds = false
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                self.pastPartiesExpand.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2));
            }
        } else {
            pastPartiesContentHeight.constant = 0
            pastPartiesBottom.constant = 18
            pastPartiesContent.clipsToBounds = true
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.pastPartiesExpand.transform = CGAffineTransform(rotationAngle: 0)
            })
        }
    }
    
    @IBAction func pastPartiesViewAllPressed(_ sender: Any) {
        if user == nil || user!.pastParties.isEmpty {
            let message = MDCSnackbarMessage()
            message.text = "No past parties. Once you attend a party with Viibe, you will be able to see it here."
            message.duration = 2
            
            MDCSnackbarManager.show(message)
            return
        }
        
        let vc = PastPartiesController(nibName: "PastPartiesController", bundle: nil)
    ViewController.instance?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func show(vc: UIViewController, close: @escaping () -> Void) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        self.close = close;
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func backPressed(_ sender: Any) {
        if (close != nil) {
            close!()
        }
    }
    
    func signOutPressed() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.deleteAll()
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        ViewController.instance?.navigationController?.pushViewController(vc, animated: true)
        ViewController.instance?.navigationController?.viewControllers = [vc]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservedTix.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let party = reservedTix[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTicketCell", for: indexPath) as! MyTicketCell
        cell.setup(party: party, vc: ViewController.instance!)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        self.tableCSHeight = self.myTicketsTable!.contentSize.height
        self.myTicketsTableHeight.constant = self.myTicketsTable!.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}

extension AccountCardController: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        ViewController.instance?.navigationController?.isNavigationBarHidden = true
        ViewController.instance?.navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            ViewController.instance?.navigationController?.isNavigationBarHidden = true
            ViewController.instance?.navigationController?.popViewController(animated: true)
            return
        }
        
        guard let userUnmanaged = Util.getCurrentUserUnmanaged(),
            let card = token.card else {
            ViewController.instance?.navigationController?.isNavigationBarHidden = true
            ViewController.instance?.navigationController?.popViewController(animated: true)
            return
        }
        
        let params = ["UserID": userUnmanaged.id,
                      "StripeToken": token.tokenId,
                      "CardLast4": card.last4,
                      "CardBrand": Util.cardBrand(brand: card.brand),
                      "ServerVersion": "2"]
        
        Alamofire.request(Config.shared.setCardURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            let user = Util.getCurrentUser(realm)
            
            if (response.response?.statusCode != 200) {
                Util.defaultAlert(self, title: "Uh Oh...", message: "Something went wrong while uploading your card to our server. Try again later.", action: {
                    ViewController.instance?.navigationController?.isNavigationBarHidden = true
                    ViewController.instance?.navigationController?.popViewController(animated: true)

                })
                
                ViewController.instance?.navigationController?.isNavigationBarHidden = true
                ViewController.instance?.navigationController?.popViewController(animated: true)
                return
            }
            
            if let json = response.result.value as? [String: Any],
                let success = json["Success"] as? Bool {
                if success {
                    try! realm.write {
                        if let img = UIImage(named: "\(Util.cardBrand(brand: token.card!.brand))CardImage") {
                            let imgData = UIImagePNGRepresentation(img)!
                            user?.creditCardImage = imgData
                            
                            self.creditCardImage.image = img
                        }
                    
                        user?.creditCardLast4 = token.card!.last4
                    }
                
                    self.creditCardImage.isHidden = false
                    self.creditCardNumber.text = "∙∙∙∙ ∙∙∙∙ ∙∙∙∙ \(token.card!.last4)"
                    self.paymentButtonText.text = "EDIT"
                    
                    ViewController.instance?.navigationController?.isNavigationBarHidden = true
                    ViewController.instance?.navigationController?.popViewController(animated: true)
                    
                    self.showPaymentRemove(true, duration: 0.3)
                    
                    let alertController = MDCAlertController(title: "Card Added!", message: "You're all set. Your card was added successfully.")
                    
                    let action = MDCAlertAction(title:"OK") { (action) in
                        
                    }
                    alertController.addAction(action)
                    ViewController.instance?.present(alertController, animated:true, completion: nil)
                } else {
                    let alertController = MDCAlertController(title: "Uh Oh...", message: "Something went wrong while uploading your card to our server. Try again later.")
                    
                    let action = MDCAlertAction(title:"OK") { (action) in
                        ViewController.instance?.navigationController?.isNavigationBarHidden = true
                        ViewController.instance?.navigationController?.popViewController(animated: true)
                    }
                    
                    alertController.addAction(action)
                    self.present(alertController, animated:true, completion: nil)
                }
            }
        }
    }
}
