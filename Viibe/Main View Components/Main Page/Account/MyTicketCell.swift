//
//  MyTicketCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 1/12/19.
//  Copyright © 2019 Brodly. All rights reserved.
//

import UIKit

class MyTicketCell: UITableViewCell {
    
    var party: vParty?
    var vc: UIViewController?
    
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var viewParty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(party: vParty, vc: UIViewController) {
        self.party = party
        self.vc = vc
        
        partyName.text = party.name
    }
    
    @IBAction func viewPressed(_ sender: Any) {
        if party == nil || vc == nil { return }
        
        let card = ViibeCard()
        card.show(vc!, newControllerVC: vc!, party: party!, type: "TICKET", selected: party?.tickets.first!) { }
    }
}
