//
//  SearchResultsController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/15/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation

class FeedSearchResultsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var posts: [vPost] = []
    var filtered: [vPost] = []
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noResults: UILabel!
    
    var currSearch = ""
    var isShown = false
    
    var city = ""
    var gender = true;
    var user: vUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Util.getCurrentUserUnmanaged() {
            self.user = user
            self.gender = user.gender
        }
        
        city = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        table.register(UINib(nibName: "FeedPostCell", bundle: nil), forCellReuseIdentifier: "FeedPostCell")
        table.register(UINib(nibName: "FeedEventCell", bundle: nil), forCellReuseIdentifier: "FeedEventCell")
        
        table.dataSource = self
        table.delegate = self
        table.allowsSelection = false
        table.separatorStyle = .singleLine
        table.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        table.separatorColor = UIColor(hex: "CCCCCC")
        table.tableFooterView = UIView()
        
        load()
    }
    
    func filter(_ str: String) {
        currSearch = str
        if table == nil { return }
        
        let prefilter = filtered
        
        filtered = posts.filter({ (post) -> Bool in
            return str.isEmpty || post.contains(str: str)
        })
        
        noResults.isHidden = !filtered.isEmpty
        
        if !str.isEmpty {
            filtered.sort { (a, b) -> Bool in
                if a.startsWith(str: str) && !b.startsWith(str: str) {
                    return true
                } else if b.startsWith(str: str) && !a.startsWith(str: str) {
                    return false
                }
                
                return a < b
            }
        }
        
        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]
        
        for i in 0..<prefilter.count {
            let a = prefilter[i]
            
            if !filtered.contains(a) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }
        
        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]
            
            if !prefilter.contains(a) {
                inserted.append(IndexPath(row: index, section: 0))
            }
            
            index += 1
        }
        
        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }
            
            let a = prefilter[i]
            let filteredIndex = filtered.index(of: a)
            
            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }
        
        self.table.beginUpdates()
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)
        
        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }
        
        self.table.endUpdates()
    }
    
    func load() {
        let realm = try! Realm()
        let realmPosts = realm.objects(Post.self)
        
        posts = []
        for post in realmPosts {
            posts.append(vPost(post))
        }
        
        posts.sort()
        filter(currSearch)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = filtered[indexPath.row]
        
        if post.type == "EVENT" {
            let cell = table.dequeueReusableCell(withIdentifier: "FeedEventCell", for: indexPath) as! FeedEventCell
            cell.setup(post, vc: self)
            return cell
        }
        
        let cell = table.dequeueReusableCell(withIdentifier: "FeedPostCell", for: indexPath) as! FeedPostCell
        cell.setup(post, vc: self)
        return cell
        
    }
    
}
