//
//  HostSearchResultsController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/15/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift

class HostSearchResultsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var hosts: [vHost] = []
    var filtered: [vHost] = []
    
    var currSearch = ""
    var isShown = false
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noResults: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.register(UINib(nibName: "ListHostCell", bundle: nil), forCellReuseIdentifier: "ListHostCell")
        
        table.delegate = self
        table.dataSource = self
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 160
        table.tableFooterView = UIView()
        
        load()
    }
    
    func filter(_ str: String) {
        currSearch = str
        if table == nil { return }
        
        let prefilter = filtered
        
        filtered = hosts.filter({ (a) -> Bool in
            return str.isEmpty || a.name.uppercased().contains(str.uppercased())
        })
        
        noResults.isHidden = !filtered.isEmpty
        
        filtered.sort { (a, b) -> Bool in
            if a.name.lowercased().starts(with: str.lowercased()) && !b.name.lowercased().starts(with: str.lowercased()) {
                return true
            } else if b.name.lowercased().starts(with: str.lowercased()) && !a.name.lowercased().starts(with: str.lowercased()) {
                return false
            }
            
            return true
        }
        
        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]
        
        for i in 0..<prefilter.count {
            let a = prefilter[i]
            
            if !filtered.contains(a) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }
        
        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]
            
            if !prefilter.contains(a) {
                inserted.append(IndexPath(row: index, section: 0))
            }
            
            index += 1
        }
        
        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }
            
            let a = prefilter[i]
            let filteredIndex = filtered.index(of: a)
            
            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }
        
        self.table.beginUpdates()
        
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)
        
        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }
        
        self.table.endUpdates()
    }
    
    func load() {
        let realm = try! Realm()
        
        let realmHosts = realm.objects(Host.self)
        
        hosts = []
        for host in realmHosts {
            hosts.append(vHost(host))
        }
        
        filter(currSearch)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let host = filtered[indexPath.row]
        
        let cell = table.dequeueReusableCell(withIdentifier: "ListHostCell", for: indexPath) as! ListHostCell
        cell.vc = ViewController.instance
        cell.setup(host)
        return cell
        
    }
}
