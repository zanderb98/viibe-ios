//
//  SearchResultsController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/15/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation

class ListSearchResultsController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var parties: [vParty] = []
    var filtered: [vParty] = []
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noResults: UILabel!
    
    var currSearch = ""
    var isShown = false
    
    var city = ""
    var gender = true;
    var user: vUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = Util.getCurrentUserUnmanaged() {
            self.user = user
            self.gender = user.gender
        }
        
        city = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        table.register(UINib(nibName: "ListPartyCell", bundle: nil), forCellReuseIdentifier: "ListPartyCell")
        
        table.delegate = self
        table.dataSource = self
    
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        load()
    }
    
    func filter(_ str: String) {
        currSearch = str
        if table == nil { return }
        
        let prefilter = filtered
        
        filtered = parties.filter({ (a) -> Bool in
            return str.isEmpty || a.name.uppercased().contains(str.uppercased())
        })
        
        noResults.isHidden = !filtered.isEmpty
        
        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]
        
        for i in 0..<prefilter.count {
            let a = prefilter[i]
            
            if !filtered.contains(a) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }
        
        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]
            
            if !prefilter.contains(a) {
                inserted.append(IndexPath(row: index, section: 0))
            }
            
            index += 1
        }
        
        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }
            
            let a = prefilter[i]
            let filteredIndex = filtered.index(of: a)
            
            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }
        
        self.table.beginUpdates()
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)
        
        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }
        
        self.table.endUpdates()
    }
    
    func load() {
        let realm = try! Realm()
        let realmParties = realm.objects(Party.self).filter { (party) -> Bool in
            return (Util.isPartyToday(party: party) || Date().compare(party.date) == .orderedAscending) && !party.isPastParty && !party.isReserved && party.open
        }
        
        parties = []
        for party in realmParties {
            parties.append(vParty(party))
        }
        
        let userCoord = CLLocation(latitude: UserDefaults.standard.double(forKey: "LastKnownLat"),
                                   longitude: UserDefaults.standard.double(forKey: "LastKnownLng"))
        
        parties.sort { (a, b) -> Bool in
            if a.city == city && b.city != city {
                return true
            } else if a.city != city && b.city == city {
                return false
            }
            
            if a.date == b.date {
                return CLLocation(latitude: a.lat, longitude: a.lng).distance(from: userCoord) < CLLocation(latitude: b.lat, longitude: b.lng).distance(from: userCoord)
            }
            
            return a.date.compare(b.date) == .orderedAscending
        }
        
        filter(currSearch)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let party = filtered[indexPath.row]
        
        let cell = table.dequeueReusableCell(withIdentifier: "ListPartyCell", for: indexPath) as! ListPartyCell
        cell.setup(party, newVC: self, gender: gender)
        return cell
        
    }

}
