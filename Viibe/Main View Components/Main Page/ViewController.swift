//
//  ViewController.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/23/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import Mapbox
import MaterialComponents
import RealmSwift
import Lottie
import Floaty

class ViewController: UIViewController, MDCTabBarDelegate {
    static var instance: ViewController?
    
    @IBOutlet weak var pagerContainer: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var locationPermsView: UIVisualEffectView!
    @IBOutlet weak var locationPermsButton: UIView!
    @IBOutlet weak var locationPermsDesc: UILabel!
    @IBOutlet weak var locationAnim: LOTAnimationView!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchBtnTrailing: NSLayoutConstraint!
    @IBOutlet weak var searchBackground: UIView!
    
    var favoritesCard: FavoritesCard?
    var accountCard: AccountCard?
    var tabs: [UITabBarItem] = []

    @IBOutlet weak var tabContainer: UIView!
    var tabBar: MDCTabBar!
    
    var showingFavoritesWarning = false
    
    var pageContainer: UIPageViewController!
    
    var mapVC: MapController?
    var listVC: ListCardController?
    var feedVC: FeedController?
    var accountVC: AccountCardController?
    
    var currPage = 0
    var selectedItem: UITabBarItem?
    
    var homeSelected: UIImage!
    var mapSelected: UIImage!
    var feedSelected: UIImage!
    var accountSelected: UIImage!

    var homeUnselected: UIImage!
    var mapUnselected: UIImage!
    var feedUnselected: UIImage!
    var accountUnselected: UIImage!
    
    //Search
    
    var searchPageContainer: UIPageViewController!
    @IBOutlet weak var searchTabsContainer: UIView!
    @IBOutlet weak var tab1Title: UILabel!
    @IBOutlet weak var tab1IndicatorW: NSLayoutConstraint!
    @IBOutlet weak var tab2Title: UILabel!
    @IBOutlet weak var tab2IndicatorW: NSLayoutConstraint!
    @IBOutlet weak var tab3Title: UILabel!
    @IBOutlet weak var tab3IndicatorW: NSLayoutConstraint!
    
    var selectedTitle: UILabel!
    var selectedIndicatorW: NSLayoutConstraint!
    
    var listSearchVC: ListSearchResultsController!
    var hostSearchVC: HostSearchResultsController!
    var feedSearchVC: FeedSearchResultsController!

    override func viewDidLoad() {
        super.viewDidLoad()
        ViewController.instance = self
        self.definesPresentationContext = true
        
        locationAnim.setAnimation(named: "location2")
        locationAnim.loopAnimation = true
        locationAnim.play()
        
        locationPermsButton.addDropShadow()
        locationPermsButton.setCornerRadius(radius: 5)
        
        let locPermsRec = UITapGestureRecognizer(target: self, action: #selector(locationPermsButtonPressed))
        locationPermsButton.addGestureRecognizer(locPermsRec)
        
        locationPermsDesc.attributedText = NSMutableAttributedString().normal("Please press the button below and select ").bold("Location → Always").normal(". We need this information to be able to know when you leave the party to give accurate pricing.")
        
        topBar.addDropShadow(opacity: 0.2)
        
        tabBar = MDCTabBar(frame: CGRect(x: 0,
                                             y: 0, width: tabContainer.bounds.width,
                                             height: tabContainer.bounds.height))
        
        homeSelected = resizeImage(image: #imageLiteral(resourceName: "tab_home_selected"), newWidth: 32)
        homeUnselected = resizeImage(image: #imageLiteral(resourceName: "tab_home_unselected"), newWidth: 32)
        
        mapSelected = resizeImage(image: #imageLiteral(resourceName: "tab_map_selected"), newWidth: 26)
        mapUnselected = resizeImage(image: #imageLiteral(resourceName: "tab_map_unselected"), newWidth: 26)
        
        feedSelected = resizeImage(image: #imageLiteral(resourceName: "tab_feed_selected"), newWidth: 32)
        feedUnselected = resizeImage(image: #imageLiteral(resourceName: "tab_feed_unselected"), newWidth: 32)
        
        accountSelected = resizeImage(image: #imageLiteral(resourceName: "tab_account_selected"), newWidth: 24)
        accountUnselected = resizeImage(image: #imageLiteral(resourceName: "tab_account_unselected"), newWidth: 24)
        
        let listItem = UITabBarItem(title: "LIST", image: homeSelected, tag: 0)
        let mapItem = UITabBarItem(title:"MAP", image: mapUnselected, tag: 0)
        let feedItem = UITabBarItem(title: "FEED", image: feedUnselected, tag: 0)
        let accountItem = UITabBarItem(title: "ACCOUNT", image: accountUnselected, tag: 0)
        
        selectedItem = listItem
        
        tabs = [listItem, mapItem, feedItem, accountItem]
        
        tabBar.items = tabs
        tabBar.delegate = self
        tabBar.tintColor = UIColor.clear

        tabBar.itemAppearance = .images
        tabBar.alignment = .justified
        tabBar.setSelectedItem(mapItem, animated: false)
        
        tabContainer.addSubview(tabBar)
        tabContainer.addDropShadow(offset: CGSize(width: 0, height: -5))
        
        selectedTitle = tab1Title
        selectedIndicatorW = tab1IndicatorW
        
        searchField.attributedPlaceholder = NSAttributedString(string: "Type to search...",
                                                               attributes: [NSForegroundColorAttributeName: UIColor(hex: "F2F2F2")])
        
        searchTabsContainer.addDropShadow(offset: CGSize(width: 0, height: 5))

        setupPager()
        setupSearchPager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setupPager() {
        mapVC = MapController(nibName: "MapController", bundle: nil)
        listVC = ListCardController(nibName: "ListCardController", bundle: nil)
        feedVC = FeedController(nibName: "FeedController", bundle: nil)
        accountVC = AccountCardController(nibName: "AccountCardController", bundle: nil)

        // Create the page container
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.setViewControllers([listVC!], direction: .forward, animated: false, completion: nil)
        
        (pageContainer.view.subviews.filter { $0 is UIScrollView }.first as? UIScrollView)?.isScrollEnabled = false
        
        // Add it to the view
        pagerContainer.insertSubview(pageContainer.view, at: 0)
        pageContainer.view.frame = CGRect(x: 0, y: 0, width: pagerContainer.frame.width, height: pagerContainer.frame.height)
        pageContainer.view.backgroundColor = UIColor(hex: "EFEFEF")
    }
    
    func setupSearchPager() {
        listSearchVC = ListSearchResultsController(nibName: "ListSearchResultsController", bundle: nil)
        hostSearchVC = HostSearchResultsController(nibName: "HostSearchResultsController", bundle: nil)
        feedSearchVC = FeedSearchResultsController(nibName: "FeedSearchResultsController", bundle: nil)
        
        // Create the page container
        searchPageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        searchPageContainer.setViewControllers([listSearchVC!], direction: .forward, animated: false, completion: nil)
        
        // Add it to the view
        view.insertSubview(searchPageContainer.view, aboveSubview: searchBackground)
        searchPageContainer.view.frame = CGRect(x: 0, y: searchTabsContainer.frame.maxY, width: view.frame.width, height: view.frame.height - searchTabsContainer.frame.maxY)
        searchPageContainer.view.backgroundColor = UIColor.white
        searchPageContainer.view.isHidden = true
    }
    
    func locationPermsButtonPressed() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        if selectedItem != item {
            selectedItem?.image = unselectedImage(pos: currPage)
            selectedItem = item
        }
        
        if item.title! == "LIST" {
            if (currPage == 0) {
                return
            }
            
            searchTab1Pressed(self)
            
            pageContainer.setViewControllers([listVC!], direction: .reverse, animated: true, completion: nil)
            
            currPage = 0
        } else if item.title! == "MAP" {
            if (currPage == 1) {
                return
            }
            
            searchTab1Pressed(self)
            
            pageContainer.setViewControllers([mapVC!], direction: currPage < 1 ? .forward : .reverse, animated: true, completion: nil)
            
            currPage = 1
        } else if item.title! == "FEED" {
            if (currPage == 2) {
                return
            }
            
            searchTab3Pressed(self)
            
            pageContainer.setViewControllers([feedVC!], direction: currPage < 2 ? .forward : .reverse, animated: true, completion: nil)
            
            currPage = 2
        } else if item.title! == "ACCOUNT" {
            if (currPage == 3) {
                return
            }
            
            if Util.getLoginStatus() == .skipped {
                Util.clearLoginStatus()
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
                self.navigationController?.pushViewController(vc, animated: true)
                self.navigationController?.viewControllers = [vc]
                return
            }
            
            searchTab1Pressed(self)
            
            pageContainer.setViewControllers([accountVC!], direction: currPage < 3 ? .forward : .reverse, animated: true, completion: nil)
            
            currPage = 3
        }
        
        selectedItem?.image = selectedImage(pos: currPage)
    }
    
    func unselectedImage(pos: Int) -> UIImage {
        if pos == 0 {
            return homeUnselected
        } else if pos == 1 {
            return mapUnselected
        } else if pos == 2 {
            return feedUnselected
        } else {
            return accountUnselected
        }
    }

    func selectedImage(pos: Int) -> UIImage {
        if pos == 0 {
            return homeSelected
        } else if pos == 1 {
            return mapSelected
        } else if pos == 2 {
            return feedSelected
        } else {
            return accountSelected
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: newWidth, height: newHeight), false, 0)
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        searchBtnTrailing.constant = UIScreen.main.bounds.width - 28
        Util.fadeIn(views: [searchContainer, searchBackground, searchTabsContainer], duration: 0.3)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.topBar.layoutIfNeeded()
        }) { (success) in
            self.searchField.becomeFirstResponder()
        }
    }
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        closeSearch()
    }
    
    @IBAction func searchBackgroundPressed(_ sender: Any) {
        closeSearch()
    }
    
    func closeSearch() {
        searchField.text = ""
        searchResultsVisible = false
        searchBtnTrailing.constant = 12
        
        Util.fadeOut(views: [searchContainer, searchBackground, searchTabsContainer, searchPageContainer.view], duration: 0.3)
        self.view.endEditing(true)
        
        currPage == 2 ? searchTab3Pressed(self) : searchTab1Pressed(self)
        
        UIView.animate(withDuration: 0.5) {
            self.topBar.layoutIfNeeded()
        }
    }
    
    var searchResultsVisible = false
    
    @IBAction func searchTextChanged(_ sender: Any) {
        if searchResultsVisible && searchField.text!.isEmpty {
            Util.fadeOut(views: [searchPageContainer.view], duration: 0.3)
            searchResultsVisible = false
        } else {
            if !searchResultsVisible {
                Util.fadeIn(views: [searchPageContainer.view], duration: 0.3)
                searchResultsVisible = true
            }
        }
        
        listSearchVC.filter(searchField.text!)
        hostSearchVC.filter(searchField.text!)
        feedSearchVC.filter(searchField.text!)
    }
    
    @IBAction func searchTab1Pressed(_ sender: Any) {
        switchSearchTabs(newTitle: tab1Title, newIndicatorW: tab1IndicatorW, oldTitle: selectedTitle, oldIndicatorW: selectedIndicatorW)
        
        searchPageContainer.setViewControllers([listSearchVC!], direction: .reverse, animated: !searchPageContainer.view.isHidden, completion: nil)
    }
    
    @IBAction func searchTab2Pressed(_ sender: Any) {
        searchPageContainer.setViewControllers([hostSearchVC], direction: selectedTitle == tab1Title ? .forward : .reverse, animated: !searchPageContainer.view.isHidden, completion: nil)
        
        switchSearchTabs(newTitle: tab2Title, newIndicatorW: tab2IndicatorW, oldTitle: selectedTitle, oldIndicatorW: selectedIndicatorW)
    }
    
    @IBAction func searchTab3Pressed(_ sender: Any) {
        switchSearchTabs(newTitle: tab3Title, newIndicatorW: tab3IndicatorW, oldTitle: selectedTitle, oldIndicatorW: selectedIndicatorW)
        
        searchPageContainer.setViewControllers([feedSearchVC], direction: .forward, animated: !searchPageContainer.view.isHidden, completion: nil)
    }
    
    func switchSearchTabs(newTitle: UILabel, newIndicatorW: NSLayoutConstraint,
                          oldTitle: UILabel, oldIndicatorW: NSLayoutConstraint) {
        
        oldIndicatorW.constant = 0
        newIndicatorW.constant = 75
        
        selectedTitle = newTitle
        selectedIndicatorW = newIndicatorW
        
        UIView.animate(withDuration: 0.3) {
            oldTitle.textColor = UIColor(hex: "7888A3")
            newTitle.textColor = Colors.viibeOrange
            
            self.searchTabsContainer.layoutIfNeeded()
        }
    }
    
    func openParty(id: String) {
        mapVC?.openParty(id: id)
    }
    
    func updatedCity() {
        mapVC?.updatedCity()
    }
    
    func hideLocPermsView(_ hidden: Bool) {
        locationPermsView.isHidden = hidden
    }
    
    var keyboardOpen = false
    
    func keyboardWillShow(notification: NSNotification) {
        if keyboardOpen {
            return
        }
        
        keyboardOpen = true
        
        self.pageContainer.view.frame.size.height += tabContainer.frame.height
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if !keyboardOpen {
            return
        }
        
        keyboardOpen = false
        
        self.pageContainer.view.frame.size.height -= tabContainer.frame.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

