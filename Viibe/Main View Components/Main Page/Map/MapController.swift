//
//  MapController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/1/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Mapbox
import MaterialComponents
import RealmSwift
import Lottie

class MapController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var cityButton: UIView!
    @IBOutlet weak var cityText: UILabel!
    @IBOutlet weak var cityMarkerImage: UIImageView!
    @IBOutlet weak var hostShadow: UIView!
    @IBOutlet weak var centerButton: UIView!
    
    var centerButtonShowing = false
    var animating = false
    var centering = false
    
    var locationManager: CLLocationManager?
    var mapView: MGLMapView?
    
    var currLat = 42.3584308, currLong = -71.0597763
    
    var currLocSet = false
    var closestCity = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        centerButton.isHidden = true
        centerButton.addDropShadow()
        
        cityMarkerImage.image = UIImage(named: "map-marker")!.withRenderingMode(.alwaysTemplate)
        
        let centerRec = UITapGestureRecognizer(target: self, action: #selector(self.centerMap))
        centerButton.addGestureRecognizer(centerRec)
        
        mapView = MGLMapView(frame: view.bounds)
        mapView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView!.styleURL = MGLStyle.lightStyleURL()
        mapView!.attributionButton.isHidden = true
        
        mapView!.delegate = self
        mapView!.showsUserLocation = true
        
        mapView!.tintColor = UIColor(hex: "777777")
        
        mapView!.setCenter(CLLocationCoordinate2D(latitude:  UserDefaults.standard.double(forKey: "LastKnownLat"), longitude: UserDefaults.standard.double(forKey: "LastKnownLng")),
                           zoomLevel: 12, animated: false)
        
        view.addSubview(mapView!)
        view.sendSubview(toBack: mapView!)
        
        hostShadow.addDropShadow()

        cityButton.layer.shadowColor = UIColor.black.cgColor
        cityButton.layer.shadowOpacity = 0.3
        cityButton.layer.shadowOffset = CGSize(width: 2, height: 2)
                
        if let city = UserDefaults.standard.string(forKey: "SelectedCity") {
            cityText.text = city.uppercased()
            closestCity = city
        }
        
        LocationManager.shared.requestLocationPermission { (status) in
            LocationManager.shared.getLocationUpdates({ (loc) in
                self.currLat = loc.latitude
                self.currLong = loc.longitude
                
                UserDefaults.standard.set(self.currLat, forKey: "LastKnownLat")
                UserDefaults.standard.set(self.currLong, forKey: "LastKnownLng")
                
                self.closestCity = Util.closestCity(lat: self.currLat, lng: self.currLong)
                self.cityText.text = self.closestCity.uppercased()
                
                UserDefaults.standard.set(self.closestCity, forKey: "SelectedCity")
                
                let realm = try! Realm()
                let user = Util.getCurrentUser(realm)
                
                try! realm.write {
                    user?.lastKnownLat = self.currLat
                    user?.lastKnownLng = self.currLong
                }
                
                if (!self.currLocSet) {
                    self.mapView?.setCenter(CLLocationCoordinate2D(latitude: self.currLat, longitude: self.currLong), zoomLevel: 12, animated: false)
                    
                    HostLoader.loadHosts {
                        PartyLoader.loadParties(latitude: self.currLat, longitude: self.currLong, complete: {
                            self.setUpMap()
                        })
                    }
                    
                    self.currLocSet = true
                }
            })
            
            self.checkLocationPerms()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUpMap), name: Notifications.PartiesUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkLocationPerms), name: Notifications.EnteredForeground, object: nil)
    }
    
    func centerMap() {
        hideCenterButton()
        centering = true
        
        mapView?.setZoomLevel(12, animated: true)
        mapView?.setCenter(CLLocationCoordinate2D(latitude: currLat, longitude: currLong), zoomLevel: 12, direction: mapView!.direction, animated: true, completionHandler: {
            self.centerButtonShowing = false
            self.centering = false
        })
        
        let selected = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        if !closestCity.isEmpty && selected != closestCity {
            UserDefaults.standard.set(closestCity, forKey: "SelectedCity")
            
            self.cityText.text = closestCity.uppercased()
            setUpMap()
        }
    }
    
    func mapViewRegionIsChanging(_ mapView: MGLMapView) {
        let distance = CLLocation.distance(from: mapView.centerCoordinate, to: CLLocationCoordinate2D(latitude: currLat, longitude: currLong))
        
        let currClosest = Util.closestCity(lat: mapView.centerCoordinate.latitude, lng: mapView.centerCoordinate.longitude)
        
        if !centering && cityText.text != currClosest.uppercased() {
            UserDefaults.standard.set(currClosest, forKey: "SelectedCity")
            self.cityText.text = currClosest.uppercased()
            setUpMap()
            
        }
        
        if !centerButtonShowing {
            if (distance > 1500) {
                showCenterButton()
            }
        } else {
            if (distance < 1500) {
                hideCenterButton()
                centerButtonShowing = false
            }
        }
    }
    
    func showCenterButton() {
        if animating || centering {
            return
        }
        
        centerButtonShowing = true
        centerButton.isHidden = false
        
        let hostTrailing = view.getConstraintByID(id: "hostTrailing")!
        
        centerButton.layoutIfNeeded()
        
        hostTrailing.constant += 40
        
        animating = true
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.animating = false
        }
    }
    
    func hideCenterButton() {
        if animating || centering {
            return
        }
        
        let hostTrailing = view.getConstraintByID(id: "hostTrailing")!
        
        hostTrailing.constant -= 40
        
        animating = true
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            self.centerButton.isHidden = true
            self.animating = false
        })
    }
    
    func checkLocationPerms() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways {
            ViewController.instance?.hideLocPermsView(true)
        } else {
            ViewController.instance?.hideLocPermsView(true)
        }
    }

    @IBAction func cityButtonPressed(sender: Any?) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        ViewController.instance?.navigationController?.view.layer.add(transition, forKey: nil)
        
        let cityController = CitySelectionController(nibName: "CitySelectionController", bundle: nil)
        ViewController.instance?.navigationController?.pushViewController(cityController, animated: false)
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        setUpMap()
    }
    
    func setUpMap() {
        if let currAnnotations = mapView?.annotations {
            mapView!.removeAnnotations(currAnnotations)
        }
        
        let city = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        let realm = try! Realm()
        let parties = realm.objects(Party.self).filter { (party) -> Bool in
            return !party.isPastParty &&
                !party.isMyParty &&
                !party.isBouncedParty &&
                !party.isReserved &&
                party.open &&
                party.city == city &&
                Util.isPartyToday(party: party)
        }
        
        for party in parties {
            let annotation = ViibePointAnnotation()
            annotation.party = vParty(party)
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: party.lat, longitude: party.lng)
            mapView?.addAnnotation(annotation)
        }
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        guard let viibeAnnotation = annotation as? ViibePointAnnotation,
            let party = viibeAnnotation.party else {
                return nil
        }
        
        let reuseIdentifier = "\(party.id)"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = ViibeAnnotation(reuseIdentifier: reuseIdentifier)
            annotationView!.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        }
        
        let realm = try! Realm()
        if let _ = realm.objects(Party.self).filter("id == 'h\(party.id)'").first {
            (annotationView as! ViibeAnnotation).color = UIColor(hex: "FFC107")
        } else if let _ = realm.objects(Party.self).filter("id == 'b\(party.id)'").first {
            (annotationView as! ViibeAnnotation).color = UIColor(hex: "FFC107")
        }
        
        (annotationView as! ViibeAnnotation).party = party
        (annotationView as! ViibeAnnotation).setup()
        
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        var party: vParty? = nil
        
        if let a = annotation as? ViibePointAnnotation, let p = a.party {
            party = p
        } else {
            //Tapped user location
            
            var foundParty: vParty? = nil
            
            guard let annotations = mapView.annotations else {
                return
            }
            
            for ann in annotations {
                if ann is ViibePointAnnotation {
                    let loc1 = CLLocation(latitude: ann.coordinate.latitude,
                                          longitude: ann.coordinate.longitude)
                    let loc2 = CLLocation(latitude: annotation.coordinate.latitude,
                                          longitude: annotation.coordinate.longitude)
                    
                    if loc1.distance(from: loc2) < 100 {
                        foundParty = (ann as! ViibePointAnnotation).party!
                    }
                }
            }
            
            if foundParty != nil {
                party = foundParty
            } else {
                return
            }
        }
        
        let realm = try! Realm()
        if let reserved = realm.objects(Party.self).filter("id == 'r\(party!.id)'").first {
            party!.key = reserved.key
            party!.isReserved = true
            
            let alert = ViibeCard()
            alert.show(ViewController.instance!, party: party!)
        } else {
            let alert = ViibeCard()
            alert.show(ViewController.instance!, party: party!)
        }
        
        mapView.deselectAnnotation(annotation, animated: false)
    }
    
    func pushHostButtonUp() {
        let hostBottom = view.getConstraintByID(id: "hostBottom")
        hostBottom?.constant = 90
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 2.7, repeats: false, block: { (_) in
            hostBottom?.constant = 44
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (_) in
                ViewController.instance?.showingFavoritesWarning = false
            })
        })
    }
    
    func openParty(id: String) {
        let realm = try! Realm()
        if let party = realm.objects(Party.self).first(where: { (p) -> Bool in
            return p.id == id
        }) {
            let alert = ViibeCard()
            alert.show(ViewController.instance!, party: vParty(party))
        } else {
            Util.defaultAlert(self, title: "Couldn't Open Party")
        }
    }
    
    func updatedCity() {
        guard let city = UserDefaults.standard.string(forKey: "SelectedCity") else {
            return
        }
        
        cityText.text = city.uppercased()
        setUpMap()
        
        if let loc = Util.locationFor(city: city) {
            mapView!.setCenter(CLLocationCoordinate2D(latitude:  loc.0, longitude: loc.1),
                               zoomLevel: 12, animated: false)
            
            if !centerButtonShowing {
                showCenterButton()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
