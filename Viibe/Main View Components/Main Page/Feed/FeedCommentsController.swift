//
//  FeedCommentsController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class FeedCommentsController: UIViewController, UITableViewDataSource {
    var post: vPost?

    @IBOutlet weak var topCard: UIView!
    @IBOutlet weak var headerTable: UITableView!
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var replyCard: UIView!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    
    var showKeyboard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topCard.addDropShadow()
        
        headerTable.register(UINib(nibName: "FeedPostCell", bundle: nil), forCellReuseIdentifier: "FeedPostCell")
        headerTable.register(UINib(nibName: "FeedEventCell", bundle: nil), forCellReuseIdentifier: "FeedEventCell")

        headerTable.dataSource = self
        headerTable.allowsSelection = false
        headerTable.separatorStyle = .none
        headerTable.addDropShadow()
        
        headerTable.rowHeight = UITableViewAutomaticDimension
        headerTable.estimatedRowHeight = 100
        
        table.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        
        table.dataSource = self
        table.allowsSelection = false
        table.separatorStyle = .none
                
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 50
        
        replyCard.addDropShadow()
        setupMessageField()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundTapped)))
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(ViibeCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViibeCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if showKeyboard {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                self.messageField.becomeFirstResponder()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == table ? post!.comments.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == headerTable {
            if post!.type == "EVENT" {
                let cell = headerTable.dequeueReusableCell(withIdentifier: "FeedEventCell", for: indexPath) as! FeedEventCell
                cell.setup(post!, vc: self, fromCommentsVC: true)
                return cell
            }
            
            let cell = headerTable.dequeueReusableCell(withIdentifier: "FeedPostCell", for: indexPath) as! FeedPostCell
            cell.setup(post!, vc: self, fromCommentsVC: true)
            return cell
        }
        
        let comment = post!.comments[post!.comments.count - indexPath.row - 1]
        
        let cell = table.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        cell.setup(comment, postID: post!.id, vc: self, heightUpdated: {
            tableView.beginUpdates()
            tableView.endUpdates()
        }, last: indexPath.row == post!.comments.count - 1)
        return cell
    }
    
    func setupMessageField() {
        sendBtn.setImage(#imageLiteral(resourceName: "send").withRenderingMode(.alwaysTemplate), for: .normal)
        sendBtn.imageView?.tintColor = UIColor(hex: "EFEFEF")
        
        messageField.addTarget(self, action: #selector(FeedCommentsController.textChanged), for: .editingChanged)
        messageField.attributedPlaceholder = NSAttributedString(string: "Enter a Comment",
                                                                attributes: [NSForegroundColorAttributeName: UIColor(hex: "999999")])
    }
    
    func textChanged() {
        sendBtn.imageView?.tintColor = messageField!.text!.isEmpty ? UIColor(hex: "EFEFEF") : Colors.viibeOrange
    }
    
    func backgroundTapped() {
        self.view.endEditing(true)
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        Util.checkUsername(self) {
            self.send()
        }
    }
    
    func send() {
        guard let u = Util.getCurrentUserUnmanaged(), let p = post else {
            return
        }
        
        if messageField == nil || messageField!.text!.isEmpty {
            return
        }
        
        let params = [
            "PostID": p.id,
            "UserID": u.id,
            "Text": messageField!.text!,
            "Parents": "[]",
            "ServerVersion": "2"
        ]
        
        Alamofire.request(Config.shared.sendCommentURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let commentJSON = json["Comment"] as? [String: Any] {
                
                let comment = Comment()
                comment.setup(commentJSON)
                
                let realm = try! Realm()
                if let rPost = realm.objects(Post.self).first(where: { (realmPost) -> Bool in
                    return realmPost.id == p.id
                }) {
                    try! realm.write {
                        rPost.comments.append(comment)
                    }
                }
                
                self.messageField!.text = ""
                self.view.endEditing(true)
                self.post!.comments.insert(vComment(comment), at: self.post!.comments.count)
                self.table!.beginUpdates()
                self.table!.insertRows(at: [IndexPath(row: self.post!.comments.count - 1, section: 0)], with: .automatic)
                self.table!.endUpdates()
                
                self.table!.reloadData()
                
                self.sendBtn.imageView?.tintColor = UIColor(hex: "DDDDDD")
                
            } else {
                Util.defaultAlert(self, title: "Failed to Send Comment")
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        headerTable.getConstraintByID(id: "headerTableHeight")?.constant = headerTable.contentSize.height + 12
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == headerTable {
            self.viewWillLayoutSubviews()
        }
    }

    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    var keyboardOpen = false
    var originalHeight: CGFloat = 0
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardOpen {
                originalHeight = self.view.frame.height
                self.view.frame.size.height -= keyboardSize.height
                
                keyboardOpen = true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if keyboardOpen {
            self.view.frame.size.height = originalHeight
            keyboardOpen = false
        }
    }
    
}
