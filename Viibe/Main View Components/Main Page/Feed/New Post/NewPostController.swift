//
//  NewPostController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/6/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
import RealmSwift

class NewPostController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var postText: UITextView!
    @IBOutlet weak var fab: MDCFloatingButton!
    
    var user: vUser?
    var placeholder = "Enter any info or questions you have about an event..."
    var textColor = UIColor(hex: "#777777")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topBar.layer.shadowColor = UIColor.black.cgColor
        topBar.layer.shadowOpacity = 0.25
        topBar.layer.shadowRadius = 3
        topBar.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        postText.text = placeholder
        postText.textColor = .lightGray
        postText.delegate = self
        
        fab.setImage(#imageLiteral(resourceName: "checkmark").withRenderingMode(.alwaysTemplate), for: .normal)
        fab.tintColor = .white
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.fab.alpha = self.fab.disabledAlpha
        }
        
        guard let u = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        self.user = u
        self.username.text = "@\(u.username)"
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(NewPostController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewPostController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == nil || textView.text.isEmpty {
            fab.alpha = fab.disabledAlpha
        } else {
            fab.alpha = 1
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = textColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        if fab.alpha == fab.disabledAlpha {
            return
        }
        
        guard var text = postText.text else { return }
        text = text.trim()
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let params = ["UserID": user.id,
                      "Text": text,
                      "Latitude": "\(lat)",
            "Longitude": "\(lng)",
            "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.newPostURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            
            if response.response?.statusCode != 200 {
                Util.defaultAlert(ViewController.instance!, title: "Oops...", message: "Failed to upload post")
                return
            }
            
            guard let json = response.result.value as? [String: Any],
                let postJSON = json["Post"] as? [String: Any] else {
                    Util.defaultAlert(ViewController.instance!, title: "Oops...", message: "Failed to upload post")
                    return
            }
            
            let post = Post()
            post.setup(json: postJSON)
            
            try! realm.write {
                realm.add(post)
            }
            
            ViewController.instance?.feedVC?.reload()
            
            self.navigationController?.popViewController(animated: true)
            Util.defaultAlert(ViewController.instance!, title: "Success!", message: "Post uploaded successfully!")
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    var keyboardOpen = false
    var originalHeight: CGFloat = 0
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardOpen {
                originalHeight = self.view.frame.height
                self.view.frame.size.height = originalHeight - keyboardSize.height
                keyboardOpen = true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if keyboardOpen {
            self.view.frame.origin.y = originalHeight
            keyboardOpen = false
        }
    }
}
