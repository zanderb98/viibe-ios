//
//  MessageCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/16/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class CommentCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var commentText: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var dropdown: UIImageView!
    @IBOutlet weak var showMore: UILabel!
    @IBOutlet weak var reply: UILabel!
    @IBOutlet weak var viewReplies: UILabel!
    @IBOutlet weak var replyLayout: UIView!
    @IBOutlet weak var repliesLayout: UIView!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    @IBOutlet weak var hideReplies: UILabel!
    @IBOutlet weak var repliesTable: UITableView!
    @IBOutlet weak var replyBack: UIImageView!
    
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var sendBtn: UIImageView!
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var separatorLeading: NSLayoutConstraint!
    
    @IBOutlet weak var showMoreWidth: NSLayoutConstraint!
    var showMoreW: CGFloat = 0
    @IBOutlet weak var showMoreLeading: NSLayoutConstraint!
    @IBOutlet weak var replyWidth: NSLayoutConstraint!
    var replyW: CGFloat = 0
    @IBOutlet weak var replyLeading: NSLayoutConstraint!
    @IBOutlet weak var replyLayoutHeight: NSLayoutConstraint!
    
    var repliesLayoutHeight: NSLayoutConstraint? = nil
    
    var vc: UIViewController?
    var heightUpdated: (() -> Void)?
    var indexPath: IndexPath?
    var comment: vComment?
    var postID = ""
    var partyID = ""
    
    var cmtHeight: CGFloat = 0
    var tableHeight: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        dropdown.image = #imageLiteral(resourceName: "dropdown-arrow").withRenderingMode(.alwaysTemplate)
        dropdown.tintColor = UIColor(hex: "#777777")
        
        repliesTable.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        repliesTable.delaysContentTouches = true
        repliesTable.canCancelContentTouches = true
        repliesTable.allowsSelection = false
        repliesTable.separatorStyle = .none
        repliesTable.rowHeight = UITableViewAutomaticDimension
        repliesTable.estimatedRowHeight = 100
        
        sendBtn!.image = #imageLiteral(resourceName: "send").withRenderingMode(.alwaysTemplate)
        sendBtn!.tintColor = UIColor(hex: "DDDDDD")
        sendBtn!.isUserInteractionEnabled = true
        
        messageField.addTarget(self, action: #selector(CommentCell.messageTextChanged), for: .editingChanged)
        messageField.attributedPlaceholder = NSAttributedString(string: "Enter a Comment",
                                                                attributes: [NSForegroundColorAttributeName: UIColor(hex: "999999")])
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(CommentCell.chatSendPressed))
        sendBtn!.addGestureRecognizer(rec)
    }
    
    func setup(_ cmt: vComment, postID: String, vc: UIViewController, heightUpdated: @escaping () -> Void, last: Bool) {
        self.vc = vc
        self.heightUpdated = heightUpdated
        self.comment = cmt
        self.postID = postID
        
        self.globalSetup(cmt: cmt, last: last)
    }
    
    func setup(_ cmt: vComment, partyID: String, vc: UIViewController, heightUpdated: @escaping () -> Void, last: Bool) {
        self.vc = vc
        self.heightUpdated = heightUpdated
        self.comment = cmt
        self.partyID = partyID
        
        self.globalSetup(cmt: cmt, last: last)
    }
    
    private func globalSetup(cmt: vComment, last: Bool) {
        if !cmt.hasParent {
            repliesTable.dataSource = self
            repliesTable.delegate = self
            
            (repliesTable as! InnerTableView).heightUpdated = {
                self.heightUpdated?()
            }
        }
        
        replyBack.image = #imageLiteral(resourceName: "arrow-right").withRenderingMode(.alwaysTemplate)
        replyBack.tintColor = Colors.viibeBlue
        replyBack.transform = CGAffineTransform(rotationAngle: .pi)
        
        separator.isHidden = last
        separatorLeading.constant = cmt.hasParent ? 16 : 0
        
        name.text = "@\(cmt.username)"
        date.text = "\(Util.messageTimestamp(date: cmt.date)) ago"
        
        showMoreW = "Show More".width(font: showMore.font)
        replyW = "Reply".width(font: reply.font)
        
        viewReplies.isHidden = cmt.comments.isEmpty
        viewReplies.text = "View \(cmt.numReplies() == 1 ? "Reply" : "\(cmt.numReplies()) Replies")"
        
        hideReply(hide: Util.getLoginStatus() == LoginStatus.skipped || cmt.hasParent)
        
        commentText.text = cmt.text
        cmtHeight = cmt.text.height(font: commentText.font, maxWidth: commentText.frame.width)
        
        if cmtHeight > 60 {
            textHeight.constant = 60
            hideShowMore(hide: false)
        } else {
            textHeight.constant = cmtHeight
            hideShowMore(hide: true)
        }
        
        if replyWidth.constant == 0 && showMoreWidth.constant == 0 && viewReplies.isHidden {
            setBottomSpace(12)
        } else if replyWidth.constant == 0 {
            setBottomSpace(24)
        }
        
        closeReplies()
    }
    
    @IBAction func reportPressed(_ sender: Any) {
        if vc == nil || comment == nil { return }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        
        let reportAction: UIAlertAction = UIAlertAction(title: "Report", style: .default) { action -> Void in
            
            let params = ["PostID": self.postID, "CommentID": self.comment!.id, "UserID": user.id]
            
            Alamofire.request(Config.shared.reportCommentURL, method: .post, parameters: params)
            Util.defaultAlert(self.vc!, title: "Comment Reported Successfully")
        }
        
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(reportAction)
        vc!.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func showMorePressed(_ sender: Any) {
        if showMore.isHidden {
            return
        }
        
        if showMore.text == "Show More" {
            textHeight.constant = cmtHeight
            showMore.text = "Show Less"
        } else {
            textHeight.constant = 60
            showMore.text = "Show More"
        }
        
        heightUpdated?()
    }
    
    @IBAction func replyPressed(_ sender: Any) {
        Util.fadeIn(views: [replyLayout], duration: 0.3)

    }
    
    @IBAction func replyBackPressed(_ sender: Any) {
        Util.fadeOut(views: [replyLayout], duration: 0.3)
    }
    
    var replyTableHeight: CGFloat = -1
    
    @IBAction func viewRepliesPressed(_ sender: Any) {
        if (viewReplies.isHidden) { return }
        
        Util.fadeOut(views: [viewReplies], duration: 0.3)
        
        if let RLH = repliesLayoutHeight {
            repliesLayout.removeConstraint(RLH)
            
            self.heightUpdated?()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.heightUpdated?()
            }
        }
    }
    
    @IBAction func hideRepliesPressed(_ sender: Any) {
        Util.fadeIn(views: [viewReplies], duration: 0.3)
        closeReplies()
    }
    
    func closeReplies() {
        repliesLayoutHeight = NSLayoutConstraint(item: repliesLayout, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        repliesLayout.addConstraint(repliesLayoutHeight!)
        heightUpdated?()
    }
    
    func hideShowMore(hide: Bool) {
        showMoreWidth.constant = hide ? 0 : showMoreW
        showMoreLeading.constant = hide ? 0 : 16
        
        self.layoutIfNeeded()
    }
    
    func hideReply(hide: Bool) {
        replyWidth.constant = hide ? 0 : replyW
        replyLeading.constant = hide ? 0 : 16
        
        self.layoutIfNeeded()
    }
    
    func setBottomSpace(_ constant: CGFloat) {
        replyLayoutHeight.constant = constant
        
        heightUpdated?()
    }
}

extension CommentCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comment!.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        let cmt = comment!.comments[comment!.comments.count - indexPath.row - 1]
        
        guard let hUpdate = heightUpdated else { return cell }
        
        cell.setup(cmt, partyID: partyID, vc: vc!, heightUpdated: hUpdate, last: indexPath.row == comment!.comments.count - 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.heightUpdated?()
    }
    
    func messageTextChanged() {
        sendBtn.tintColor = messageField!.text!.isEmpty ? UIColor(hex: "DDDDDD"): Colors.viibeBlue
    }
    
    func chatSendPressed() {
        Util.checkUsername(ViewController.instance!) {
            self.send()
        }
    }
    
    func send() {
        guard let u = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        if messageField == nil || self.comment == nil || messageField!.text!.isEmpty {
            return
        }
        
        var params = [
            "UserID": u.id,
            "Text": messageField!.text!,
            "Parents": "[\"\(comment!.id)\"]",
            "ServerVersion": "2"
        ]
        
        if partyID.isEmpty {
            params["PostID"] = postID
        } else {
            params["PartyID"] = partyID
        }
        
        Alamofire.request(Config.shared.sendCommentURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let commentJSON = json["Comment"] as? [String: Any] {
                
                let c = Comment()
                c.setup(commentJSON)
                
                let realm = try! Realm()
                
                if !self.partyID.isEmpty {
                    if let realmParty = realm.objects(Party.self).first(where: { (realmParty) -> Bool in
                        return realmParty.id == self.partyID
                    }) {
                        try! realm.write {
                            for cmt in realmParty.comments {
                                if cmt.id == self.comment!.id {
                                    cmt.comments.append(c)
                                    break
                                }
                            }
                        }
                    }
                } else {
                    if let realmPost = realm.objects(Post.self).first(where: { (realmPost) -> Bool in
                        return realmPost.id == self.postID
                    }) {
                        try! realm.write {
                            for cmt in realmPost.comments {
                                if cmt.id == self.comment!.id {
                                    cmt.comments.append(c)
                                    break
                                }
                            }
                        }
                    }
                }
                
                self.messageField!.text = ""
                self.endEditing(true)
                self.comment!.comments.append(vComment(c))
                self.repliesTable!.beginUpdates()
                self.repliesTable!.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.repliesTable!.endUpdates()
                
                self.sendBtn!.tintColor = UIColor(hex: "DDDDDD")
                self.viewReplies.text = "View \(self.comment!.numReplies() == 1 ? "Reply" : "\(self.comment!.numReplies()) Replies")"
                
                self.replyBackPressed(self)
                self.viewRepliesPressed(self)
            } else {
                Util.defaultAlert(self.vc!, title: "Failed to Send Comment")
            }
        }
    }
}
