//
//  FeedController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/1/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import MaterialComponents
import RealmSwift
import CoreLocation
import Floaty

class FeedController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noPostsLabel: UILabel!
    
    @IBOutlet weak var addFAB: Floaty!
    
    var posts: [vPost] = []
    var filtered: [vPost] = []
    
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.sendSubview(toBack: newPostButton)
        view.sendSubview(toBack: newEventButton)
        
        addFAB.addDropShadow(opacity: 0.3, offset: CGSize(width: 5, height: 5))
        addFAB.tintColor = Colors.viibeBlue
        
        let newPostItem = FloatyItem()
        newPostItem.hasShadow = false
        newPostItem.buttonColor = UIColor(hex: "F2F2F2")
        newPostItem.icon = #imageLiteral(resourceName: "pencil2").withRenderingMode(.alwaysTemplate)
        newPostItem.iconTintColor = Colors.viibeBlue
        newPostItem.title = "New Post"
        newPostItem.handler = { (_) in
            self.newPost()
        }
        
        addFAB.addItem(item: newPostItem)
        
        let newEventItem = FloatyItem()
        newEventItem.hasShadow = false
        newEventItem.buttonColor = UIColor(hex: "F2F2F2")
        newEventItem.icon = #imageLiteral(resourceName: "champagne").withRenderingMode(.alwaysTemplate)
        newEventItem.iconTintColor = Colors.viibeBlue
        newEventItem.title = "New Event"
        newEventItem.handler = { (_) in
            self.newEvent()
        }
        
        addFAB.addItem(item: newEventItem)
        
        table.register(UINib(nibName: "FeedPostCell", bundle: nil), forCellReuseIdentifier: "FeedPostCell")
        table.register(UINib(nibName: "FeedEventCell", bundle: nil), forCellReuseIdentifier: "FeedEventCell")
        
        table.dataSource = self
        table.delegate = self
        table.allowsSelection = false
        table.separatorStyle = .singleLine
        table.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        table.separatorColor = UIColor(hex: "CCCCCC")
        
        table.tableHeaderView = Util.footerView(height: 20)
        table.tableFooterView = Util.footerView(height: 24)
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 200
                
        FeedLoader.loadPosts() {
            self.loadPosts()
        }

        loadPosts()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundPressed)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedController.reload), name: Notifications.PostsUpdated, object: nil)
    }
    
    func loadPosts() {
        let realm = try! Realm()
        let realmPosts = realm.objects(Post.self)
        
        posts = []
        for post in realmPosts {
            posts.append(vPost(post))
        }
        
        posts.sort()
        
        filterAndUpdate(searchText: searchText, scrollToTop: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = filtered[indexPath.row]
        
        if post.type == "EVENT" {
            let cell = table.dequeueReusableCell(withIdentifier: "FeedEventCell", for: indexPath) as! FeedEventCell
            
            cell.setup(post, vc: self)
            cell.reloadUI = { self.loadPosts() }
            
            return cell
        }

        let cell = table.dequeueReusableCell(withIdentifier: "FeedPostCell", for: indexPath) as! FeedPostCell
        
        cell.setup(post, vc: self)
        cell.reloadUI = { self.loadPosts() }
        
        return cell
    }
    
    func filterAndUpdate(searchText: String, scrollToTop: Bool = true) {
        self.searchText = searchText
        let prefilter = filtered

        filtered = posts.filter({ (post) -> Bool in
            return searchText.isEmpty || post.contains(str: searchText)
        })

        if !searchText.isEmpty {
            filtered.sort { (a, b) -> Bool in
                if a.startsWith(str: searchText) && !b.startsWith(str: searchText) {
                    return true
                } else if b.startsWith(str: searchText) && !a.startsWith(str: searchText) {
                    return false
                }

                return a < b
            }
        }

        noPostsLabel.isHidden = !filtered.isEmpty
        noPostsLabel.text = posts.isEmpty ? "NO POSTS YET" : "NO RESULTS"

        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]

        for i in 0..<prefilter.count {
            let a = prefilter[i]

            if !filtered.contains(a) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }

        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]

            if !prefilter.contains(a) {
                inserted.append(IndexPath(row: index, section: 0))
            }

            index += 1
        }

        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }

            let a = prefilter[i]

            let filteredIndex = filtered.index(of: a)

            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }

        self.table.beginUpdates()
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)

        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }

        self.table.endUpdates()

        if scrollToTop && !filtered.isEmpty {
            self.table.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    
    func newPost() {
        Util.checkUsername(ViewController.instance!) {
            let newPostVC = NewPostController(nibName: "NewPostController", bundle: nil)
            ViewController.instance?.navigationController?.pushViewController(newPostVC, animated: true)
        }
    }
    
    func newEvent() {
        Util.checkUsername(ViewController.instance!) {
            let newEventVC = NewEventController(nibName: "NewEventController", bundle: nil)
            ViewController.instance?.navigationController?.pushViewController(newEventVC, animated: true)
        }
    }
    
    func backgroundPressed() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!addFAB.closed) { addFAB.close() }
        self.reload()
    }
    
    private var lastContentOffset: CGFloat = 0
    private var buttonsHidden = false

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            return
        }
        
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            if buttonsHidden {
//                Util.fadeIn(views: [newPostFAB, newEventFAB], duration: 0.3)
                buttonsHidden = false
            }
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y) {
            if !buttonsHidden {
//                Util.fadeOut(views: [newPostFAB, newEventFAB], duration: 0.3)
                buttonsHidden = true
            }
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        if buttonsHidden {
//            Util.fadeIn(views: [newPostFAB, newEventFAB], duration: 0.3)
            buttonsHidden = false
        }
    }
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBAction func addButtonPressed(_ sender: Any) {
        if addFAB.closed {
            addFAB.open()
            
            view.bringSubview(toFront: newPostButton)
            view.bringSubview(toFront: newEventButton)
        } else {
            addFAB.close()
            
            view.sendSubview(toBack: newPostButton)
            view.sendSubview(toBack: newEventButton)
        }
        
        view.bringSubview(toFront: addButton)
    }
    
    @IBOutlet weak var newPostButton: UIButton!
    @IBOutlet weak var newEventButton: UIButton!
    
    @IBAction func newPostPressed(_ sender: Any) {
        newPost()
    }
    
    @IBAction func newEventPressed(_ sender: Any) {
        newEvent()
    }
    
    func reload() {
        loadPosts()
        
        let contentOffset = table.contentOffset
        table.reloadData()
        table.layoutIfNeeded()
        table.setContentOffset(contentOffset, animated: false)
    }
}
