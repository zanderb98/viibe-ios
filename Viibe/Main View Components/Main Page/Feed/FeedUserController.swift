//
//  FeedUserController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/27/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift

class FeedUserController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var topCard: UIView!
    @IBOutlet weak var userCard: UIView!
    @IBOutlet weak var statsCard: UIView!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var numPosts: UILabel!
    
    var username = ""
    var userID = ""
    
    var posts: [vPost] = []
    var filtered: [vPost] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.register(UINib(nibName: "FeedPostCell", bundle: nil), forCellReuseIdentifier: "FeedPostCell")
        table.register(UINib(nibName: "FeedEventCell", bundle: nil), forCellReuseIdentifier: "FeedEventCell")
        
        table.dataSource = self
        table.allowsSelection = false
        table.separatorStyle = .singleLine
        table.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        table.separatorColor = UIColor(hex: "CCCCCC")
        
        let headerView = Util.footerView(height: 50)
        let headerLabel = UILabel(frame: CGRect(x: 16, y: 20, width: 200, height: 30))
        headerLabel.attributedText = NSMutableAttributedString().normal("Posts", font: Fonts.museo700(size: 24), color: Colors.viibeBlue)
        
        headerView.addSubview(headerLabel)
        
        table.tableHeaderView = headerView
        table.tableFooterView = Util.footerView(height: 20)
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 200
        
        FeedLoader.loadUserPosts(id: userID) {
            (postsIn, followingIn, followersIn) in
            self.loadPosts()
            
            self.followers.attributedText = NSMutableAttributedString().normal("Followers: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(followersIn)", font: Fonts.museo500(size: 14), color: .black)
            
            self.following.attributedText = NSMutableAttributedString().normal("Following: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(followingIn)", font: Fonts.museo500(size: 14), color: .black)
            
            self.numPosts.attributedText = NSMutableAttributedString().normal("Posts: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(postsIn.count)", font: Fonts.museo500(size: 14), color: .black)
        }
        
        topCard.addDropShadow()
        statsCard.addDropShadow(offset: CGSize(width: 0, height: 5))

    }
    
    func loadPosts() {
        let realm = try! Realm()
        let realmPosts = realm.objects(Post.self).filter { (p) -> Bool in
            return p.userID == self.userID
        }
        
        posts = []
        for post in realmPosts {
            posts.append(vPost(post))
        }
        
        posts.sort()
        filtered = posts
        table.reloadData()
//        filterAndUpdate(scrollToTop: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = filtered[indexPath.row]
        
        if post.type == "EVENT" {
            let cell = table.dequeueReusableCell(withIdentifier: "FeedEventCell", for: indexPath) as! FeedEventCell
            cell.setup(post, vc: self, fromUserVC: true)
            return cell
        }
        
        let cell = table.dequeueReusableCell(withIdentifier: "FeedPostCell", for: indexPath) as! FeedPostCell
        cell.setup(post, vc: self, fromUserVC: true)
        return cell
    }

    
//    @objc func textFieldDidChange(textField: UITextField) {
//        filterAndUpdate()
//    }
    
//    func filterAndUpdate(scrollToTop: Bool = true) {
//        let prefilter = filtered
//
//        filtered = posts.filter({ (post) -> Bool in
//            return searchField.text!.isEmpty || post.contains(str: searchField.text!)
//        })
//
//        if !searchField.text!.isEmpty {
//            filtered.sort { (a, b) -> Bool in
//                if a.startsWith(str: searchField.text!) && !b.startsWith(str: searchField.text!) {
//                    return true
//                } else if b.startsWith(str: searchField.text!) && !a.startsWith(str: searchField.text!) {
//                    return false
//                }
//
//                return a < b
//            }
//        }
//
//        noPostsLabel.isHidden = !filtered.isEmpty
//        noPostsLabel.text = posts.isEmpty ? "NO POSTS YET" : "NO RESULTS"
//
//        var deleted: [IndexPath] = []
//        var inserted: [IndexPath] = []
//        var moved: [IndexPath: IndexPath] = [:]
//
//        for i in 0..<prefilter.count {
//            let a = prefilter[i]
//
//            if !filtered.contains(a) {
//                deleted.append(IndexPath(row: i, section: 0))
//            }
//        }
//
//        var index = 0
//        for i in 0..<filtered.count {
//            let a = filtered[i]
//
//            if !prefilter.contains(a) {
//                inserted.append(IndexPath(row: index, section: 0))
//            }
//
//            index += 1
//        }
//
//        for i in 0..<prefilter.count {
//            let index = IndexPath(row: i, section: 0)
//            if inserted.contains(index) || deleted.contains(index) {
//                continue
//            }
//
//            let a = prefilter[i]
//
//            let filteredIndex = filtered.index(of: a)
//
//            moved[index] = IndexPath(row: filteredIndex!, section: 0)
//        }
//
//        self.table.beginUpdates()
//        self.table.deleteRows(at: deleted, with: .automatic)
//        self.table.insertRows(at: inserted, with: .automatic)
//
//        for (at, to) in moved {
//            self.table.moveRow(at: at, to: to)
//        }
//
//        self.table.endUpdates()
//
//        if scrollToTop && !filtered.isEmpty {
//            self.table.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
//        }
//    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
