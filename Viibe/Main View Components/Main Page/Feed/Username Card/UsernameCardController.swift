import UIKit
import Pulsator
import MBCircularProgressBar
import RealmSwift
import MaterialComponents
import Alamofire

class UsernameCardController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var usernameText: MDCTextField!
    @IBOutlet weak var submitCard: UIView!
    @IBOutlet weak var background: UIView!
    
    var alert: UsernameCard?
    
    var onTicketScreen = false
    var cardTop: NSLayoutConstraint?
    
    var inVC: UIViewController?
    var success: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.shadowOpacity = 0.6
        background.layer.shadowOffset = CGSize(width: 0, height: -1)
        background.layer.shadowRadius = 3
        
        submitCard.setCornerRadius(radius: 5)
        submitCard.addDropShadow()
        
        usernameText.autocorrectionType = .no
        usernameText.clearButtonMode = .never
        usernameText.autocapitalizationType = .none
        usernameText.delegate = self
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(UsernameCardController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UsernameCardController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: String = currentString.replacingCharacters(in: range, with: string)
        
        return newString.isAlphanumeric || newString.isEmpty
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func show(vc: UIViewController) {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.present(self, animated: true, completion: nil)
        
        self.inVC = vc
    }
    
    func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        guard let username = usernameText.text else { return }
        
        view.endEditing(true)
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        let params = ["UserID": user.id,
                      "Username": username,
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.setUsernameURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            
            if response.response?.statusCode != 200 {
                Util.defaultAlert(self, title: "Oops...", message: "The username you chose is already taken.")
                return
            }
            
            guard let userManaged = Util.getCurrentUser(realm) else {
                Util.defaultAlert(self, title: "Oops...", message: "An error occurred. Log out and log back in.")
                return
            }

            try! realm.write {
                userManaged.username = username
            }
            
            Util.defaultAlert(self, title: "Success!", message: "You set '\(username)' as you username.") {
                self.success?()
                self.hide()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let views: [UIView] = [background, partyName, usernameText, submitCard]
        
        var found = false
        for touch in touches {
            if (views.contains(touch.view!)) {
                found = true
                break
            }
        }
        
        if (!onTicketScreen && !found) { self.alert?.hide() }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
