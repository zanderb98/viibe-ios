//
//  FedPostCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/2/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class FeedPostCell: UITableViewCell {

    @IBOutlet weak var card: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var postText: UILabel!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var upvote: UIImageView!
    @IBOutlet weak var downvote: UIImageView!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var like: UIImageView!
    @IBOutlet weak var dislike: UIImageView!
    @IBOutlet weak var reply: UILabel!
    @IBOutlet weak var postTextWidth: NSLayoutConstraint!
    @IBOutlet weak var viewRepliesHeight: NSLayoutConstraint!
    @IBOutlet weak var viewRepliesBottom: NSLayoutConstraint!
    @IBOutlet weak var usernameWidth: NSLayoutConstraint!
    @IBOutlet weak var usernameTrailing: NSLayoutConstraint!
    
    let neutral = UIColor(hex: "#AAAAAA")
    
    var post: vPost?
    var vc: UIViewController?
    
    let fromCommentsVC: Bool = false

    var reloadUI: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    
        like.image = #imageLiteral(resourceName: "upvote").withRenderingMode(.alwaysTemplate)
        dislike.image = #imageLiteral(resourceName: "downvote").withRenderingMode(.alwaysTemplate)
        
        postTextWidth.constant = likes.frame.minX - 16
        self.layoutIfNeeded()
    }

    func setup(_ post: vPost, vc: UIViewController, fromCommentsVC: Bool = false, fromUserVC: Bool = false) {
        self.post = post
        self.vc = vc
        
        username.text = fromUserVC ? "" : "@\(post.username)"
        postText.text = post.text
        likes.text = "\(post.likes)"
        
        usernameWidth.constant = username.text!.width(font: username.font)
        usernameTrailing.constant = fromUserVC ? 0 : 12
        
        timestamp.text = "\(Util.messageTimestamp(date: post.date)) ago"
        
        hideViewReplies(hide: fromCommentsVC || post.numComments() == 0)
        comments.text = "View \(post.numComments()) \(post.numComments() == 1 ? "Reply" : "Replies")"
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        like.tintColor = user.likes.contains(post.id) ? Colors.viibeBlue : neutral
        dislike.tintColor = user.dislikes.contains(post.id) ? Colors.viibeBlue : neutral
        
        self.layoutIfNeeded()
    }
    
    @IBAction func replyPressed(_ sender: Any) {
        if fromCommentsVC { return }
        
        let commentsVC = FeedCommentsController(nibName: "FeedCommentsController", bundle: nil)
        commentsVC.post = post
        commentsVC.showKeyboard = true
        ViewController.instance?.navigationController?.pushViewController(commentsVC, animated: true)
    }
    
    @IBAction func commentsPressed(_ sender: Any) {
        let commentsVC = FeedCommentsController(nibName: "FeedCommentsController", bundle: nil)
        commentsVC.post = post
        ViewController.instance?.navigationController?.pushViewController(commentsVC, animated: true)
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(vc!, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
        }
        
        guard let user = Util.getCurrentUser() else {
            return
        }
        
        let realm = try! Realm()
        let posts = realm.objects(Post.self).filter("id='\(post!.id)'")
        
        var params: [String: String] = [:]
        params["UserID"] = user.id
        params["PostID"] = post!.id
        
        try! realm.write {
            if contains(str: post!.id, list: user.likes) {
                for i in (0...user.likes.count - 1).reversed() {
                    if (user.likes[i].str == post!.id) {
                        user.likes.remove(objectAtIndex: i);
                    }
                }
                
                like.tintColor = neutral
                dislike.tintColor = neutral
                
                params["Action"] = "unlike"
                updatePosts(posts: posts, amt: -1);
            } else {
                if contains(str: post!.id, list: user.dislikes) {
                    for i in (0...user.dislikes.count - 1).reversed() {
                        if (user.dislikes[i].str == post!.id) {
                            user.dislikes.remove(objectAtIndex: i);
                        }
                    }
                    
                    updatePosts(posts: posts, amt: 1);
                    
                    params["Action"] = "switchtolike"
                } else {
                    params["Action"] = "like"
                }
                
                updatePosts(posts: posts, amt: 1);
                
                let str = StringObj()
                str.str = post!.id
                
                user.likes.append(str);
                like.tintColor = Colors.viibeBlue
                dislike.tintColor = neutral
            }
        }
        
        likes.text = "\(posts[0].likes)"
        post?.likes = posts[0].likes

        Alamofire.request(Config.shared.feedLikeURL, method: .post, parameters: params)
    }
    
    @IBAction func dislikeButtonPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(vc!, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
        }
        
        guard let user = Util.getCurrentUser() else {
            return
        }
        
        let realm = try! Realm()
        let posts = realm.objects(Post.self).filter("id='\(post!.id)'")
        
        var params: [String: String] = [:]
        params["UserID"] = user.id
        params["PostID"] = post!.id
        
        try! realm.write {
            if contains(str: post!.id, list: user.dislikes) {
                for i in (0...user.dislikes.count - 1).reversed() {
                    if (user.dislikes[i].str == post!.id) {
                        user.dislikes.remove(objectAtIndex: i);
                    }
                }
                
                like.tintColor = neutral
                dislike.tintColor = neutral
                
                params["Action"] = "undislike"
                updatePosts(posts: posts, amt: 1);
            } else {
                if  contains(str: post!.id, list: user.likes) {
                    for i in (0...user.likes.count - 1).reversed() {
                        if (user.likes[i].str == post!.id) {
                            user.likes.remove(objectAtIndex: i);
                        }
                    }
                    
                    updatePosts(posts: posts, amt: -1);
                    
                    params["Action"] = "switchtodislike"
                } else {
                    params["Action"] = "dislike"
                }
                
                updatePosts(posts: posts, amt: -1);
                
                let str = StringObj()
                str.str = post!.id
                
                user.dislikes.append(str)
                like.tintColor = neutral
                dislike.tintColor = Colors.viibeBlue
            }
        }
        
        likes.text = "\(posts[0].likes)"
        post?.likes = posts[0].likes
        
        Alamofire.request(Config.shared.feedLikeURL, method: .post, parameters: params)
    }
    
    @IBAction func usernamePressed(_ sender: Any) {
        let vc = FeedUserController(nibName: "FeedUserController", bundle: nil)
        
        vc.username = post!.username
        vc.userID = post!.userID
        
        ViewController.instance?.navigationController?.pushViewController(vc, animated: true)
    }
    
    internal func contains(str: String, list: List<StringObj>) -> Bool {
        for obj in list {
            if str == obj.str {
                return true
            }
        }
        
        return false
    }
    
    internal func updatePosts(posts: Results<Post>, amt: Int) {
        for post in posts {
            post.likes += amt
        }
    }
    
    func hideViewReplies(hide: Bool) {
        viewRepliesHeight.constant = hide ? 0 : 30
        viewRepliesBottom.constant = hide ? 4 : 12
        
        self.layoutIfNeeded()
    }
    
    @IBAction func morePressed(_ sender: Any) {
        guard let user = Util.getCurrentUserUnmanaged(), let post = self.post, let vc = self.vc else {
            return
        }
        
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        
        let reportAction: UIAlertAction = UIAlertAction(title: "Report Post", style: .default) { action -> Void in
            
            let params = ["PostID": post.id, "UserID": user.id]
            
            Alamofire.request(Config.shared.reportPostURL, method: .post, parameters: params).response { (response) in
                
                if (response.response?.statusCode != 200) {
                     Util.defaultAlert(self.vc!, title: "Failed to Report Post", message: "Please restart the app and try again.")
                    return
                }
                
                Util.defaultAlert(self.vc!, title: "Post Reported Successfully")
                FeedLoader.loadPosts(complete: {
                    self.reloadUI?()
                })
            }
        }
        
        let blockAction: UIAlertAction = UIAlertAction(title: "Block @\(post.username)", style: .default) { action -> Void in
            
            let params = ["PostID": post.id, "UserID": user.id]
            
            Alamofire.request(Config.shared.blockUserURL, method: .post, parameters: params).response { (response) in
                
                if (response.response?.statusCode != 200) {
                    Util.defaultAlert(self.vc!, title: "Failed to Block @\(post.username)", message: "Please restart the app and try again.")
                    return
                }
                
                Util.defaultAlert(self.vc!, title: "@\(post.username) Blocked Successfully")
                FeedLoader.loadPosts(complete: {
                    self.reloadUI?()
                })
            }
        }
        
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(reportAction)
        actionSheet.addAction(blockAction)
        vc.present(actionSheet, animated: true, completion: nil)
    }
}
