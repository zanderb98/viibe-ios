//
//  NewPostController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/6/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
import RealmSwift
import CoreLocation
import ActionSheetPicker_3_0

class NewEventController: UIViewController {

    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var titleField: MDCTextField!
    @IBOutlet weak var captionField: MDCTextField!
    @IBOutlet weak var dateCard: UIView!
    @IBOutlet weak var dateImg: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeCard: UIView!
    @IBOutlet weak var timeImg: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationCard: UIView!
    @IBOutlet weak var locImg: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var fab: MDCFloatingButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var fabBottom: NSLayoutConstraint!
    
    var loc: CLLocationCoordinate2D?
    var address: String?
    var eventDate: Date = Date()
    
    var user: vUser?
    var dateSet = false
    var timeSet = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topBar.layer.shadowColor = UIColor.black.cgColor
        topBar.layer.shadowOpacity = 0.25
        topBar.layer.shadowRadius = 3
        topBar.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        titleField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        captionField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        fab.setImage(#imageLiteral(resourceName: "checkmark").withRenderingMode(.alwaysTemplate), for: .normal)
        fab.tintColor = .white
        
        dateCard.setCornerRadius(radius: 10)
        dateCard.addDropShadow()
        
        dateImg.image = #imageLiteral(resourceName: "calendar").withRenderingMode(.alwaysTemplate)
        dateImg.tintColor = Colors.viibeBlue
        dateLabel.textColor = dateImg.tintColor
        
        timeCard.setCornerRadius(radius: 10)
        timeCard.addDropShadow()
        
        timeImg.image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysTemplate)
        timeImg.tintColor = Colors.viibeBlue
        timeLabel.textColor = timeImg.tintColor
        
        locationCard.setCornerRadius(radius: 10)
        locationCard.addDropShadow()
        
        locImg.image = #imageLiteral(resourceName: "map-marker").withRenderingMode(.alwaysTemplate)
        locImg.tintColor = Colors.viibeBlue
        locationLabel.textColor = locImg.tintColor
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundPressed)))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.fab.alpha = self.fab.disabledAlpha
        }
        
        guard let u = Util.getCurrentUserUnmanaged() else {
            return
        }
        
        self.user = u
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(NewEventController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewEventController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func backgroundPressed() {
        self.view.endEditing(true)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        self.checkFinished()
    }

    
    @IBAction func submitPressed(_ sender: Any) {
        if fab.alpha == fab.disabledAlpha {
            return
        }
        
        guard var name = titleField.text else { return }
        name = name.trim()
        
        guard var caption = captionField.text else { return }
        caption = caption.trim()
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            return
        }

        let params = ["UserID": user.id,
                      "Title": name,
                      "Caption": caption,
                      "EventDate": "\(eventDate.timeIntervalSince1970)",
                      "Address": address!,
                      "Latitude": "\(loc!.latitude)",
                      "Longitude": "\(loc!.longitude)",
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.newEventURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            
            if response.response?.statusCode != 200 {
                Util.defaultAlert(ViewController.instance!, title: "Oops...", message: "Failed to upload event")
                return
            }
            
            guard let json = response.result.value as? [String: Any],
                let postJSON = json["Event"] as? [String: Any] else {
                    Util.defaultAlert(ViewController.instance!, title: "Oops...", message: "Failed to upload event")
                    return
            }
            
            let post = Post()
            post.setup(json: postJSON)
            
            try! realm.write {
                realm.add(post)
            }
            
            ViewController.instance?.feedVC?.reload()
            
            self.navigationController?.popViewController(animated: true)
            Util.defaultAlert(ViewController.instance!, title: "Success!", message: "Post uploaded successfully!")
        }
    }
    
    @IBAction func dateCardPressed(_ sender: Any) {
        self.view.endEditing(true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d"

        let last = UserDefaults.standard.string(forKey: "LastSetDate") ?? "--"
        let lastDate = dateSet ? eventDate : (formatter.date(from: last) ?? Date())

        let datePicker = ActionSheetDatePicker(title: "Pick Date", datePickerMode: UIDatePickerMode.date, selectedDate: lastDate, doneBlock: {
            picker, value, index in
            if let dt = value as? Date {
                var compsIn = Calendar.current.dateComponents([.month, .day, .year], from: dt)
                var comps = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute, .second], from: self.eventDate)

                comps.month = compsIn.month
                comps.day = compsIn.day
                comps.year = compsIn.year

                self.eventDate = Calendar.current.date(from: comps)!
                self.dateSet = true

                self.dateLabel.text = formatter.string(from: self.eventDate)

                UserDefaults.standard.set(self.dateLabel.text, forKey: "LastSetDate")
                self.checkFinished()
            }
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)

        datePicker?.minimumDate = Date()

        datePicker?.show()
    }
    
    @IBAction func timeCardPressed(_ sender: Any) {
        self.view.endEditing(true)

        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        
        let last = UserDefaults.standard.string(forKey: "LastSetTime") ?? "10:30 PM"
        let lastDate = timeSet ? eventDate : (formatter.date(from: last) ?? Date())
        
        let datePicker = ActionSheetDatePicker(title: "Pick Time", datePickerMode: UIDatePickerMode.time, selectedDate: lastDate, doneBlock: {
            picker, value, index in
            if let dt = value as? Date {
                var compsIn = Calendar.current.dateComponents([.hour, .minute], from: dt)
                var comps = Calendar.current.dateComponents([.month, .day, .year, .hour, .minute, .second], from: self.eventDate)
                
                comps.hour = compsIn.hour
                comps.minute = compsIn.minute
                
                self.eventDate = Calendar.current.date(from: comps)!
                self.timeSet = true
                
                self.timeLabel.text = formatter.string(from: self.eventDate)
                
                UserDefaults.standard.set(self.timeLabel.text, forKey: "LastSetTime")
                self.checkFinished()
            }
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        datePicker?.show()
    }
    
    @IBAction func locationCardPressed(_ sender: Any) {
        let controller = ViibeLocationPicker(nibName: "ViibeLocationPicker", bundle: nil)
        
        let lastLat = UserDefaults.standard.double(forKey: "LastSetLat")
        let lastLng = UserDefaults.standard.double(forKey: "LastSetLng")
        
        var lastLoc: CLLocationCoordinate2D? = nil
        if !(lastLat == 0 && lastLng == 0) {
            lastLoc = CLLocationCoordinate2DMake(CLLocationDegrees(lastLat), CLLocationDegrees(lastLng))
        }
        
        controller.setLocation = loc ?? lastLoc
        controller.setAddress = address
        controller.confirmed = { (loc: CLLocationCoordinate2D, address: String) in
            if address.isEmpty {
                return
            }
            
            self.loc = loc
            self.address = address
            self.locationLabel.text = address
            
            UserDefaults.standard.set(Double(loc.latitude), forKey: "LastSetLat")
            UserDefaults.standard.set(Double(loc.longitude), forKey: "LastSetLng")
            
            self.checkFinished()
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func checkFinished() {
        let finished = loc != nil && address != nil && dateSet && timeSet &&
            titleField.text != nil && !titleField.text!.isEmpty &&
            captionField.text != nil && !captionField.text!.isEmpty
        self.fab.alpha = finished ? 1 : fab.disabledAlpha

    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    var keyboardOpen = false
    var originalHeight: CGFloat = 0
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardOpen {
                fabBottom.constant = 16 + keyboardSize.height
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                
                keyboardOpen = true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if keyboardOpen {
           fabBottom.constant = 16
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            
            keyboardOpen = false
        }
    }
}
