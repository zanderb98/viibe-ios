//
//  ViibeLocationPicker.swift
//  Venu
//
//  Created by Zander Bobronnikov on 10/2/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import Mapbox
import GooglePlaces
import CoreLocation

class ViibeLocationPicker: UIViewController, MGLMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var topCard: UIView!
    @IBOutlet weak var backButton: UIImageView!
    @IBOutlet weak var confirmButton: UIView!
    @IBOutlet weak var confirmText: UILabel!
    @IBOutlet weak var addressCard: UIView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var addressLoading: UIActivityIndicatorView!
    
    var party: Party?
    
    var confirmed: ((CLLocationCoordinate2D, String) -> Void)?
    var setLocation: CLLocationCoordinate2D?
    
    var setAddress: String?
    
    var lastCenter: CLLocationCoordinate2D?
    
    var mapView: MGLMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressCard.setCornerRadius(radius: address.frame.height / 2)
        addressCard.addDropShadow()
        
        mapView = MGLMapView(frame: view.bounds)
        mapView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView!.styleURL = MGLStyle.lightStyleURL()
        mapView!.attributionButton.isHidden = true
        mapView!.compassView.isHidden = true
        
        if (setLocation == nil) {
             LocationManager.shared.requestLocationPermission { (status) in
                LocationManager.shared.getLocationUpdates { (loc) in
                    self.mapView!.setCenter(CLLocationCoordinate2D(latitude: loc.latitude,
                                                              longitude: loc.longitude),
                                       zoomLevel: 14, animated: false)
                }
            }
        } else {
            mapView!.setCenter(CLLocationCoordinate2D(latitude: setLocation!.latitude,
                                                      longitude: setLocation!.longitude),
                                                      zoomLevel: 14, animated: false)

        }
        
        mapView!.delegate = self
        view.addSubview(mapView!)
        view.sendSubview(toBack: mapView!)
        
        let markerBottom = view.getConstraintByID(id: "markerBottom")
        markerBottom?.constant = view.bounds.height / 2
        view.layoutIfNeeded()

        let backImage = #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate)
        backButton.image = backImage
        backButton.tintColor = .black
        
        topCard.setCornerRadius(radius: 5)
        topCard.addDropShadow()
        
        confirmButton.setCornerRadius(radius: confirmButton.frame.height / 2)
        confirmButton.addDropShadow()
        
        let panRec = UIPanGestureRecognizer(target: self, action: #selector(mapDragged(recognizer:)))
        mapView?.addGestureRecognizer(panRec)
        panRec.delegate = self
        
        if let address = setAddress {
            self.address.text = address
            addressLoading.stopAnimating()
            
            let addressW = addressCard.getConstraintByID(id: "addressWidth")!
            addressW.constant = self.address.text!.width(font: self.address.font) + 16
            self.addressLoading.stopAnimating()
            
            UIView.animate(withDuration: 0.45) {
                self.addressCard.layoutIfNeeded()
            }
        } else {
            self.findAddress()
        }
    }
    
    func findAddress() {
        addressLoading.startAnimating()
        address.text = ""
        
        let addressW = addressCard.getConstraintByID(id: "addressWidth")!
        
        LocationManager.geocodeLatLng(lat: mapView!.centerCoordinate.latitude, lng: mapView!.centerCoordinate.longitude) { (result) in
            
            self.address.text = "Error Loading Address"
            if let a = result {
                self.setAddress = a
                self.address.text = a
            }
            
            addressW.constant = self.address.text!.width(font: self.address.font) + 16
            self.addressLoading.stopAnimating()
            
            UIView.animate(withDuration: 0.45) {
                self.addressCard.layoutIfNeeded()
            }
        }
    }
    
    @objc func mapDragged(recognizer: UIPanGestureRecognizer) {
        if recognizer.state == .ended {
            self.findAddress()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmPressed(_ sender: Any) {
        if (mapView?.centerCoordinate == nil || confirmed == nil) { return }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.confirmButton.backgroundColor = UIColor(hex: "4CAF50")
            self.confirmText.textColor = .white
            self.confirmText.text = "CONFIRMED"
        }) { (_) in
            Timer.scheduledTimer(withTimeInterval: 0.65, repeats: false, block: { (_) in
                self.confirmed!(self.mapView!.centerCoordinate, self.setAddress ?? "")
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        openSearchBar()
    }
    
    func openSearchBar() {
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        
        let autocompleteController = GMSAutocompleteViewController()
        let northEast = CLLocationCoordinate2D(latitude: lat + 0.1, longitude: lng + 0.1)
        let southWest = CLLocationCoordinate2D(latitude: lat - 0.1, longitude: lng - 0.1)
        autocompleteController.autocompleteBounds = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        let filter = GMSAutocompleteFilter()
        
        filter.country = "US"
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension ViibeLocationPicker: GMSAutocompleteViewControllerDelegate {
    
    // Handle the host's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false) { (_) in
            self.mapView?.setCenter(place.coordinate, zoomLevel: 14, animated: true)
            
            let comps = place.formattedAddress!.components(separatedBy: ",")
            if comps.count == 1 {
                self.address.text = place.formattedAddress!
            } else if comps.count > 1 {
                self.address.text = comps[0] + "," + comps[1]
            } else {
                self.address.text = "Error Loading Address"
            }
            
            self.addressLoading.isHidden = true
            
            if comps.count >= 1 {
                self.setAddress = self.address.text!
            }
            
            let addressW = self.addressCard.getConstraintByID(id: "addressWidth")!
            addressW.constant = self.address.text!.width(font: self.address.font) + 16
            
            UIView.animate(withDuration: 0.45) {
                self.addressCard.layoutIfNeeded()
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    // host canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
