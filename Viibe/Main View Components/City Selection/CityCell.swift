//
//  CityCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 7/24/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(_ city: String) {
        cityLabel.text = city
    }
    
}
