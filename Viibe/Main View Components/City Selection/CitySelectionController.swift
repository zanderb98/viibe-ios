//
//  CitySelectionController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 7/24/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class CitySelectionController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var both: [(String, Double)] = []
    var filtered: [(String, Double)] = []
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noResultsLabel: UILabel!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var searchField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.addDropShadow()
        searchBar.setCornerRadius(radius: searchBar.frame.height / 2)
        
        searchField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        table.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: "CityCell")
        table.dataSource = self
        table.delegate = self
        
        table.allowsSelection = true
        table.allowsMultipleSelection = false
        
        table.tableHeaderView = Util.footerView(height: 175)
        table.tableFooterView = Util.footerView(height: 24)
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 160
        
        loadCities()
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        filterAndUpdate()
    }
    
    func filterAndUpdate() {
        let prefilter = filtered
        
        filtered = both.filter({ (a) -> Bool in
            return searchField.text!.isEmpty || a.0.uppercased().contains(searchField.text!.uppercased())
        })
        
        if !searchField.text!.isEmpty {
            filtered.sort { (a, b) -> Bool in
                if a.0.lowercased().starts(with: searchField.text!.lowercased()) && !b.0.lowercased().starts(with: searchField.text!.lowercased()) {
                    return true
                } else if b.0.lowercased().starts(with: searchField.text!.lowercased()) && !a.0.lowercased().starts(with: searchField.text!.lowercased()) {
                    return false
                }
                
                return a.1 < b.1
            }
        }
        
        noResultsLabel.isHidden = !filtered.isEmpty
        noResultsLabel.text = "NO RESULTS"
        
        var deleted: [IndexPath] = []
        var inserted: [IndexPath] = []
        var moved: [IndexPath: IndexPath] = [:]
        
        for i in 0..<prefilter.count {
            let a = prefilter[i]
            
            if !filtered.map({ (a) -> String in
                return a.0
            }).contains(a.0) {
                deleted.append(IndexPath(row: i, section: 0))
            }
        }
        
        var index = 0
        for i in 0..<filtered.count {
            let a = filtered[i]
            
            if !prefilter.map({ (a) -> String in
                return a.0
            }).contains(a.0) {
                inserted.append(IndexPath(row: index, section: 0))
            }
            
            index += 1
        }
        
        for i in 0..<prefilter.count {
            let index = IndexPath(row: i, section: 0)
            if inserted.contains(index) || deleted.contains(index) {
                continue
            }
            
            let a = prefilter[i]
            
            let filteredIndex = filtered.index { (b) -> Bool in
                return a.0 == b.0
            }
            
            moved[index] = IndexPath(row: filteredIndex!, section: 0)
        }
        
        self.table.beginUpdates()
        self.table.deleteRows(at: deleted, with: .automatic)
        self.table.insertRows(at: inserted, with: .automatic)
        
        for (at, to) in moved {
            self.table.moveRow(at: at, to: to)
        }
        
        self.table.endUpdates()
        
        if !filtered.isEmpty {
            self.table.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let city = filtered[indexPath.row]
        let cell = table.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityCell
        cell.selectionStyle = .none
        
        cell.setup(city.0)
        
        return cell
    }
    
    func loadCities() {
        both = Util.getCities(lat:  UserDefaults.standard.double(forKey: "LastKnownLat"), lng: UserDefaults.standard.double(forKey: "LastKnownLng"))
        filterAndUpdate()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(filtered[indexPath.row].0, forKey: "SelectedCity")
        
        ViewController.instance?.updatedCity()
        self.backPressed(tableView)
    }

    @IBAction func backPressed(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
        self.navigationController?.popViewController(animated: false)
    }

}
