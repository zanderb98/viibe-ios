//
//  LoginViewController.swift
//  Venu
//
//  Created by Zander Bobronnikov on 10/3/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import AVFoundation
import FacebookCore
import FacebookLogin
import GoogleSignIn
import Alamofire

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    static var googleSignInCallback: ((String, String, URL?) -> Void)?
    
    @IBOutlet weak var loginSubmit: UIView!
    @IBOutlet weak var signupCard: UIView!
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginCard: UIView!
    @IBOutlet weak var termsOfService: UILabel!
    @IBOutlet weak var forgotPassword: UILabel!
    @IBOutlet weak var spinnerView: UIView!

    @IBOutlet weak var cancelCard: UIView!
    @IBOutlet weak var cancelLabel: UILabel!
    
    @IBOutlet weak var cancelBottom: NSLayoutConstraint!
    @IBOutlet weak var cancelHeight: NSLayoutConstraint!
    @IBOutlet weak var facebookTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var facebookCard: UIView!
    @IBOutlet weak var googleCard: UIView!
    
    var avPlayer: AVPlayer?
    var paused = false
    
    var inLoginMode = false
    var inForgotPWMode = false

    override func viewDidLoad() {
        super.viewDidLoad()
//        setUpVideo()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        loginSubmit.setCornerRadius(radius: 5)
        loginSubmit.isHidden = true
        
        signupCard.setCornerRadius(radius: 5)
        loginCard.setCornerRadius(radius: 5)
        cancelCard.setCornerRadius(radius: 5)
        facebookCard.setCornerRadius(radius: 5)
        googleCard.setCornerRadius(radius: 5)
        
        loginCard.layer.borderWidth = 2.5
        loginCard.layer.borderColor = UIColor.white.cgColor
        
        loginSubmit.layer.borderWidth = 2.5
        loginSubmit.layer.borderColor = UIColor.white.cgColor
        
        cancelCard.layer.borderWidth = 2.5
        cancelCard.layer.borderColor = UIColor.white.cgColor
        cancelCard.clipsToBounds = true
        
        facebookCard.layer.borderWidth = 2.5
        facebookCard.layer.borderColor = UIColor.white.cgColor
        facebookCard.clipsToBounds = true
        
        googleCard.layer.borderWidth = 2.5
        googleCard.layer.borderColor = UIColor.white.cgColor
        googleCard.clipsToBounds = true
        
        emailField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                              attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                              attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.backgroundPressed))
        view.addGestureRecognizer(rec)
        
        termsOfService.attributedText = NSMutableAttributedString().normal("By Logging In, You Agree to\nOur ", fontSize: 14)
                                                                    .underline("Terms of Service", fontSize: 14)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func backgroundPressed() {
        emailField.endEditing(true)
        passwordField.endEditing(true)
    }
    
    @IBAction func emailLoginPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        if inForgotPWMode {
            requestPassword()
            return
        }
        
        guard let email = emailField.text, let pass = passwordField.text else {
            return
        }
        
        if !Util.isValidEmail(email) {
            Util.defaultAlert(self, title: "Enter Valid Email", message: "The email you entered is invalid. Please enter a valid email and try again")
            return
        }
        
        if pass.count < 6 {
            Util.defaultAlert(self, title: "Password Too Short", message: "Your password must be at least 6 characters long.")
            return
        }
        
        self.spinnerView.isHidden = false
        
        AccountLoader.loginLoadEmail(email: email, password: pass, complete: {
            (firstLogin) in
            
            Util.clearLoginStatus()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
            self.navigationController?.pushViewController(vc, animated: true)
            
            self.spinnerView.isHidden = true
        }, failed: {
            self.spinnerView.isHidden = true
            Util.defaultAlert(self, title: "Login Failed", message: "Please check to make sure your login information is correct and try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
        })
    }
    
    func requestPassword() {
        guard let email = passwordField.text else {
            return
        }
        
        if !Util.isValidEmail(email) {
            Util.defaultAlert(self, title: "Enter Valid Email", message: "The email you entered is invalid. Please enter a valid email and try again")
            return
        }
        
        self.backgroundPressed()
        self.spinnerView.isHidden = false
        
        let params = ["Email": email]
        
        Alamofire.request(Config.shared.requestPasswordURL, method: .post, parameters: params).responseJSON { (response) in
            
            self.spinnerView.isHidden = true
            if response.response!.statusCode == 200 {
                Util.defaultAlert(self, title: "Success!", message: "We have sent you an email with reset instructions.") {
                    self.loginBack(self)
                }
            } else {
                Util.defaultAlert(self, title: "Enter Valid Email", message: "The email you entered is not associated with a Viibe account. Please enter a valid email and try again")
            }
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        if forgotPassword.isHidden { return }
        
        Util.fadeOut(views: [forgotPassword, signupCard, googleCard, facebookCard], duration: 0.3)
        passwordField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                       attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        passwordField.isSecureTextEntry = false
        passwordField.text = ""
        
        inForgotPWMode = true
    }
    
    @IBAction func facebookCardPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success( _, _, let accessToken):
                    self.spinnerView.isHidden = false
                    AccountLoader.loginLoadFB(accessToken: accessToken, complete: {
                        (firstLogin) in
                        
                        Util.clearLoginStatus()
                        
                        if !firstLogin {
                            LocationManager.shared.requestLocationPermission()
                        }
                        
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        self.spinnerView.isHidden = true
                    }, failed: {
                        self.spinnerView.isHidden = true
                        Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
                    })
                }
        }
    }
    
    @IBAction func googleCardPressed(_ sender: Any) {
        Util.setGoogleSignInFromLogin(true)

        LoginViewController.googleSignInCallback = { idToken, accessToken, profilePicURL in
            if idToken.isEmpty {
                Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
                return
            }
            
            self.spinnerView.isHidden = false
            
            AccountLoader.loginLoadGoogle(idToken: idToken, accessToken: accessToken, profilePicURL: profilePicURL, complete: {
                (firstLogin) in
                
                Util.clearLoginStatus()
                
                if !firstLogin {
                    LocationManager.shared.requestLocationPermission()
                }
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                self.navigationController?.pushViewController(vc, animated: true)
                
                self.spinnerView.isHidden = true
            }, failed: {
                self.spinnerView.isHidden = true
                Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
            })
        }
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func loginCardPressed(_ sender: Any) {
        if inLoginMode || inForgotPWMode {
            return
        }
        
        facebookTrailing.constant = 47 + loginSubmit.frame.width
        cancelHeight.constant = loginCard.frame.height
        cancelBottom.constant = 32
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.loginCard.backgroundColor = .white
        }
        
        inLoginMode = true
        Util.fadeIn(views: [emailField, passwordField, loginSubmit, facebookCard, googleCard, forgotPassword], duration: 0.3)
        Util.fadeOut(views: [signupLabel, loginLabel,], duration: 0.3)
    }
    
    @IBAction func loginBack(_ sender: Any) {
        if !inLoginMode {
            return
        }
        
        backgroundPressed() // Close keyboard
        
        if inForgotPWMode {
            //In forgot password mode
            Util.fadeIn(views: [forgotPassword, signupCard, googleCard, facebookCard], duration: 0.3)
            passwordField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                                    attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
            passwordField.isSecureTextEntry = true
            passwordField.text = ""
            
            inForgotPWMode = false
            return
        }
        
        facebookTrailing.constant = 35
        cancelHeight.constant = 0
        cancelBottom.constant = 20
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.loginCard.backgroundColor = UIColor.clear
        }
        
        inLoginMode = false
        Util.fadeOut(views: [emailField, passwordField, loginSubmit, facebookCard, googleCard, forgotPassword], duration: 0.3)
        Util.fadeIn(views: [signupLabel, loginLabel], duration: 0.3)
    }
    
    @IBAction func signupPressed(_ sender: Any) {
        if inLoginMode {
            return
        }
        
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.navigationController!.view.layer.add(transition, forKey: nil)
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "SignupViewController")
        self.navigationController?.pushViewController(vc, animated: false)
    
    }
    
    @IBAction func skipPressed(_ sender: Any) {
        Util.defaultAlert(self, title: "Are you sure?", message: "By skipping login you will not have access to many features") {
            
            Util.setSkippedLogin()
            UserDefaults.standard.set(true, forKey: Constants.FirstListLoad)
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func termsPressed(_ sender: Any) {
        guard let url = URL(string: Config.shared.viibeTermsURL) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func setUpVideo() {
        let theURL = Bundle.main.url(forResource:"login-video", withExtension: "mp4")
        
        avPlayer = AVPlayer(url: theURL!)
        let avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        avPlayer!.volume = 0
        avPlayer!.actionAtItemEnd = .none
        
        avPlayerLayer.frame = view.layer.bounds
        view.backgroundColor = .clear
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer!.currentItem)
    }
    
    func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (avPlayer == nil) { return }
        avPlayer!.play()
        paused = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (avPlayer == nil) { return }
        avPlayer!.pause()
        paused = true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if inLoginMode && !inForgotPWMode {
            Util.fadeOut(views: [facebookCard, googleCard, forgotPassword], duration: 0.3)
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if inLoginMode && !inForgotPWMode {
            Util.fadeIn(views: [facebookCard, googleCard, forgotPassword], duration: 0.3)
        }
        
        self.view.frame.origin.y = 0
    }

  }
