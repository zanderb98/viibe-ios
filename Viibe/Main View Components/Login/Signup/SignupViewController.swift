//
//  SignupViewController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/15/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore

class SignupViewController: UIViewController, GIDSignInUIDelegate {

    static var googleSignInCallback: ((String, String, URL?) -> Void)?
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var emailCard: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var firstNameCard: UIView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameCard: UIView!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var maleCard: UIView!
    @IBOutlet weak var maleImg: UIImageView!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleCard: UIView!
    @IBOutlet weak var femaleImg: UIImageView!
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var passwordCard: UIView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatCard: UIView!
    @IBOutlet weak var repeatField: UITextField!
    @IBOutlet weak var submitCard: UIView!
    @IBOutlet weak var terms: UILabel!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var spinnerView: UIView!
    
    @IBOutlet weak var facebookCard: UIView!
    @IBOutlet weak var googleCard: UIView!
    
    var gender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        emailCard.setCornerRadius(radius: 5)
        firstNameCard.setCornerRadius(radius: 5)
        lastNameCard.setCornerRadius(radius: 5)
        passwordCard.setCornerRadius(radius: 5)
        repeatCard.setCornerRadius(radius: 5)
        submitCard.setCornerRadius(radius: 5)
        maleCard.setCornerRadius(radius: 5)
        femaleCard.setCornerRadius(radius: 5)
        
        maleCard.layer.borderWidth = 2.5
        maleCard.layer.borderColor = UIColor.white.cgColor
        
        femaleCard.layer.borderWidth = 2.5
        femaleCard.layer.borderColor = UIColor.white.cgColor
        
        submitCard.layer.borderWidth = 2.5
        submitCard.layer.borderColor = UIColor.white.cgColor
        
        maleImg.image = #imageLiteral(resourceName: "human-male").withRenderingMode(.alwaysTemplate)
        maleImg.tintColor = .white
        
        femaleImg.image = #imageLiteral(resourceName: "human-female").withRenderingMode(.alwaysTemplate)
        femaleImg.tintColor = .white
        
        facebookCard.setCornerRadius(radius: 5)
        googleCard.setCornerRadius(radius: 5)
        
        facebookCard.layer.borderWidth = 2.5
        facebookCard.layer.borderColor = UIColor.white.cgColor
        facebookCard.clipsToBounds = true
        
        googleCard.layer.borderWidth = 2.5
        googleCard.layer.borderColor = UIColor.white.cgColor
        googleCard.clipsToBounds = true
        
        emailField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        firstNameField.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        lastNameField.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        repeatField.attributedPlaceholder = NSAttributedString(string: "Repeat Password",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor(hex: "DF8233")])
        
        terms.attributedText = NSMutableAttributedString().normal("By Signing Up, You Agree to\nOur ", fontSize: 14)
            .underline("Terms of Service", fontSize: 14)
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.backgroundPressed))
        view.addGestureRecognizer(rec)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignupViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignupViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func facebookCardPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                self.spinnerView.isHidden = false
                AccountLoader.loginLoadFB(accessToken: accessToken, complete: {
                    (firstLogin) in
                    
                    Util.clearLoginStatus()
                    
                    if !firstLogin {
                        LocationManager.shared.requestLocationPermission()
                    }
                    
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    self.spinnerView.isHidden = true
                }, failed: {
                    self.spinnerView.isHidden = true
                    Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
                })
            }
        }
    }
    
    @IBAction func googleCardPressed(_ sender: Any) {
        Util.setGoogleSignInFromLogin(false)
        
        SignupViewController.googleSignInCallback = { idToken, accessToken, profilePicURL in
            if idToken.isEmpty {
                Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
                return
            }
            
            self.spinnerView.isHidden = false
            
            AccountLoader.loginLoadGoogle(idToken: idToken, accessToken: accessToken, profilePicURL: profilePicURL, complete: {
                (firstLogin) in
                
                Util.clearLoginStatus()
                
                if !firstLogin {
                    LocationManager.shared.requestLocationPermission()
                }
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                self.navigationController?.pushViewController(vc, animated: true)
                
                self.spinnerView.isHidden = true
            }, failed: {
                self.spinnerView.isHidden = true
                Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at help@feeltheviibe.com.")
            })
        }
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        guard var email = emailField.text,
            var firstName = firstNameField.text,
            var lastName = lastNameField.text,
            let pass = passwordField.text,
            let repeatPass = repeatField.text else {
            return
        }
        
        email = email.trim()
        firstName = firstName.trim()
        lastName = lastName.trim()
        
        if !Util.isValidEmail(email) {
            Util.defaultAlert(self, title: "Enter Valid Email", message: "The email you entered is invalid. Please enter a valid email and try again")
            return
        }
        
        if firstName.isEmpty {
            Util.defaultAlert(self, title: "Enter First Name", message: "Please enter your first name and try again")
            return
        }
        
        if !firstName.containsOnlyLetters() {
            Util.defaultAlert(self, title: "Enter Valid First Name", message: "Please enter your first name and try again")
            return
        }
        
        if lastName.isEmpty {
            Util.defaultAlert(self, title: "Enter Last Name", message: "Please enter your last name and try again")
            return
        }
        
        if !lastName.containsOnlyLetters() {
            Util.defaultAlert(self, title: "Enter Valid Lst Name", message: "Please enter your last name and try again")
            return
        }
        
        if gender.isEmpty {
            Util.defaultAlert(self, title: "Select Gender", message: "Please select your gender and try again")
            return
        }
        
        if pass.count < 6 {
            Util.defaultAlert(self, title: "Password Too Short", message: "Your password must be at least 6 characters long")
            return
        }
        
        if pass != repeatPass {
            Util.defaultAlert(self, title: "Passwords don't Match", message: "Your make sure your passwords match and try again")
            return
        }
        
        let name = "\(firstName) \(lastName)"
        
        spinnerView.isHidden = false
        UserLoader.signup(email: email, password: pass, name: name, gender: gender, failed: {
            self.spinnerView.isHidden = true
            
            Util.defaultAlert(self, title: "Email in Use", message: "Enter a different email and try again")
            return
        }) {
            self.spinnerView.isHidden = true
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func backgroundPressed() {
        view.endEditing(true)
    }
    
    @IBAction func malePressed(_ sender: Any) {
        gender = "male"
        
        maleCard.backgroundColor = .white
        maleImg.tintColor = .orange
        maleLabel.textColor = .orange
        
        femaleCard.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        femaleImg.tintColor = UIColor(hex: "FFFFFF")
        femaleLabel.textColor = UIColor(hex: "FFFFFF")
    }
    
    @IBAction func femalePressed(_ sender: Any) {
        gender = "female"
        
        femaleCard.backgroundColor = .white
        femaleImg.tintColor = .orange
        femaleLabel.textColor = .orange
        
        maleCard.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
        maleImg.tintColor = UIColor(hex: "FFFFFF")
        maleLabel.textColor = UIColor(hex: "FFFFFF")
    }
    
    @IBAction func termsPressed(_ sender: Any) {
        guard let url = URL(string: Config.shared.viibeTermsURL) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        self.navigationController!.view.layer.add(transition, forKey: nil)
        self.navigationController!.popViewController(animated: false)
    }
    
    var originalHeight: CGFloat = -1
    
    func keyboardWillShow(notification: NSNotification) {
        terms.isHidden = true
        termsButton.isHidden = true
        
        originalHeight = originalHeight == -1 ? self.view.frame.height : originalHeight
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.height == originalHeight {
                self.view.frame.size.height -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        terms.isHidden = false
        termsButton.isHidden = false
        self.view.frame.size.height = originalHeight
    }

}
