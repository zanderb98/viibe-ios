//
//  HostProfileController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/17/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import RealmSwift

class HostProfileController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var table: UITableView!
    var pastParties: [vParty] = []
    var liveParties: [vParty] = []
    var upcomingParties: [vParty] = []
    
    var pastExpanded = true
    var liveExpanded = true
    var upcomingExpanded = true
    
    var host: vHost?
    var header: HostHeaderView?
    
    var gender = true
    var updateListCell: (() -> Void)?
    
    var openParty: vParty?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topBar.addDropShadow()
        
        setupTable()
        setupParties()
        
        if let user = Util.getCurrentUserUnmanaged() {
            gender = user.gender
        }
    }
    
    func setupTable() {
        table.dataSource = self
        table.separatorStyle = .none
        table.register(UINib(nibName: "ListPartyCell", bundle: nil), forCellReuseIdentifier: "ListPartyCell")
        table.register(UINib(nibName: "ListHeaderCell", bundle: nil), forCellReuseIdentifier: "ListHeaderCell")

        let realm = try! Realm()
        host = vHost(realm.objects(Host.self).first(where: { (h) -> Bool in
            return h.id == host!.id
        })!)
                
        header = HostHeaderView.instanceFromNib()
        header!.setup(host!)
        
        table.tableHeaderView = header
        table.tableFooterView = Util.footerView(height: 0)
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 100
        table.allowsSelection = false
    }
    
    func setupParties() {
        let realm = try! Realm()
        let hosted = realm.objects(Party.self).filter { (p) -> Bool in
            return self.host!.hostedParties.contains(p.id)
        }
        
        var totalPastLit = 0
        
        for party in hosted {
            if Util.isPartyToday(party: party) {
                liveParties.append(vParty(party))
            } else if (Date().compare(party.date) == .orderedAscending) {
                upcomingParties.append(vParty(party))
            } else {
                pastParties.append(vParty(party))
                
                totalPastLit += party.lit
            }
        }
        
        if !pastParties.isEmpty {
            header?.setAvgLit(lit: Int(Double(totalPastLit) / Double(pastParties.count)))
        }
        
        pastParties.sort { (p1, p2) -> Bool in
            return p1.date.compare(p2.date) == .orderedDescending
        }
        
        liveParties.sort { (p1, p2) -> Bool in
            return p1.date.compare(p2.date) == .orderedAscending
        }
        
        upcomingParties.sort { (p1, p2) -> Bool in
            return p1.date.compare(p2.date) == .orderedAscending
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return liveParties.isEmpty ? 0 : (liveExpanded ? liveParties.count + 1 : 1)
        } else if section == 1 {
            return upcomingParties.isEmpty ? 0 :  (upcomingExpanded ? upcomingParties.count + 1 : 1)
        } else {
            return pastParties.isEmpty ? 0 : (pastExpanded ? pastParties.count + 1 : 1)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var party: vParty
        
        if indexPath.row == 0 {
            var expanded = false
            var parties = 0
            
            if indexPath.section == 0 {
                expanded = liveExpanded
                parties = liveParties.count
            } else if indexPath.section == 1 {
                expanded = upcomingExpanded
                parties = upcomingParties.count
            } else {
                expanded  = pastExpanded
                parties = pastParties.count
            }
            
            let cell = table.dequeueReusableCell(withIdentifier: "ListHeaderCell", for: indexPath) as! ListHeaderCell
            cell.setup(section: indexPath.section, parties: parties, expanded: expanded, tapped: {
                if indexPath.section == 0 {
                    self.liveExpanded = !self.liveExpanded
                    
                    self.table.beginUpdates()
        
                    for i in 1...self.liveParties.count {
                        if self.liveExpanded {
                            self.table.insertRows(at: [IndexPath(row: i, section: indexPath.section)], with: .bottom)
                        } else {
                            self.table.deleteRows(at: [IndexPath(row: i, section: indexPath.section)], with: .top)
                        }
                    }
                    
                    self.table.endUpdates()
                } else if indexPath.section == 1 {
                    self.upcomingExpanded = !self.upcomingExpanded
                    
                    self.table.beginUpdates()
                    
                    for i in 1...self.upcomingParties.count {
                        if self.upcomingExpanded {
                            self.table.insertRows(at: [IndexPath(row: i, section: indexPath.section)], with: .bottom)
                        } else {
                            self.table.deleteRows(at: [IndexPath(row: i, section: indexPath.section)], with: .top)
                        }
                    }
                    
                    self.table.endUpdates()
                } else {
                    self.pastExpanded = !self.pastExpanded
                    
                    self.table.beginUpdates()
                    
                    for i in 1...self.pastParties.count {
                        if self.pastExpanded {
                            self.table.insertRows(at: [IndexPath(row: i, section: indexPath.section)], with: .bottom)
                        } else {
                            self.table.deleteRows(at: [IndexPath(row: i, section: indexPath.section)], with: .top)
                        }
                    }
                    
                    self.table.endUpdates()
                }
            })
            
            return cell
        }
        
        if indexPath.section == 0 {
            party = liveParties[indexPath.row - 1]
        } else if indexPath.section == 1 {
             party = upcomingParties[indexPath.row - 1]
        } else {
             party = pastParties[indexPath.row - 1]
        }
        
        let cell = table.dequeueReusableCell(withIdentifier: "ListPartyCell", for: indexPath) as! ListPartyCell
        
        cell.setup(party, newVC: self, gender: gender)
        return cell
    }

    @IBAction func backPressed(_ sender: Any) {
        updateListCell?()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let p = openParty {
            let card = ViibeCard()
            card.show(self, newControllerVC: self, party: p)
        }
    }
}
