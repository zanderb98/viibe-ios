//
//  HostHeaderView.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/17/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import SDWebImage

class HostHeaderView: UIView {
    
    @IBOutlet weak var topCard: UIView!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicBig: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileDesc: UILabel!
    @IBOutlet weak var followCard: UIView!
    @IBOutlet weak var followLabel: UILabel!
    
    @IBOutlet weak var metricsCard: UIView!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var events: UILabel!
    @IBOutlet weak var avgLit: UILabel!
    
    var newImage: UIImage?
    var saveButton: UIView?
    var vc: UIViewController?
    var host: vHost?
    
    var initialFollowers = -1
    var initialFollowing = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width))
    }
    
    
    func setup(_ host: vHost) {
        self.host = host
        
        profileName.text = host.name
        
        profileDesc.text = host.desc.isEmpty ? "No Description": host.desc
        
        if !host.profilePictureURL.isEmpty {
            profilePic.sd_setImage(with: URL(string: host.profilePictureURL), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            profilePicBig.sd_setImage(with: URL(string: host.profilePictureURL), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        
        let height = profileDesc.text!.height(font: profileDesc.font!, maxWidth:  UIScreen.main.bounds.width - 96)
        profileDesc.getConstraintByID(id: "descHeight")?.constant = height + 6
 
        self.layoutIfNeeded()
        
        topCard.addDropShadow(opacity: 0.6)
        followCard.setCornerRadius(radius: 10)
        followCard.layer.borderWidth = 1.5
        
//        metricsCard.addDropShadow(opacity: 0.6)
        
        profilePic.setCornerRadius(radius: profilePic.frame.width / 2)
        profilePicView.addDropShadow()
        
        setFollowersText(host.followers)
        
        events.attributedText = NSMutableAttributedString().normal("Events: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(host.hostedParties.count)", font: Fonts.museo500(size: 14), color: .black)

        initialFollowers = host.followers
        if let user = Util.getCurrentUserUnmanaged() {
            initialFollowing = user.following.contains(host.id)
        }
        
        followCard.backgroundColor = !initialFollowing ? UIColor.clear : Colors.viibeOrange
        followCard.layer.borderColor = !initialFollowing ? UIColor.white.cgColor : Colors.viibeOrange.cgColor
        followLabel.text = !initialFollowing ? "FOLLOW" : "FOLLOWING"
        followCard.addDropShadow(opacity: !initialFollowing ? 0 : 0.3)
        
        followCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(followPressed)))
    }
    
    func setAvgLit(lit: Int) {
        avgLit.attributedText = NSMutableAttributedString().normal("Avg Lit: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(lit)%", font: Fonts.museo500(size: 14), color: .black)
    }
    
    @objc func followPressed() {
        guard let user = Util.getCurrentUserUnmanaged(), let h = host else { return }
        let newFollowing = !user.following.contains(h.id)
        let add = newFollowing ? 1 : -1
        
        UIView.animate(withDuration: 0.3) {
            self.followCard.backgroundColor = !newFollowing ? UIColor.clear : Colors.viibeOrange
            self.followCard.layer.borderColor = !newFollowing ? UIColor.white.cgColor : Colors.viibeOrange.cgColor
            self.followLabel.text = !newFollowing ? "FOLLOW" : "FOLLOWING"
            self.followCard.addDropShadow(opacity: !newFollowing ? 0 : 0.3)
        }
        
        let params = [
            "HostID": h.id,
            "UserID": user.id,
            "Follow": newFollowing ? "true" : "false",
            "ServerVersion": "2"
            ]
        
        Alamofire.request(Config.shared.followURL, method: .post, parameters: params).response { (response) in
            if response.response?.statusCode == 200 {
                do {
                    let realm = try Realm()
                    let rUser = Util.getCurrentUser(realm)!
                    let rHost = Util.getHost(self.host!.id)!
                    
                    try! realm.write {
                        rHost.followers += add
                        self.setFollowersText(rHost.followers)
                        
                        if newFollowing {
                            let obj = StringObj()
                            obj.str = h.id
                            rUser.following.append(obj)
                        } else {
                            for i in 0..<rUser.following.count {
                                if rUser.following[i].str == h.id {
                                    rUser.following.remove(objectAtIndex: i)
                                    break
                                }
                            }
                        }
                    }
                } catch {}
            } else {
                Util.defaultAlert(self.vc!, title: "Failed to follow \(h.name)")
                UIView.animate(withDuration: 0.3) {
                    self.setFollowersText(Int(self.followers.text!)! - add)
                    self.followCard.backgroundColor = newFollowing ? UIColor.clear : Colors.viibeOrange
                    self.followCard.layer.borderColor = newFollowing ? UIColor.white.cgColor : Colors.viibeOrange.cgColor
                    self.followLabel.text = newFollowing ? "FOLLOW" : "FOLLOWING"
                    self.followCard.addDropShadow(opacity: newFollowing ? 0 : 0.3)
                    
                }
            }
        }
    }
    
    func setFollowersText(_ num: Int) {
        self.followers.attributedText = NSMutableAttributedString().normal("Followers: ", font: Fonts.museo700(size: 14), color: UIColor(hex: "8593AA")).normal("\(num)", font: Fonts.museo500(size: 14), color: .black)
    }
    
    class func instanceFromNib() -> HostHeaderView {
        return UINib(nibName: "HostHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HostHeaderView
    }
}
