//
//  ListHeaderCell.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 7/25/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class ListHeaderCell: UITableViewCell {

    @IBOutlet weak var header: UILabel!
    var tapped: (() -> Void)?
    var expanded = true
    var parties = 0
    var section = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped)))

    }
    
    func setup(section: Int, parties: Int, expanded: Bool, tapped: @escaping (() -> Void)) {
        self.tapped = tapped
        self.parties = parties
        self.section = section
        self.expanded = expanded
        
        let beginning = expanded ? "-" : "+"
        
        if (section == 0) {
            header.text = "\(beginning) Live Events (\(parties))"
        } else if (section == 1) {
            header.text = "\(beginning) Upcoming Events (\(parties))"
        } else {
            header.text = "\(beginning) Past Events (\(parties))"
        }
    }
    
    @objc func cellTapped() {
        setup(section: section, parties: parties, expanded: !expanded, tapped: tapped ?? {})
        tapped?()
    }
}
