//
//  OnboardingPage3.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 4/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Lottie
import Stripe
import Alamofire
import MaterialComponents
import RealmSwift

class OnboardingPage3: UIViewController, STPAddCardViewControllerDelegate {

    @IBOutlet weak var iconCard: UIView!
    @IBOutlet weak var animView: LOTAnimationView!
    @IBOutlet weak var addCardButton: UIView!
    @IBOutlet weak var checkAnim: LOTAnimationView!
    @IBOutlet weak var text: UILabel!
    
    var inVC: OnboardingController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconCard.setCornerRadius(radius: iconCard.frame.height / 2)
        iconCard.addDropShadow()
        
        addCardButton.addDropShadow()
        addCardButton.setCornerRadius(radius: 5)
        
        addCardButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addCardPressed)))
        
        animView.setAnimation(named: "money")
        animView.loopAnimation = true
        animView.play()
    }
    
    func addCardPressed() {
        let vc = STPAddCardViewController()
        vc.delegate = self
        
        inVC?.navigationController?.isNavigationBarHidden = false
        inVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        inVC?.navigationController?.isNavigationBarHidden = true
        inVC?.navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            self.inVC?.navigationController?.isNavigationBarHidden = true
            self.inVC?.navigationController?.popViewController(animated: true)
            return
        }
        
        guard let userUnmanaged = Util.getCurrentUserUnmanaged(),
            let card = token.card else {
                self.inVC?.navigationController?.isNavigationBarHidden = true
                self.inVC?.navigationController?.popViewController(animated: true)
                return
        }
        
        let params = ["UserID": userUnmanaged.id,
                      "StripeToken": token.tokenId,
                      "CardLast4": card.last4,
                      "CardBrand": Util.cardBrand(brand: card.brand),
                      "ServerVersion": "2"]
        
        Alamofire.request(Config.shared.setCardURL, method: .post, parameters: params).responseJSON { (response) in
            
            let realm = try! Realm()
            let user = Util.getCurrentUser(realm)
            
            if (response.response?.statusCode != 200) {
                Util.defaultAlert(self, title: "Uh Oh...", message: "Something went wrong while uploading your card to our server. Try again later.", action: {
                    self.inVC?.navigationController?.isNavigationBarHidden = true
                    self.inVC?.navigationController?.popViewController(animated: true)
                    
                })
                
                self.inVC?.navigationController?.isNavigationBarHidden = true
                self.inVC?.navigationController?.popViewController(animated: true)
                return
            }
            
            if let json = response.result.value as? [String: Any],
                let success = json["Success"] as? Bool {
                if success {
                    try! realm.write {
                        if let img = UIImage(named: "\(Util.cardBrand(brand: token.card!.brand))CardImage") {
                            let imgData = UIImagePNGRepresentation(img)!
                            user?.creditCardImage = imgData
                        }
                        
                        user?.creditCardLast4 = token.card!.last4
                    }
                    
                    self.inVC?.navigationController?.isNavigationBarHidden = true
                    self.inVC?.navigationController?.popViewController(animated: true)
                    
                    self.finishAnim()
                } else {
                    let alertController = MDCAlertController(title: "Uh Oh...", message: "Something went wrong while uploading your card to our server. Try again later.")
                    
                    let action = MDCAlertAction(title:"OK") { (action) in
                        self.inVC?.navigationController?.isNavigationBarHidden = true
                        self.inVC?.navigationController?.popViewController(animated: true)
                    }
                    
                    alertController.addAction(action)
                    self.present(alertController, animated:true, completion: nil)
                }
            }
        }
    }
    
    func finishAnim() {
        Util.fadeOut(views: [self.inVC!.title3, self.inVC!.pageControl, self.inVC!.nextText, self.iconCard, self.text, self.addCardButton], duration: 0.5, completion: {
            self.checkAnim.isHidden = false
            self.checkAnim.setAnimation(named: "onboardingcheck2")
            self.checkAnim.loopAnimation = false
            
            self.checkAnim.play()
            
            Timer.scheduledTimer(withTimeInterval: 1.3, repeats: false, block: { (_) in
                let vc = self.inVC!.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                self.inVC?.navigationController?.pushViewController(vc, animated: true)
            })
        })
    }
}
