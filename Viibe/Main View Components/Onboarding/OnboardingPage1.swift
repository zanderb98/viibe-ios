//
//  OnboardingPage1.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 4/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit

class OnboardingPage1: UIViewController {

    @IBOutlet weak var iconCard: UIView!
    var marker: ViibeAnnotation!
    @IBOutlet weak var topText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconCard.setCornerRadius(radius: iconCard.frame.height / 2)
        iconCard.addDropShadow()
        
        let p = vParty()
        p.lit = 60;
        p.crowded = 50;
        
        marker = ViibeAnnotation()
        marker.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 10, y: topText.frame.origin.y + topText.frame.height + 50, width: 20, height: 20)
        marker.party = p;
        marker.color = .orange
        marker.setup()
        
        view.addSubview(marker)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (marker == nil) {
            return
        }
        
        marker.removeFromSuperview()
        
        let p = vParty()
        p.lit = 60;
        p.crowded = 50;
        
        marker = ViibeAnnotation()
        marker.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 10, y: topText.frame.origin.y + topText.frame.height + 50, width: 20, height: 20)
        marker.party = p;
        marker.color = .white
        marker.setup()
        view.addSubview(marker)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
