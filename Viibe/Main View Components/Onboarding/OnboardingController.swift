//
//  OnboardingController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 4/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import UIKit
import CHIPageControl
import MaterialComponents

class OnboardingController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    var pageContainer: UIPageViewController!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var viibeTitle: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var title3: UILabel!
    @IBOutlet weak var nextText: UILabel!
    
    @IBOutlet weak var pageControl: CHIPageControlAleppo!
    var page1: OnboardingPage1!
    var page2: OnboardingPage2!
    var page3: OnboardingPage3!
    
    var controllers: [UIViewController]!
    
    override func viewDidLoad() {
        pageControl.tintColor = .white
        
        page1 = OnboardingPage1(nibName: "OnboardingPage1", bundle: nil)
        page2 = OnboardingPage2(nibName: "OnboardingPage2", bundle: nil)
        page3 = OnboardingPage3(nibName: "OnboardingPage3", bundle: nil)
        page3.inVC = self
        
        controllers = [page1, page2, page3]
        
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.dataSource = self
        pageContainer.delegate = self
        pageContainer.setViewControllers([page1], direction: .forward, animated: false, completion: nil)
        
        (pageContainer.view.subviews.filter { $0 is UIScrollView }.first as? UIScrollView)?.delegate = self
        
        // Add it to the view
        view.insertSubview(pageContainer.view, at: 1)
        pageContainer.view.frame = view.frame
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let vc = pageViewController.viewControllers?.first {
            curr = controllers.index(of: vc)!
        }
    }
    
    var curr: Int = 0
    
    var title1start: CGFloat = 0
    var title2start: CGFloat = 0
    var title3start: CGFloat = 0
    
    var lastPage: CGFloat = 0.0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview!)
        let currDirection = translation.x > 0 ? -1 : 1

        var pageNumber = fabs(scrollView.contentOffset.x - scrollView.frame.width) / scrollView.frame.width
        
        pageNumber = CGFloat(curr) + CGFloat(currDirection) * pageNumber
        print("PAGNUM \(pageNumber) curr \(curr) dir \(currDirection)")
        
        title1.frame.origin.x = title1start - 100 * pageNumber
        viibeTitle.frame.origin.x = title1start + title1.frame.width + 4 - 100 * pageNumber
        let alpha1 = 1 - pageNumber
        title1.alpha = (alpha1 < 0) ? 0 : alpha1
        viibeTitle.alpha = title1.alpha
        
        title2.frame.origin.x = title2start - 100 * pageNumber
        
        var alpha2 = pageNumber
        if (curr == 1) {
            alpha2 = currDirection == 1 ? 2 - pageNumber : pageNumber
        } else if (curr == 2) {
            alpha2 = 2 - pageNumber
        }
        title2.alpha = (alpha2 < 0 || alpha2 > 1) ? 0 : alpha2
        
        title3.frame.origin.x = title3start - 100 * pageNumber
        let alpha3 = pageNumber - 1
        title3.alpha = (alpha3 < 0) ? 0 : alpha3
        
        if (pageNumber >= 2) {
            nextText.text = "Done"
        } else {
            nextText.text = "Next"
        }
        
        pageControl.progress = Double(pageNumber)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        title1.sizeToFit()
        title2.sizeToFit()
        title3.sizeToFit()
        viibeTitle.sizeToFit()
        
        title1start = view.center.x - (title1.frame.width + viibeTitle.frame.width + 4) / 2
        title2start = view.center.x - title2.frame.width / 2 + 100
        title3start = view.center.x - title3.frame.width / 2 + 200
        
        title1.frame.origin.x = title1start
        viibeTitle.frame.origin.x = title1start + title1.frame.width + 4
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController == page1 {
            return  nil
        } else if viewController == page2 {
            return page1
        } else {
            return page2
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController == page1 {
            return page2
        } else if viewController == page2 {
            return page3
        } else {
            return nil
        }
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        if curr != 2 {
            pageContainer.setViewControllers([controllers[curr + 1]], direction: .forward, animated: true, completion: { (_) in
                self.curr += 1
            })
        } else {
            let alertController = MDCAlertController(title: "Card Not Entered", message: "Remember, you will need to enter a card before you attend your first Viibe event.")
            
            let action = MDCAlertAction(title:"OK") { (action) in
               self.page3.finishAnim()
            }
            
            let action2 = MDCAlertAction(title: "Cancel")
            
            alertController.addAction(action)
            alertController.addAction(action2)
            self.present(alertController, animated:true, completion: nil)
        }
    }
    
}
