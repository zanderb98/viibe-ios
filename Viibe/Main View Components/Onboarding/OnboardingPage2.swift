//
//  OnboardingPage2.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 4/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import Lottie

class OnboardingPage2: UIViewController {

    @IBOutlet weak var iconCard: UIView!
    @IBOutlet weak var animView: LOTAnimationView!
    @IBOutlet weak var locationPermsButton: UIView!
    @IBOutlet weak var text: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        text.attributedText = NSMutableAttributedString().normal("We need access to your location to show you parties around you as well as to see when you leave the party to give accurate pricing. Tap the button below and select ", font: Fonts.fou(size: 19)).normal("Always Allow", font: Fonts.fouBold(size: 19)).normal(" to enable location.", font: Fonts.fou(size: 19))

        iconCard.setCornerRadius(radius: iconCard.frame.height / 2)
        iconCard.addDropShadow()
        
        locationPermsButton.addDropShadow()
        locationPermsButton.setCornerRadius(radius: 5)
        
        locationPermsButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(locationPermsPressed)))
        
        animView.setCornerRadius(radius: iconCard.frame.height / 2 - 10)
        animView.setAnimation(named: "location2")
        animView.loopAnimation = true
        animView.play()
    }

    func locationPermsPressed() {
        LocationManager.shared.requestLocationPermission()
    }

}
