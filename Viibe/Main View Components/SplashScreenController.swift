//
//  SplashScreenController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/4/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit

class SplashScreenController: UIViewController {

    @IBOutlet weak var logo: UIImageView!
    
    override func viewDidLoad() {
        navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let vc = storyboard!.instantiateViewController(withIdentifier:
            "\(Util.getLoginStatus() == .none ? "Login" : "Main")ViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
}
