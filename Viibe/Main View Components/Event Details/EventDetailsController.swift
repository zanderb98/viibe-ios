//
//  EventDetailsController.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/18/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import UIKit
import CoreLocation
import CHIPageControl
import MBCircularProgressBar
import Alamofire
import RealmSwift

class EventDetailsController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var graySection: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var partyName: UILabel!
    @IBOutlet weak var partyDate: UILabel!
    @IBOutlet weak var partyDistance: UILabel!
    @IBOutlet weak var favoriteImg: UIImageView!
    @IBOutlet weak var hostProfileImage: UIImageView!
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var partyDesc: UILabel!
    @IBOutlet weak var showMore: UILabel!
    @IBOutlet weak var partyDescHeight: NSLayoutConstraint!
    @IBOutlet weak var pageControl: CHIPageControlAleppo!
    
    @IBOutlet weak var followLabel: UILabel!
    @IBOutlet weak var litLabel: UILabel!
    @IBOutlet weak var crowdedLabel: UILabel!
    @IBOutlet weak var litNoTix: UILabel!
    @IBOutlet weak var crowdedNoTix: UILabel!
    @IBOutlet weak var getTicketsButton: UIView!
    @IBOutlet weak var getTicketsLabel: UILabel!
    @IBOutlet weak var navigateButton: UIView!
    @IBOutlet weak var uberButton: UIView!
    @IBOutlet weak var uberImage: UIImageView!
    @IBOutlet weak var crowdedMeter: MBCircularProgressBarView!
    @IBOutlet weak var litMeter: MBCircularProgressBarView!
    @IBOutlet weak var estimatedPrice: UILabel!
    
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var chatTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var numComments: UILabel!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var sendBtn: UIImageView?
    
    @IBOutlet weak var reserveTicketsLeft: UILabel!
    var reserveHeight: NSLayoutConstraint?
    
    var ticketButtonEnabled = false
    var estPriceShown = false
    
    var party: vParty?
    var descHeight: CGFloat = 0.0
    
    var estPrice = 0.0
    
    var favoriteChanged: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topBar.addDropShadow(opacity: 0.2)
        graySection.addDropShadow(offset: CGSize(width: 0, height: 5))
        hostProfileImage.layer.cornerRadius = hostProfileImage.frame.width / 2
        hostProfileImage.clipsToBounds = true
        
        imageScrollView.delegate = self
        
        getTicketsButton.setCornerRadius(radius: 5)
        
        navigateButton.setCornerRadius(radius: 5)
        
        uberButton.setCornerRadius(radius: 5)
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.getTicketsPressed))
        getTicketsButton.addGestureRecognizer(rec)
        
        let rec2 = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.uberPressed))
        uberButton.addGestureRecognizer(rec2)
        
        let rec3 = UITapGestureRecognizer(target: self, action: #selector(ViibeCardController.navigatePressed))
        navigateButton.addGestureRecognizer(rec3)
        
        let rec4 = UITapGestureRecognizer(target: self, action: #selector(EventDetailsController.closeKB))
        scrollView.addGestureRecognizer(rec4)

        setupWithParty()
        setupChat()
        
        // These are in the view controller extension
        NotificationCenter.default.addObserver(self, selector: #selector(EventDetailsController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EventDetailsController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func reload() {
        if party == nil { return }
        
        let oldChat = party!.comments
        
        let realm = try! Realm()
        if let partyRealm = realm.objects(Party.self).filter("id == '\(party!.id)'").first {
            party = vParty(partyRealm)
            
            if let reserved = realm.objects(Party.self).filter("id == 'r\(party!.id)'").first {
                party?.isReserved = true
                party?.key = reserved.key
            }
            
            setupWithParty()
        } else {
            return
        }
        
        var rowsAdded: [IndexPath] = []
        let newChat = party!.comments
        
        for i in 0..<newChat.count {
            let cmt = newChat[i]
            
            var found = false
            for old in oldChat {
                if cmt.id == old.id {
                    found = true
                    break
                }
            }
            
            if !found {
                rowsAdded.append(IndexPath(row: newChat.count - i - 1, section: 0))
            }
        }
        
        chatTable?.insertRows(at: rowsAdded, with: .automatic)
    }
    
    func closeKB() {
        self.view.endEditing(true)
    }
    
    func setupWithParty() {
        if party == nil { return }
        
        let realm = try! Realm()
        if let partyRealm = realm.objects(Party.self).filter("id == '\(party!.id)'").first {
            self.party = vParty(partyRealm)
            
            if let reserved = realm.objects(Party.self).filter("id == 'r\(party!.id)'").first {
                self.party?.isReserved = true
                self.party?.key = reserved.key
            }
        }
        
        partyName.text = party!.name
        partyDate.text = Util.getDateText(date: party!.date)
        
        favoriteImg.image = Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_empty")
        
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        let userCoord = CLLocation(latitude: lat, longitude: lng)
        
        let distanceMeters = userCoord.distance(from: CLLocation(latitude: party!.lat, longitude: party!.lng))
        let distanceMi = distanceMeters * 0.000621371192
        
        partyDistance.text = "\(String(format: "%.1f", distanceMi)) mi"
        
        imageScrollView.contentSize = CGSize(width: (imageScrollView.frame.width * CGFloat(party!.images.count)), height: imageScrollView.frame.height)
        
        pageControl.isHidden = party!.images.count <= 1
        for i in 0..<party!.images.count {
            let imgView = UIImageView(frame: CGRect(x: (imageScrollView.frame.width * CGFloat(i)),
                                                        y: 0, width: imageScrollView.frame.width,
                                                        height: imageScrollView.frame.height))
            
            if let url = URL(string: party!.images[i].url) {
                imgView.sd_setImage(with: url)
                imgView.contentMode = .scaleAspectFill
                
                imageScrollView.insertSubview(imgView, at: 0)
            }
        }
        
        if party!.images.isEmpty {
            let imgView = UIImageView(frame: CGRect(x: 0,
                                                    y: 0, width: imageScrollView.frame.width,
                                                    height: imageScrollView.frame.height))
            imgView.image = #imageLiteral(resourceName: "default-party-image")
            imgView.contentMode = .scaleAspectFill
            
            imageScrollView.insertSubview(imgView, at: 0)
        }
        
        pageControl.numberOfPages = party!.images.count
        
        if let host = party!.host,  let url = URL(string: host.profilePictureURL) {
            hostProfileImage.sd_setImage(with: url)
            hostName.text = "By \(host.name)"
        }
        
        partyDesc.text = party!.desc
        
        descHeight = party!.desc.height(font: partyDesc.font, maxWidth: partyDesc.frame.width)
        
        if descHeight > 60 {
            partyDescHeight.constant = 60
            showMore.isHidden = false
        } else {
            partyDescHeight.constant = descHeight
            showMore.isHidden = true
        }
        
        litMeter.value = CGFloat(party!.lit)
        crowdedMeter.value = CGFloat(party!.crowded)
        
        if party!.availableTix.isEmpty || party!.crowded < 15 {
            litMeter.value = 0
            crowdedMeter.value = 0
            
            litMeter.showValueString = false
            litMeter.showUnitString = false
            litNoTix.isHidden = false
            
            crowdedMeter.showValueString = false
            crowdedMeter.showUnitString = false
            crowdedNoTix.isHidden = false
        } else {
            litMeter.showValueString = true
            litMeter.showUnitString = true
            litNoTix.isHidden = true
            
            crowdedMeter.showValueString = true
            crowdedMeter.showUnitString = true
            crowdedNoTix.isHidden = true
        }
        
        numComments.text = "\(party!.numComments()) Comments"
        
        checkFollowing()
        
        guard let user = Util.getCurrentUserUnmanaged() else { return }
        
        if Util.getLoginStatus() == .loggedIn {
            loadEstimatedPrice(user)
        }
        
        setupGetTicketButton(user)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkFollowing()
    }
    
    func checkFollowing() {
        if Util.getLoginStatus() == .skipped {
            self.followLabel.isHidden = true
            return
        } else {
            self.followLabel.isHidden = false
        }
        
        if let user = Util.getCurrentUserUnmanaged(), let h = party!.host {
            self.followLabel.text = user.following.contains(h.id) ? "Following" : "Follow"
        }
    }
    
    func setupChat() {
        chatTable!.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        chatTable!.dataSource = self
        chatTable!.allowsSelection = false
        chatTable!.separatorStyle = .none
        chatTable!.rowHeight = UITableViewAutomaticDimension
        chatTable!.estimatedRowHeight = 100
        chatTable!.tableFooterView = Util.footerView(height: 10)
        chatTable!.tableHeaderView = Util.footerView(height: 10)
        
        messageField.addTarget(self, action: #selector(EventDetailsController.messageTextChanged), for: .editingChanged)
        messageField.attributedPlaceholder = NSAttributedString(string: "Enter a Comment",
                                                                attributes: [NSForegroundColorAttributeName: UIColor(hex: "999999")])
        
        sendBtn!.image = #imageLiteral(resourceName: "send").withRenderingMode(.alwaysTemplate)
        sendBtn!.tintColor = UIColor(hex: "EFEFEF")
        sendBtn!.isUserInteractionEnabled = true
        
        let rec = UITapGestureRecognizer(target: self, action: #selector(EventDetailsController.chatSendPressed))
        sendBtn!.addGestureRecognizer(rec)
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
    
        self.chatTableHeight.constant = self.chatTable!.contentSize.height
    }
    
    func setupGetTicketButton(_ user: vUser?) {
        if reserveHeight == nil {
            reserveHeight = reserveTicketsLeft.getConstraintByID(id: "reserveHeight")
        }
        
        if party!.availableTix.isEmpty {
            ticketButtonEnabled = false
            reserveHeight?.isActive = true
            
            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "PAY AT VENUE"
        } else if Util.getLoginStatus() == .skipped {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true
            
            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "LOGIN"
        } else if party!.isReserved {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true
            
            self.getTicketsLabel.text = "VIEW TICKET"
            self.reserveTicketsLeft.text = "\(party!.reserveTixLeft) LEFT"
            self.reserveTicketsLeft.isHidden = false
        } else if party!.isReservable {
            ticketButtonEnabled = true
            reserveHeight?.isActive = false
            
            self.getTicketsLabel.text = "RESERVE"
            self.reserveTicketsLeft.text = "\(party!.reserveTixLeft) LEFT"
            self.reserveTicketsLeft.isHidden = false
            self.getTicketsButton.backgroundColor = Colors.viibeBlue
        } else if !user!.creditCardLast4.isEmpty || estPrice == 0.0 {
            ticketButtonEnabled = true
            reserveHeight?.isActive = true
            
            self.getTicketsLabel.text = "GET TICKET"
            self.getTicketsLabel.textColor = .white
            self.getTicketsButton.backgroundColor = Colors.viibeBlue
        } else {
            ticketButtonEnabled = false
            reserveHeight?.isActive = true
            
            getTicketsButton.backgroundColor = UIColor(hex: "EFEFEF")
            getTicketsLabel.textColor = UIColor(hex: "cccccc")
            getTicketsLabel.text = "ADD PAYMENT"
        }
    }
    
    func uberPressed() {
        if party == nil { return }
        
        let urlStr = "https://m.uber.com/ul/?client_id=U2aHW22C3HAX0ZoNPHZofjCQxGw7s8oj&action=setPickup&pickup=my_location&dropoff[latitude]=\(party!.lat)&dropoff[longitude]=\(party!.lng)&dropoff[nickname]=\(party!.name)".replacingOccurrences(of: " ", with: "%20")
        
        guard let url = URL(string: urlStr) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func hostPressed(_ sender: Any) {
        if let p = party, let host = p.host {
            let hostVC = HostProfileController(nibName: "HostProfileController", bundle: nil)
            hostVC.host = host
            self.navigationController?.pushViewController(hostVC, animated: true)
        }
    }
    
    func navigatePressed() {
        if party == nil { return }
        
        let urlStr = "http://maps.apple.com/?daddr=\(party!.lat),\(party!.lng)"
        
        guard let url = URL(string: urlStr) else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    func getTicketsPressed() {
        if !ticketButtonEnabled { return }
        
        if reserveHeight == nil {
            reserveHeight = reserveTicketsLeft.getConstraintByID(id: "reserveHeight")
        }
        
        if Util.getLoginStatus() == .skipped {
            Util.clearLoginStatus()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            self.navigationController?.viewControllers = [vc]
            
            return
        }
        
        guard let user = Util.getCurrentUserUnmanaged() else {
            Util.defaultAlert(self, title: "Theres a problem...", message: "Log out and back in and try again.")
            return
        }
        
        if user.creditCardLast4.isEmpty && estPrice > 0 {
            Util.defaultAlert(self, title: "No Payment Method Set", message: "Go to the account page, add a credit/debit card, and try again.")
            return
        }
        
            if !Reachability.isConnectedToNetwork() {
                Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
                return
            }
            
            if (party!.isReservable) {
                if party!.reservableTicketCount > 1 {
                    showTicketPicker()
                } else {
                    for ticket in party!.availableTix {
                        if ticket.isReserveAvailable {
                            showReserve(ticket: ticket)
                        }
                    }
                }
            } else {
                if !party!.isReserved && party!.ticketCount > 1 {
                    showTicketPicker()
                } else {
                    if party!.isReserved {
                        for t in party!.tickets {
                            if t.id == party!.reservedTicketID {
                                showTicket(ticket: t)
                                return
                            }
                        }
                    }
                    
                   showTicket(ticket: party!.availableTix.first!)
                }
            }
    }
    
    func loadEstimatedPrice(_ user: vUser) {
        if party == nil {
            return
        }
        
        var ticketID = ""
        var minPrice = 99999999.0
        
        for ticket in party!.availableTix {
            if user.gender && ticket.malePrice < minPrice {
                ticketID = ticket.id
                minPrice = ticket.malePrice
            } else if !user.gender && ticket.femalePrice < minPrice {
                ticketID = ticket.id
                minPrice = ticket.femalePrice
            }
        }
        
        let params = ["PartyID": party!.id, "Gender": (user.gender ? "male" : "female"), "TicketID": ticketID, "ServerVersion": "2"]
        Alamofire.request(Config.shared.partyEstimateURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any] {
                self.estPrice = json["Price"] as! Double
                self.estimatedPrice.text =  "Est. $\(String(format: "%.2f", self.estPrice))"
                self.setupGetTicketButton(user)
                
                if !self.estPriceShown  {
                    Util.fadeIn(views: [self.estimatedPrice], duration: 0.4, completion: nil)
                    self.estPriceShown = true
                }
            } else {}
        }
    }
    
    @objc func showTicketPicker() {
        if party!.availableTix.count <= 1 {
            return
        }
        
        let actionSheet: UIAlertController = UIAlertController(title: "Pick Ticket Type", message: "This can be changed on the next page", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        actionSheet.addAction(cancelAction)
        
        for ticket in party!.availableTix {
            if party!.isReservable {
                if ticket.isReserveAvailable {
                    let action = UIAlertAction(title: ticket.name, style: .default) { action -> Void in
                        self.showReserve(ticket: ticket)
                    }
                    
                    actionSheet.addAction(action)
                }
            } else {
                if ticket.isAvailable {
                    let action = UIAlertAction(title: ticket.name, style: .default) { action -> Void in
                        self.showTicket(ticket: ticket)
                    }
                    
                    actionSheet.addAction(action)
                }
            }
        }
        
        //Present the AlertController
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showTicket(ticket: vTicket) {
        guard let p = party else { return }
        
        let card = ViibeCard()
        card.show(self, newControllerVC: self, party: p, type: "TICKET", selected: ticket, reloadDetails: reload)
    }
    
    func showReserve(ticket: vTicket) {
        guard let p = party else { return }

        let card = ViibeCard()
        card.show(self, newControllerVC: self, party: p, type: "RESERVE", selected: ticket, reloadDetails: reload)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.progress = Double(scrollView.contentOffset.x / scrollView.frame.size.width)
    }
    
    @IBAction func showMorePressed(_ sender: Any) {
        if showMore.isHidden {
            return
        }
        
        if showMore.text == "Show More" {
            partyDescHeight.constant = descHeight
            showMore.text = "Show Less"
        } else {
            partyDescHeight.constant = 60
            showMore.text = "Show More"
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func favoritesPressed(_ sender: Any) {
        setPartyFavorite()
        favoriteImg.image = Util.isFavorited(party!.id) ? #imageLiteral(resourceName: "heart_filled") : #imageLiteral(resourceName: "heart_empty")
        favoriteChanged?()
    }
    
    @IBAction func sharePressed(_ sender: Any) {
        guard let p = party else { return }
        
        let text = "Join me at \'\(p.name)\' on \(Util.getDayText(date: p.date, fullMonth: true)) at \(Util.getTimeText(date: p.date))! Download Viibe for details and tickets."
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
      
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func setPartyFavorite() {
        if Util.isFavorited(party!.id) {
            Util.unfavoriteParty(party!.id)
        } else {
            Util.favoriteParty(party!.id)
        }
    }
    
    @IBAction func followPressed(_ sender: Any) {
        guard let user = Util.getCurrentUserUnmanaged(), let h = party!.host else { return }
        let newFollowing = !user.following.contains(h.id)
        let add = newFollowing ? 1 : -1
        
        self.followLabel.text = !newFollowing ? "Follow" : "Following"
        
        let params = [
            "HostID": h.id,
            "UserID": user.id,
            "Follow": newFollowing ? "true" : "false",
            "ServerVersion": "2"
        ]
        
        Alamofire.request(Config.shared.followURL, method: .post, parameters: params).response { (response) in
            if response.response?.statusCode == 200 {
                do {
                    let realm = try Realm()
                    let rUser = Util.getCurrentUser(realm)!
                    let rHost = Util.getHost(h.id)!
                    
                    try! realm.write {
                        rHost.followers += add
                        
                        if newFollowing {
                            let obj = StringObj()
                            obj.str = h.id
                            rUser.following.append(obj)
                        } else {
                            for i in 0..<rUser.following.count {
                                if rUser.following[i].str == h.id {
                                    rUser.following.remove(objectAtIndex: i)
                                    break
                                }
                            }
                        }
                    }
                } catch {}
            } else {
                Util.defaultAlert(self, title: "Failed to follow \(h.name)")
                self.followLabel.text = newFollowing ? "Follow" : "Following"
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var originalHeight: CGFloat = -1
    var keyboardOpen = false
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if !keyboardOpen {
                originalHeight = scrollView.frame.size.height
                self.scrollView.frame.size.height -= keyboardSize.height
                
                keyboardOpen = true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if keyboardOpen {
            self.scrollView.frame.size.height = originalHeight
            keyboardOpen = false
        }
    }
}

extension EventDetailsController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return party!.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        let cmt = party!.comments[party!.comments.count - indexPath.row - 1]
        
        cell.setup(cmt, partyID: party!.id, vc: self, heightUpdated: {
            tableView.beginUpdates()
            tableView.endUpdates()
            
            self.viewWillLayoutSubviews()
        }, last: indexPath.row == party!.comments.count - 1)
        return cell
    }
    
    func messageTextChanged() {
        sendBtn?.tintColor = messageField!.text!.isEmpty ? UIColor(hex: "EFEFEF"): Colors.viibeOrange
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func chatSendPressed() {
        Util.checkUsername(self) {
            self.send()
        }
    }
    
    func send() {
        guard let u = Util.getCurrentUserUnmanaged(), let p = party else {
            return
        }
        
        if messageField == nil || messageField!.text!.isEmpty {
            return
        }
        
        let params = [
            "PartyID": p.id,
            "UserID": u.id,
            "Text": messageField!.text!,
            "Parents": "[]",
            "ServerVersion": "2"
        ]
        
        Alamofire.request(Config.shared.sendCommentURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let commentJSON = json["Comment"] as? [String: Any] {
                
                let comment = Comment()
                comment.setup(commentJSON)
                
                let realm = try! Realm()
                if let realmParty = realm.objects(Party.self).first(where: { (realmParty) -> Bool in
                    return realmParty.id == self.party!.id
                }) {
                    try! realm.write {
                        realmParty.comments.append(comment)
                    }
                }
                
                self.messageField!.text = ""
                self.view.endEditing(true)
                self.party!.comments.append(vComment(comment))
                self.chatTable!.beginUpdates()
                self.chatTable!.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.chatTable!.endUpdates()
                
                self.sendBtn!.tintColor = UIColor(hex: "DDDDDD")
                
                self.viewWillLayoutSubviews()
                
            } else {
                Util.defaultAlert(self, title: "Failed to Send Comment")
            }
        }
    }
}
