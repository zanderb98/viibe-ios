//
//  vUser.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/25/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation

class vUser {
    var name = ""
    var username = ""
    var id = ""
    
    var gender = true // true = male, false = female
    var malesInGroup = 0
    var femalesInGroup = 0
    var lastTimeGroupSet: Date?
    
    var lastKnownLat = 0.0
    var lastKnownLng = 0.0
    
    var profilePicture: Data?
    
    var creditCardImage: Data?
    var creditCardLast4 = ""
    
    var currentParty: vParty?
    
    var pastParties: [vParty] = []
    var reservedTickets: [vParty] = []
    var following: [String] = []
    var likes: [String] = []
    var dislikes: [String] = []
    
    init(_ user: User) {
        self.id = user.id
        self.name = user.name
        self.username = user.username
        self.gender = user.gender
        self.malesInGroup = user.malesInGroup
        self.femalesInGroup = user.femalesInGroup
        self.lastTimeGroupSet = user.lastTimeGroupSet
        self.lastKnownLat = user.lastKnownLat
        self.lastKnownLng = user.lastKnownLng
        self.profilePicture = user.profilePicture
        self.creditCardLast4 = user.creditCardLast4
        self.creditCardImage = user.creditCardImage
        
        if let currParty = user.currentParty {
            currentParty = vParty(currParty)
        }
        
        for party in user.pastParties {
            pastParties.append(vParty(party))
        }
        
        for party in user.reservedTickets {
            reservedTickets.append(vParty(party))
        }
        
        for host in user.following {
            following.append(host.str)
        }
        
        for host in user.likes {
            likes.append(host.str)
        }
        
        for host in user.dislikes {
            dislikes.append(host.str)
        }
    }
    
    func isGroupValid() -> Bool {
        if lastTimeGroupSet == nil { return false }
        var startOfDay = NSCalendar.current.startOfDay(for: Date()).timeIntervalSince1970 as Double
        
        startOfDay = Calendar.current.component(.hour, from: Date()) < 6 ?
            (startOfDay - (18 * 60 * 60)) :
            (startOfDay + (6 * 60 * 60))
        
        return lastTimeGroupSet! > Date(timeIntervalSince1970: startOfDay)
    }
}
