//
//  ViibeAnnotation.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/23/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import UIKit
import Mapbox
import Pulsator

class ViibeAnnotation: MGLAnnotationView {
    
    var party: vParty?
    var pulsator: Pulsator?
    var color: UIColor = UIColor.orange
    var noTixColor: UIColor = UIColor(hex: "FBC02D")
    var pulsatorPos: CGPoint?

    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = party != nil && party!.availableTix.isEmpty ? noTixColor : color
//        centerOffset = CGVector(dx: -10, dy: -10)
        
        // Force the annotation view to maintain a constant size when the map is tilted.
        scalesWithViewingDistance = false
        
        // Use CALayer’s corner radius to turn this view into a circle.
        layer.cornerRadius = frame.width / 2
    
        if pulsator == nil {
            pulsator = Pulsator()
            pulsator!.animationDuration = 2
            pulsator!.pulseInterval = 0
            pulsator!.radius = 40
            pulsator!.numPulse = 1

            pulsator!.backgroundColor = party != nil && party!.availableTix.isEmpty ? noTixColor.cgColor : color.cgColor
        
            self.layer.addSublayer(pulsator!)
            
            pulsatorPos = CGPoint(x: pulsator!.position.x + 10, y: pulsator!.position.y + 10)
        }
        
        layer.layoutIfNeeded()
        pulsator!.position = pulsatorPos!
        
        if (party != nil) { setup() }
        
        pulsator!.start()
    }
    
    func setup() {
        if (party == nil) { return }
        
        pulsator?.animationDuration = 1.0 + (1.0 - (Double(party!.lit) / 100.0))
        pulsator?.numPulse = 1 + Int(3.0 * (Double(party!.crowded) / 100.0))
    }
    
}
