//
//  vHost.swift
//  viibehost
//
//  Created by Zander Bobronnikov on 5/19/18.
//  Copyright © 2018 Viibe. All rights reserved.
//

import Foundation
import UIKit

class vHost: Equatable {
    var name = ""
    var desc = ""
    var id = ""
    var followers = 0
    
    var profilePicture: UIImage?
    var profilePictureURL = ""
    
    var hostedParties: [String] = []
    
    init() {}
    
    init(_ host: Host) {
        self.id = host.id
        self.name = host.name
        self.desc = host.desc
        self.followers = host.followers

        self.profilePictureURL = host.profilePictureURL
        
        if let data = host.profilePicture {
            profilePicture = UIImage(data: data, scale: 1.0)
        }
      
        for party in host.hostedParties {
            hostedParties.append(party.str)
        }
    }
    
    static func ==(lhs: vHost, rhs: vHost) -> Bool {
        return lhs.id == rhs.id
    }
}

