//
//  vComment.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

class vComment {
    var userID: String = ""
    var username: String = ""
    var id: String = ""
    var text: String = ""
    var date: Date = Date()
    
    var hasParent = false
    
    var comments: [vComment] = []
    
    init(_ comment: Comment) {
        self.id = comment.id
        self.username = comment.username
        self.userID = comment.userID
        self.text = comment.text
        self.date = comment.date
        self.hasParent = comment.hasParent
        
        for cmt in comment.comments {
            self.comments.append(vComment(cmt))
        }
        
    }
    
    func numComments() -> Int {
        if comments.isEmpty {
            return 1
        }
        
        var sum = 0
        for cmt in comments {
            sum += cmt.numComments()
        }
        
        return 1 + sum
    }
    
    func numReplies() -> Int {
        if comments.isEmpty {
            return 1
        }
        
        var sum = 0
        for cmt in comments {
            sum += cmt.numComments()
        }
        
        return sum
    }
}
