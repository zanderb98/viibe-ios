//
//  StringObj.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 5/28/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import RealmSwift

class StringObj: Object {
    @objc dynamic var str: String = ""
}
