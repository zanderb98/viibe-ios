//
//  vPost.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation

class vPost: Equatable, Comparable {
    var type: String = ""

    var userID: String = ""
    var username: String = ""
    var id: String = ""
    var text: String = ""
    var date: Date = Date()
    var lat: Double = 0.0
    var lng: Double = 0.0
    var address: String = ""
    var likes = 0
    
    var eventDate: Date = Date()
    var title: String = ""
    var caption: String = ""
    
    var comments: [vComment] = []
    
    init(_ post: Post) {
        self.type = post.type
        self.id = post.id
        self.username = post.username
        self.userID = post.userID
        self.text = post.text
        self.date = post.date
        self.lat = post.lat
        self.lng = post.lng
        self.address = post.address
        self.likes = post.likes
        
        self.eventDate = post.eventDate
        self.title = post.title
        self.caption = post.caption
        
        for comment in post.comments {
            comments.append(vComment(comment))
        }
    }
    
    static func == (lhs: vPost, rhs: vPost) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func < (lhs: vPost, rhs: vPost) -> Bool {
        let selected = UserDefaults.standard.string(forKey: "SelectedCity") ?? "Boston"
        
        let lClosest = Util.closestCity(lat: lhs.lat, lng: lhs.lng)
        let rClosest = Util.closestCity(lat: rhs.lat, lng: rhs.lng)
        
        if lClosest == selected && rClosest != selected {
            return true
        } else if rClosest == selected && lClosest != selected {
            return false
        } else {
            
            var lDiff = (Date().timeIntervalSince1970 - lhs.date.timeIntervalSince1970) / 3600.0
            var rDiff = (Date().timeIntervalSince1970 - rhs.date.timeIntervalSince1970) / 3600.0
            
            lDiff = lDiff < 0 ? 1 : lDiff
            
            rDiff = rDiff < 0 ? 1 : rDiff

            if (lhs.likes == rhs.likes) {
                return lhs.date > rhs.date
            }
            
            return (0.75 * Double(lhs.likes) / lDiff + 0.25 * Double(lhs.likes)) >
                (0.75 * Double(rhs.likes) / rDiff + 0.25 * Double(rhs.likes))
        }
    }
    
    func startsWith(str: String) -> Bool {
        return text.lowercased().starts(with: str.lowercased()) ||
            "@\(username)".lowercased().starts(with: str.lowercased()) ||
            title.lowercased().starts(with: str.lowercased()) ||
            caption.lowercased().starts(with: str.lowercased())
        
    }
    
    func contains(str: String) -> Bool {
        return text.lowercased().contains(str.lowercased()) ||
            "@\(username)".lowercased().contains(str.lowercased()) ||
            title.lowercased().contains(str.lowercased()) ||
            caption.lowercased().contains(str.lowercased())
        
    }
    
    func numComments() -> Int {
        var sum = 0
        for cmt in comments {
            sum += cmt.numComments()
        }
        
        return sum
    }
}
