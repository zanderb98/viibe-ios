//
//  vParty.swift
//  Venu
//
//  Created by Zander Bobronnikov on 9/24/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import CoreLocation

class vParty: Equatable {
    var name = ""
    var desc = ""
    var city = ""
    
    var lit = 0 // 0 - 100
    var crowded = 0 // 0 - 100
    var id = ""
    var date = Date()
    var endDate = Date()
    var favorite = false
    
    var lat: Double = 0.0
    var lng: Double = 0.0
    var address = ""
    
    var malePrice: Double = 0.0
    var femalePrice: Double = 0.0
    
    var maleGuests = 0
    var femaleGuests = 0
    
    var capacity = 0
    var chatEnabled = true
    
    var host: vHost?
    
    var open = false
    var hasBeenClosed = false
    var lastTimeClosed: Date?
    
    var dateEntered: Date?
    var price: Double = 0.0
    
    var prophit: Double = 0.0
    var analytics: [vPartyAnalytics] = []
    
    var tickets: [vTicket] = []
    var comments: [vComment] = []
    var images: [vPartyImage] = []
    
    var isPastParty = false
    var isMyParty = false
    var isBouncedParty = false;
    var isReserved = false;
    
    var password = ""
    var bouncerCode = ""
    
    var key = ""
    var reservedTicketID = ""

    //66 Percent Females
    var targetPercent = 0.66
    
    var availableTix: [vTicket] {
        var ret: [vTicket] = []
        
        for ticket in tickets {
            if ticket.isAvailable || ticket.isReserveAvailable {
                ret.append(ticket)
            }
        }
        
        return ret
    }
    
    var isReservable: Bool {
        if Date().compare(date) != .orderedAscending || isReserved {
            return false
        }
        
        return reserveTixLeft > 0
    }
    
    var reserveTixLeft: Int {
        var total = 0
        
        for ticket in tickets {
            total += (ticket.reserveTickets - ticket.reserveTicketsSold)
        }
        
        return total
    }
    
    
    var tixLeft: Int {
        var total = 0
        
        for ticket in tickets {
            total += (ticket.quantity - ticket.quantitySold)
        }
        
        return total
    }
    
    var reservableTicketCount: Int {
        var total = 0
        
        for ticket in tickets {
            total += ticket.isReserveAvailable ? 1 : 0
        }
        
        return total
    }
    
    var ticketCount: Int {
        var total = 0
        
        for ticket in tickets {
            total += ticket.isAvailable ? 1 : 0
        }
        
        return total
    }
    
    init() {}
    
    init(_ party: Party) {
        self.name = party.name
        self.desc = party.desc
        self.city = party.city
        self.lit = party.lit
        self.crowded = party.crowed
        self.id = party.id
        self.date = party.date
        self.endDate = party.endDate
        self.lat = party.lat
        self.lng = party.lng
        self.address = party.address
        self.malePrice = party.malePrice
        self.femalePrice = party.femalePrice
        self.maleGuests = party.maleGuests
        self.femaleGuests = party.femaleGuests
        self.capacity = party.capacity
        self.chatEnabled = party.chatEnabled
        
        self.open = party.open
        self.hasBeenClosed = party.hasBeenClosed
        self.lastTimeClosed = party.lastTimeClosed
        
        self.dateEntered = party.dateEntered
        self.price = party.price
        
        self.key = party.key
        self.reservedTicketID = party.reservedTicketID
        
        self.prophit = party.prophit
        
        self.isPastParty = party.isPastParty
        self.isMyParty = party.isMyParty
        self.isBouncedParty = party.isBouncedParty
        self.isReserved = party.isReserved
        
        self.password = party.password
        self.bouncerCode = party.bouncerCode
        
        if let h = party.host {
            self.host = vHost(h)
        }
        
        for analytic in party.analytics {
            self.analytics.append(vPartyAnalytics(analytic))
        }
        
        for ticket in party.tickets {
            self.tickets.append(vTicket(ticket))
        }
        
        for cmt in party.comments {
            self.comments.append(vComment(cmt))
        }
        
        for img in party.images {
            self.images.append(vPartyImage(img))
        }
    }
    
    static func ==(lhs: vParty, rhs: vParty) -> Bool {
        return lhs.id == rhs.id
    }
    
    func numComments() -> Int {
        var sum = 0
        for cmt in comments {
            sum += cmt.numComments()
        }
        
        return sum
    }
    
    func distanceMi() -> Double {
        let userLat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let userLng = UserDefaults.standard.double(forKey: "LastKnownLng")
        
        let userLoc = CLLocation(latitude: userLat, longitude: userLng)
        let distanceMeters = userLoc.distance(from: CLLocation(latitude: self.lat, longitude: self.lng))
        return distanceMeters * 0.000621371192
    }
}
