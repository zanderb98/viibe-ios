//
//  HostLoader.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 7/21/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class HostLoader {
    
    static func loadHosts(complete: @escaping (() -> Void)) {
        
        let params = ["ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.getHostsURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let hostsArr = json["Hosts"] as? [[String: Any]] {
                
                var hosts: [Host] = []
                
                for json in hostsArr {
                    let host = Host()
                    host.setup(json: json)
                    
                    hosts.append(host)
                }
                
                let realm = try! Realm()
                let toDelete = realm.objects(Host.self).filter({ (host) -> Bool in
                    for h in hosts {
                        if h.id == host.id {
                            return false
                        }
                    }
                    
                    return true
                })
                
                try! realm.write {
                    realm.delete(toDelete)
                    realm.add(hosts, update: true)
                }
            }
            
            complete()
        }
    }
}
