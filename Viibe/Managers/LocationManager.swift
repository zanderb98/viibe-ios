//
//  LocationManager.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 2/19/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift
import Alamofire

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    private let manager = CLLocationManager()
    private var locationChanged: ((CLLocationCoordinate2D) -> Void)?
    
    override init() {
        super.init()
        manager.delegate = self
    }
    
    func getLocationUpdates(_ callback: @escaping ((CLLocationCoordinate2D) -> Void)) {
        self.locationChanged = callback
        
        if CLLocationManager.locationServicesEnabled() {
            manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            manager.startUpdatingLocation()
        }
    }
    
    func atParty(_ party: vParty) {
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            let region = CLCircularRegion(center: CLLocationCoordinate2DMake(party.lat, party.lng), radius: 100, identifier: party.id)
            
            region.notifyOnExit = true
            region.notifyOnEntry = false
            
            manager.startMonitoring(for: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let callback = locationChanged,
            let loc = manager.location {
            callback(loc.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LOCATION FAILED \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let realm = try! Realm()
        if let party = realm.objects(Party.self).filter("id == '\(region.identifier)'").first {
            guard let user = Util.getCurrentUserUnmanaged() else {
                return
            }
            
            if Reachability.isConnectedToNetwork() {
                let params = ["UserID": user.id,
                              "PartyID": party.id,
                              "ServerVersion": "2"]
                
                Alamofire.request(Config.shared.leavePartyURL, method: .post, parameters: params).responseJSON { (response) in
                }
            }
        }
    }
    
    static func geocodeLatLng(lat: Double, lng: Double, complete: @escaping (String?) -> Void) {
        Alamofire.request(Config.shared.geocodeApiURL(lat: lat, lng: lng), method: .get, parameters: [:]).responseJSON { (response) in
            
            if (response.response?.statusCode != 200) {
                complete(nil)
                return
            }
            
            if let json = response.result.value as? [String: Any],
                let results = (json["results"] as? [[String: Any]]),
                let result = results.first {
                let fullAddress = result["formatted_address"] as! String
                
                let comps = fullAddress.components(separatedBy: ",")
                if comps.count == 1 {
                    complete(fullAddress)
                } else if comps.count > 1 {
                     complete(comps[0] + "," + comps[1])
                } else {
                    complete(nil)
                }
                
            }
            
        }
    }
    
    var locationCallback: ((CLAuthorizationStatus) -> Void)?
    
    func requestLocationPermission(callback: ((CLAuthorizationStatus) -> Void)? = nil) {
        self.locationCallback = callback ?? self.locationCallback
        manager.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationCallback?(status)
        
        if status != .authorizedAlways && status != .authorizedWhenInUse {
            requestLocationPermission()
        }
    }
}
