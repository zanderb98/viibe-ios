//
//  FeedLoader.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 8/3/18.
//  Copyright © 2018 Brodly. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class FeedLoader {
    static func loadPosts(complete: ((() -> Void))? = nil) {
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        
        var params = ["Latitude": lat,
                      "Longitude": lng,
                      "ServerVersion": "2"] as [String : Any]
        
        if let user = Util.getCurrentUserUnmanaged() {
            params["UserID"] = user.id
        }
        
        let realm = try! Realm()
        var oldPosts: [vPost] = []
        
        for post in realm.objects(Post.self) {
            oldPosts.append(vPost(post))
        }
        
        Alamofire.request(Config.shared.getFeedURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let jsonArr = json["Posts"] as? [[String: Any]] {
                
                var posts: [Post] = []
                
                for json in jsonArr {
                    let post = Post()
                    post.setup(json: json)
                    
                    posts.append(post)
                }
                
                let realm = try! Realm()
                
                //Delete parties no longer in list
                let toDelete = realm.objects(Post.self).filter({ (post) -> Bool in
                    for p in posts {
                        if p.id == post.id {
                            return false
                        }
                    }
                    
                    return (post.id.count == 24)
                })
                
                //Update parties ins realm
                try! realm.write {
                    realm.delete(toDelete)
                    realm.add(posts, update: true)
                }
                
                if !posts.isEmpty || !toDelete.isEmpty {
                    NotificationCenter.default.post(Notification(name: Notifications.PostsUpdated))
                    complete?()
                }
            }
        }
    }
    
    static func loadUserPosts(id: String, complete: ((([Post], Int, Int) -> Void))? = nil) {
        let params = ["UserID": id,
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.feedUserURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let jsonArr = json["Posts"] as? [[String: Any]],
                let following = json["Following"] as? Int,
                let followers = json["Followers"] as? Int {
                
                var posts: [Post] = []
                
                for json in jsonArr {
                    let post = Post()
                    post.setup(json: json)
                    
                    posts.append(post)
                }
                
                let realm = try! Realm()
  
                //Update parties ins realm
                try! realm.write {
                    realm.add(posts, update: true)
                }
                
                
                complete?(posts, following, followers)
            } else {
                complete?([], 0, 0)
            }
        }
    }
}
