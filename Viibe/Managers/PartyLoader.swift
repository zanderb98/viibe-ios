//
//  PartyLoader.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/6/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class PartyLoader {
    static func loadParties(latitude: Double, longitude: Double, complete: @escaping (() -> Void)) {

        let params = ["Latitude": latitude,
                      "Longitude": longitude,
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.getPartiesURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let jsonArr = response.result.value as? [[String: Any]] {
                var parties: [Party] = []
                
                for json in jsonArr {
                    let party = Party()
                    party.setup(json: json)
                    
                    parties.append(party)
                }
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(parties, update: true)
                }
                
                complete()
            }
        }
    }
    
    static func getParties(ids: [String], host: Host? = nil, complete: @escaping (([Party]) -> Void)) {
        
        let params = ["IDs": ids,
                      "ServerVersion": "2"] as [String : Any]
        
        Alamofire.request(Config.shared.getPartiesURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let jsonArr = json["Parties"] as? [[String: Any]] {
                var parties: [Party] = []
                
                for json in jsonArr {
                    let party = Party()
                    party.setup(json: json)
                    party.host = host
                    
                    parties.append(party)
                }
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(parties, update: true)
                }
                
                complete(parties)
            } else {
                complete([])
            }
        }
    }

    static func updateParties() {
        let lat = UserDefaults.standard.double(forKey: "LastKnownLat")
        let lng = UserDefaults.standard.double(forKey: "LastKnownLng")
        
        let params = ["Latitude": lat,
                      "Longitude": lng,
                      "ServerVersion": "2"] as [String : Any]
        
        let realm = try! Realm()
        var oldParties: [vParty] = []
        
        for party in realm.objects(Party.self) {
            oldParties.append(vParty(party))
        }

        Alamofire.request(Config.shared.getPartiesURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let jsonArr = json["Parties"] as? [[String: Any]] {
                
                var parties: [Party] = []
                
                for json in jsonArr {
                    let party = Party()
                    party.setup(json: json)
                    
                    parties.append(party)
                }
                
                let realm = try! Realm()
                
                //Delete parties no longer in list
                let toDelete = realm.objects(Party.self).filter({ (party) -> Bool in
                    for p in parties {
                        if p.id == party.id {
                            return false
                        }
                    }
                    
                    return (party.id.count == 24)
                })
                
                //Update parties ins realm
                try! realm.write {
                    realm.delete(toDelete)
                    realm.add(parties, update: true)
                }
                
                if !parties.isEmpty || !toDelete.isEmpty {
                    NotificationCenter.default.post(Notification(name: Notifications.PartiesUpdated))
                }
            }
        }
    }
}
