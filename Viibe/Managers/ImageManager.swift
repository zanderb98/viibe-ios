//
//  ImageManager.swift
//  viibehost
//
//  Created by Zander Bobronnikov on 5/20/18.
//  Copyright © 2018 Viibe. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import RealmSwift

class ImageManager {
    static func downloadImages() {
//        let realm = try! Realm()
//        let parties = realm.objects(Party.self)
//        let savedImages = realm.objects(SavedImage.self)
//
//        var hostPicURLs: [String: [String]] = [:]
//
//        for party in parties {
//            if let host = party.host {
//                if hostPicURLs[host.profilePictureURL] == nil {
//                    hostPicURLs[host.profilePictureURL] = [party.id]
//                } else {
//                    var arr = hostPicURLs[host.profilePictureURL]!
//                    arr.append(party.id)
//
//                    hostPicURLs[host.profilePictureURL] = arr
//                }
//            }
//
//            outer: for img in party.images {
//                if img.data != nil || img.url.isEmpty {
//                    return
//                }
//
//                for saved in savedImages {
//                    if saved.url == img.url {
//                        let r2 = try! Realm()
//
//                        if let realmImg = r2.objects(PartyImage.self).first(where: { (rImg) -> Bool in
//                            return rImg.id == img.id
//                        }) {
//                            try! r2.write {
//                                realmImg.data = saved.data
//                            }
//                        }
//
//                        continue outer
//                    }
//                }
//
//                let url = img.url
//                Alamofire.request(url).responseImage { response in
//                    if let image = response.result.value {
//                        let r2 = try! Realm()
//
//                        let saved = SavedImage()
//                        saved.setup(url, image: image)
//
//                        if let realmImg = r2.objects(PartyImage.self).first(where: { (rImg) -> Bool in
//                            return rImg.id == img.id
//                        }) {
//                            try! r2.write {
//                                realmImg.setImage(img: image)
//                                r2.add(saved, update: true)
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        outer: for url in hostPicURLs.keys {
//            for img in savedImages {
//                if img.url == url {
//                    let r2 = try! Realm()
//
//                    let parties = r2.objects(Party.self).filter({ (party) -> Bool in
//                        return hostPicURLs[url]!.contains(party.id)
//                    })
//
//                    try! r2.write {
//                        for party in parties {
//                            party.host?.profilePicture = img.data
//                        }
//                    }
//
//                    continue outer
//                }
//            }
//
//            Alamofire.request(url).responseImage { response in
//                if let image = response.result.value {
//                    let r2 = try! Realm()
//
//                    let parties = r2.objects(Party.self).filter({ (party) -> Bool in
//                        return hostPicURLs[url]!.contains(party.id)
//                    })
//
//                    let saved = SavedImage()
//                    saved.setup(url, image: image)
//
//                    try! r2.write {
//                        for party in parties {
//                           party.host?.setImage(img: image)
//                        }
//
//                        r2.add(saved, update: true)
//                    }
//                }
//            }
//        }
    }
}
