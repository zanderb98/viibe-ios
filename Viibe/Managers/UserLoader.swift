//
//  UserLoader.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/5/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import FacebookCore
import Alamofire
import RealmSwift

class UserLoader {
    static func loadProfilePicture(url: URL, user: User,
                                   failed: @escaping (() -> Void),
                                   complete: (() -> Void)) {
        
        guard let data = try? Data(contentsOf: url) else {
            failed()
            return
        }
        
        user.profilePicture = data
        complete()
    }
    
    static func loginFB(accessToken: AccessToken, user: User,
                      failed: @escaping (() -> Void),
                      complete: @escaping ((Bool) -> Void)) {
        guard let userID = accessToken.userId else { return }
        
        let params = ["FBAccessToken": accessToken.authenticationToken,
                      "FBUserID": userID,
                      "Platform": "iOS",
                      "ServerVersion": "2",
                      "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]
        
        Alamofire.request(Config.shared.loginURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                user.setup(json: userJson)
                complete(userJson["FirstLogin"] as! Bool)
            } else {
                failed()
            }
        }
    }
    
    static func loginGoogle(idToken: String, accessToken: String, user: User,
                        failed: @escaping (() -> Void),
                        complete: @escaping ((Bool) -> Void)) {
        
        let params = ["GoogleIDToken": idToken,
                      "GoogleAccessToken": accessToken,
                      "Type": "google",
                      "Platform": "iOS",
                      "ServerVersion": "2",
                      "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]
        
        Alamofire.request(Config.shared.loginURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                user.setup(json: userJson)
                complete(userJson["FirstLogin"] as! Bool)
            } else {
                failed()
            }
        }
    }
    
    static func loginEmail(email: String, password: String, user: User,
                        failed: @escaping (() -> Void),
                        complete: @escaping ((Bool) -> Void)) {
        
        let params = [
            "Type": "email",
            "Email": email,
            "Password": password,
            "Platform": "iOS",
            "ServerVersion": "2",
            "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]
        
        Alamofire.request(Config.shared.loginURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                user.setup(json: userJson)
                complete(userJson["FirstLogin"] as! Bool)
            } else {
                failed()
            }
        }
    }
    
    static func signup(email: String, password: String, name: String, gender: String,
                           failed: @escaping (() -> Void),
                           complete: @escaping (() -> Void)) {
        
        let params = [
            "Email": email,
            "Password": password,
            "Name": name,
            "Gender": gender,
            "Platform": "iOS",
            "ServerVersion": "2",
            "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]
        
        Alamofire.request(Config.shared.signupURL, method: .post, parameters: params).responseJSON { (response) in
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                let user = User()
                user.setup(json: userJson)
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(user)
                }
                
                complete()
            } else {
                failed()
            }
        }
    }
    
    static func updateUser(_ old: vUser) {
        
        let params = ["UserID": old.id,
                      "ServerVersion": "2"]
        
        Alamofire.request(Config.shared.updateUserURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                
                let user = User()
                
                user.setup(json: userJson)
                
                user.profilePicture = old.profilePicture
                user.lastKnownLat = old.lastKnownLat
                user.lastKnownLng = old.lastKnownLng
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(user, update: true)
                    NotificationCenter.default.post(Notification(name: Notifications.UserUpdated))
                }
            }
        }
    }
}
