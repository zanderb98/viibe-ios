//
//  AccountLoader.swift
//  Viibe
//
//  Created by Zander Bobronnikov on 10/4/17.
//  Copyright © 2017 Brodly. All rights reserved.
//

import Foundation
import RealmSwift
import FacebookCore

class AccountLoader {
    static func loginLoadFB(accessToken: AccessToken, complete: @escaping ((Bool) -> Void), failed: @escaping (() -> Void)) {
        let dispatchGroup = DispatchGroup()
        let user = User()
        
        var firstLogin = false
        
        dispatchGroup.enter()
        UserLoader.loginFB(accessToken: accessToken, user: user, failed: failed) { (first) in
            firstLogin = first
            dispatchGroup.leave()
        }
        
        if let userID = accessToken.userId,
             let url = URL(string: "https://graph.facebook.com/\(userID)/picture?type=large") {
            
            dispatchGroup.enter()
            UserLoader.loadProfilePicture(url: url, user: user, failed: failed) {
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(user)
            }
            
            UserDefaults.standard.set(true, forKey: Constants.FirstListLoad)
            complete(firstLogin)
        }
    }
    
    static func loginLoadGoogle(idToken: String, accessToken: String, profilePicURL: URL?, complete: @escaping ((Bool) -> Void), failed: @escaping (() -> Void)) {
        let dispatchGroup = DispatchGroup()
        let user = User()
        
        var firstLogin = false
        
        dispatchGroup.enter()
        UserLoader.loginGoogle(idToken: idToken, accessToken: accessToken, user: user, failed: failed) { (first) in
            firstLogin = first
            dispatchGroup.leave()
        }
        
        if let url = profilePicURL {
            
            dispatchGroup.enter()
            UserLoader.loadProfilePicture(url: url, user: user, failed: failed) {
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(user)
            }
            
            UserDefaults.standard.set(true, forKey: Constants.FirstListLoad)
            complete(firstLogin)
        }
    }
    
    static func loginLoadEmail(email: String, password: String, complete: @escaping ((Bool) -> Void), failed: @escaping (() -> Void)) {
        let dispatchGroup = DispatchGroup()
        let user = User()
        
        var firstLogin = false
        
        dispatchGroup.enter()
        UserLoader.loginEmail(email: email, password: password, user: user, failed: failed) { (first) in
            firstLogin = first
            dispatchGroup.leave()
        }
        
        
        dispatchGroup.notify(queue: .main) {
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(user)
            }
            
            UserDefaults.standard.set(true, forKey: Constants.FirstListLoad)
            complete(firstLogin)
        }
    }
}
